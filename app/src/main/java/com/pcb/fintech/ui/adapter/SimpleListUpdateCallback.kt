package com.pcb.fintech.ui.adapter

import androidx.recyclerview.widget.ListUpdateCallback
import androidx.recyclerview.widget.RecyclerView

class SimpleListUpdateCallback(private val adapter: RecyclerView.Adapter<*>,
                               private val updateCallback: UpdateCallback) : ListUpdateCallback {

    override fun onChanged(position: Int, count: Int, payload: Any?) {
        adapter.notifyItemRangeChanged(position, count, payload)
        updateCallback.onChanged?.invoke(position, count, payload)
    }

    override fun onMoved(fromPosition: Int, toPosition: Int) {
        adapter.notifyItemMoved(fromPosition, toPosition)
        updateCallback.onMoved?.invoke(fromPosition, toPosition)
    }

    override fun onInserted(position: Int, count: Int) {
        adapter.notifyItemRangeInserted(position, count)
        updateCallback.onInserted?.invoke(position, count)
    }

    override fun onRemoved(position: Int, count: Int) {
        adapter.notifyItemRangeRemoved(position, count)
        updateCallback.onRemoved?.invoke(position, count)
    }
}

class UpdateCallback(val onChanged: ((position: Int, count: Int, payload: Any?) -> Unit)?,
                     val onMoved: ((fromPosition: Int, toPosition: Int) -> Unit)?,
                     val onInserted: ((position: Int, count: Int) -> Unit)?,
                     val onRemoved: ((position: Int, count: Int) -> Unit)?) {
    constructor() : this(null, null, null, null)
}