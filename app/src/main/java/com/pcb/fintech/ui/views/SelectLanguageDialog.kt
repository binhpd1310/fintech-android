package com.pcb.fintech.ui.views

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatDialogFragment
import com.pcb.fintech.R
import com.pcb.fintech.language.Language
import com.pcb.fintech.ui.base.HasFragmentResult
import kotlinx.android.synthetic.main.fragment_select_language.*

class SelectLanguageDialog : AppCompatDialogFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_select_language, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setCancelable(false)
        setupViews()
    }

    override fun onResume() {
        super.onResume()
        dialog?.window?.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
    }

    private fun setupViews() {
        val language = arguments?.getParcelable(EXTRA_DATA) ?: Language.VIETNAMESE
        if (language == Language.VIETNAMESE) {
            rb_vietnamese.isChecked = true
        } else {
            rb_english.isChecked = true
        }
        tv_back.setOnClickListener { dismiss() }
        tv_save.setOnClickListener { save() }
    }

    private fun save() {
        val language = if (rb_english.isChecked) Language.ENGLISH else Language.VIETNAMESE
        val bundle = Bundle().apply { putParcelable(EXTRA_DATA, language) }
        val intent = Intent().apply { putExtras(bundle) }
        (targetFragment as HasFragmentResult).onFragmentResult(targetRequestCode, HasFragmentResult.ACTION_OK, intent)
        dismiss()
    }

    companion object {

        private const val EXTRA_DATA = "EXTRA_DATA"

        fun newInstance(language: Language) = SelectLanguageDialog().apply {
            arguments = Bundle().apply { putParcelable(EXTRA_DATA, language) }
        }

        fun getExtraData(intent: Intent?): Language? {
            return intent?.extras?.getParcelable(EXTRA_DATA)
        }
    }
}