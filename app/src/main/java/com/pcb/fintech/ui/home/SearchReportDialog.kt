package com.pcb.fintech.ui.home

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.appcompat.app.AppCompatDialogFragment
import androidx.core.content.ContextCompat
import com.pcb.fintech.R
import com.pcb.fintech.data.model.other.ReportStatus
import com.pcb.fintech.data.model.other.ReportStatusDisplay
import com.pcb.fintech.ui.base.HasFragmentResult
import com.pcb.fintech.ui.views.SimpleAdapterItemSelectedListener
import com.pcb.fintech.ui.views.gone
import com.pcb.fintech.ui.views.newInstanceWithLimit
import com.pcb.fintech.ui.views.visibleAndSet
import com.pcb.fintech.utils.DateTimeUtil
import kotlinx.android.synthetic.main.fragment_search_report.*
import java.util.*

class SearchReportDialog : AppCompatDialogFragment() {

    private var mStartDate: Date? = null
    private var mEndDate: Date? = null
    private var mStatus: ReportStatus? = null

    private val statusLabels by lazy {
        mutableListOf<String>().apply {
            add(0, getString(R.string.type_all))
            ReportStatus.values().forEach { status ->
                add(getString(ReportStatusDisplay.getStringRes(status)))
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_search_report, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupViews()
    }

    override fun onResume() {
        super.onResume()
        dialog?.window?.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
    }

    private fun setupViews() {
        iv_close.setOnClickListener { dismissAllowingStateLoss() }

        sp_status.adapter = ArrayAdapter(context!!, R.layout.item_spinner, statusLabels)
        sp_status.onItemSelectedListener = object : SimpleAdapterItemSelectedListener() {
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                mStatus = ReportStatus.fromValue(position)
            }
        }

        layout_time_all.setOnClickListener {
            clickTimeAll()
        }

        layout_time_rage.setOnClickListener {
            clickTimeRange()
        }

        tv_start_date.setOnClickListener { showPickStartDate() }

        tv_end_date.setOnClickListener { showPickEndDate() }

        tv_search.setOnClickListener { clickSearch() }

        fillData()
    }

    private fun fillData() {
        val bundle = arguments ?: return
        val searchType = bundle.getInt(EXTRA_SEARCH_TYPE)
        mStatus = bundle.getParcelable<ReportStatus>(EXTRA_TYPE_STATUS)
        mStartDate = bundle.getSerializable(EXTRA_START_DATE) as Date?
        mEndDate = bundle.getSerializable(EXTRA_END_DATE) as Date?
        if (searchType == TYPE_TIME_RANGE) {
            clickTimeRange()
            tv_start_date.text = DateTimeUtil.formatDate(mStartDate, DateTimeUtil.DATE_TEXT_2)
            tv_end_date.text = DateTimeUtil.formatDate(mEndDate, DateTimeUtil.DATE_TEXT_2)
        } else {
            clickTimeAll()
        }
        sp_status.setSelection(mStatus?.value ?: 0)
    }

    private fun clickTimeAll() {
        rb_time_all.isChecked = true
        rb_time_range.isChecked = false
        val textColor = ContextCompat.getColor(context!!, R.color.gray_1)
        tv_start_date_label.setTextColor(textColor)
        tv_end_date_label.setTextColor(textColor)
        tv_start_date.setTextColor(textColor)
        tv_end_date.setTextColor(textColor)
    }

    private fun clickTimeRange() {
        rb_time_all.isChecked = false
        rb_time_range.isChecked = true
        val textColor = ContextCompat.getColor(context!!, R.color.black_1)
        tv_start_date_label.setTextColor(textColor)
        tv_end_date_label.setTextColor(textColor)
        tv_start_date.setTextColor(textColor)
        tv_end_date.setTextColor(textColor)
    }

    private fun showPickStartDate() {
        val datePickerDialog = newInstanceWithLimit(mStartDate) {
            val endDate = mEndDate
            if (endDate != null && it.time > endDate.time) {
                mStartDate = null
                tv_start_date.text = ""
                tv_date_error.visibleAndSet(getString(R.string.text_check_start_date))
            } else {
                mStartDate = it
                tv_start_date.text = DateTimeUtil.formatDate(it, DateTimeUtil.DATE_TEXT_2)
                tv_date_error.gone()
            }
        }
        datePickerDialog.show(activity?.fragmentManager, "")
    }

    private fun showPickEndDate() {
        val datePickerDialog = newInstanceWithLimit(mEndDate) {
            val startDate = mStartDate
            if (startDate != null && startDate.time > it.time) {
                mEndDate = null
                tv_end_date.text = ""
                tv_date_error.visibleAndSet(getString(R.string.text_check_end_date))
            } else {
                mEndDate = it
                tv_end_date.text = DateTimeUtil.formatDate(it, DateTimeUtil.DATE_TEXT_2)
                tv_date_error.gone()
            }
        }
        datePickerDialog.show(activity?.fragmentManager, "")
    }

    private fun clickSearch() {
        val bundle = Bundle().apply {
            putParcelable(EXTRA_TYPE_STATUS, mStatus)
            if (rb_time_all.isChecked) {
                putInt(EXTRA_SEARCH_TYPE, TYPE_TIME_ALL)
            } else {
                putInt(EXTRA_SEARCH_TYPE, TYPE_TIME_RANGE)
                putSerializable(EXTRA_START_DATE, mStartDate)
                putSerializable(EXTRA_END_DATE, mEndDate)
            }
        }
        val intent = Intent().apply { putExtras(bundle) }
        if (targetFragment is HasFragmentResult) {
            (targetFragment as HasFragmentResult).onFragmentResult(targetRequestCode, HasFragmentResult.ACTION_OK, intent)
        }
        dismiss()
    }

    companion object {
        const val EXTRA_START_DATE = "EXTRA_START_DATE"
        const val EXTRA_END_DATE = "EXTRA_END_DATE"
        const val EXTRA_TYPE_STATUS = "EXTRA_TYPE_STATUS"
        const val EXTRA_SEARCH_TYPE = "EXTRA_SEARCH_TYPE"
        const val TYPE_TIME_ALL = 0
        const val TYPE_TIME_RANGE = 1

        fun newInstance(searchType: Int, status: ReportStatus?,
                        startDate: Date?, endDate: Date?) = SearchReportDialog().apply {
            arguments = Bundle().apply {
                putInt(EXTRA_SEARCH_TYPE, searchType)
                putParcelable(EXTRA_TYPE_STATUS, status)
                putSerializable(EXTRA_START_DATE, startDate)
                putSerializable(EXTRA_END_DATE, endDate)
            }
        }
    }
}