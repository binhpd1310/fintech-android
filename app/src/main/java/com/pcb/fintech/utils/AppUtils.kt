package com.pcb.fintech.utils

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.webkit.WebChromeClient
import android.webkit.WebSettings
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.core.content.FileProvider
import com.pcb.fintech.data.model.response.LocalLink
import com.pcb.fintech.data.model.response.WebLink
import com.pcb.fintech.data.source.local.PreferenceHelper
import com.pcb.fintech.language.Language
import com.pcb.fintech.ui.base.OpenActivity
import com.pcb.fintech.ui.home.MainFragment
import java.io.File
import java.math.BigDecimal
import java.math.RoundingMode

fun deleteCache(context: Context?) {
    context ?: return
    ThreadUtils.onWorker(Runnable {
        val contents = context.cacheDir?.listFiles() ?: return@Runnable
        contents.forEach { item -> item.deleteRecursively() }
        LogUtil.debug("Delete all cache content!")
    })
}

fun String?.formatPath(): String? {
    return this?.replace("\\", "/")
}

fun getImageUri(appContext: Context, imageFile: File): Uri {
    return FileProvider.getUriForFile(
            appContext, appContext.packageName + ".provider", imageFile
    )
}

fun Double?.formatDoubleToLong(): Long {
    if (this == null) return 0
    val temp = BigDecimal.valueOf(this).setScale(0, RoundingMode.FLOOR)
    return temp.toLong()
}

fun formatToString(n: Long?): String {
    if (n == null) return "0"
    return BigDecimal.valueOf(n).setScale(0, RoundingMode.FLOOR).toString()
}

fun formatToString(d: Double?): String {
    if (d == null) return "0"
    val temp = BigDecimal.valueOf(d).setScale(0, RoundingMode.FLOOR)
    return temp.toString()
}

inline fun check(condition: () -> Boolean,
                 ifTrue: (() -> Unit)) {
    if (condition()) ifTrue.invoke()
}

fun getHiddenPhone(phoneNumber: String): String {
    val stringBuilder = StringBuilder(phoneNumber)
    for ((i, _) in phoneNumber.withIndex()) {
        if (i > 1 && i < phoneNumber.length - 3) stringBuilder.replace(i, i + 1, "*")
    }
    return stringBuilder.toString()
}

fun getHomeIntent(context: Context, isExpired: Boolean = false): Intent? {
    val bundle = MainFragment.getBundle(isExpired)
    return OpenActivity.getIntent(context, MainFragment::class.java, bundle)?.apply {
        addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
    }
}

fun WebView.configWebView() {
    with(this) {
        WebView.setWebContentsDebuggingEnabled(true)
        scrollBarStyle = WebView.SCROLLBARS_OUTSIDE_OVERLAY
        setInitialScale(1)
        with(settings) {
            javaScriptEnabled = true
            domStorageEnabled = true
            loadWithOverviewMode = true
            layoutAlgorithm = WebSettings.LayoutAlgorithm.SINGLE_COLUMN
            defaultZoom = WebSettings.ZoomDensity.FAR
            useWideViewPort = true
            setSupportZoom(true)
            builtInZoomControls = true
            displayZoomControls = false
            javaScriptCanOpenWindowsAutomatically = true
            pluginState = WebSettings.PluginState.ON
        }
        webViewClient = WebViewClient()
        webChromeClient = WebChromeClient()
    }
}

fun getMoneyInText(amount: Long, currency: String = "đ"): String {
    return "${formatToString(amount)} $currency"
}

fun getMoneyInText(amount: Double, currency: String = "đ"): String {
    return "${formatToString(amount)} $currency"
}

fun getLocalLink(pref: PreferenceHelper, webLink: WebLink?, defaultWebLink: WebLink): LocalLink {
    return if (pref.getLanguage() == Language.ENGLISH) {
        LocalLink(webLink?.linkEn ?: defaultWebLink.linkEn,
                webLink?.titleEn ?: defaultWebLink.titleEn)
    } else {
        LocalLink(webLink?.linkVi ?: defaultWebLink.linkVi,
                webLink?.titleVi ?: defaultWebLink.titleVi)
    }
}