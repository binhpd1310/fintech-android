package com.pcb.fintech.data.model.response

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class SecretQuestion constructor(
        @SerializedName("id") val id: Int? = null,
        @SerializedName("name") val name: String? = null) : Parcelable