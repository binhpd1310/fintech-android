package com.pcb.fintech.ui.report

import android.app.Application
import com.pcb.fintech.data.model.body.VerifyOtpBody
import com.pcb.fintech.data.source.CommonDataSource
import com.pcb.fintech.data.source.local.PreferenceHelper
import com.pcb.fintech.ui.base.BaseViewModel
import com.pcb.fintech.utils.SchedulerProvider

class VerifyOtpVM(application: Application, schedulerProvider: SchedulerProvider,
                  private val preferenceHelper: PreferenceHelper,
                  private val commonDataSource: CommonDataSource
) : BaseViewModel(application, schedulerProvider) {

    fun getPhoneNumber(): String {
        return preferenceHelper.getAccountInfo()?.phone ?: ""
    }

    fun saveVerifiedOtp() {
        preferenceHelper.saveVeriedOtp(true)
    }

    fun resendOtp() = resultLiveData {
        commonDataSource.createMobileOtp(getPhoneNumber())
    }

    fun verifyOtp(otp: String) = resultLiveData {
        commonDataSource.verifyMobileOtp(VerifyOtpBody(getPhoneNumber(), otp))
    }
}