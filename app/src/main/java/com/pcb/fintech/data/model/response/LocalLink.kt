package com.pcb.fintech.data.model.response

data class LocalLink(val link: String?,
                     val title: String?)