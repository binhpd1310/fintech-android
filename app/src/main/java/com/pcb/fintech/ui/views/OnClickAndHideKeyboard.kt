package com.pcb.fintech.ui.views

import android.view.View
import com.pcb.fintech.utils.AndroidUtil

class OnClickAndHideKeyboard(private val handleClick: (view: View) -> Unit) : View.OnClickListener {

    override fun onClick(v: View) {
        handleClick(v)
        AndroidUtil.hideKeyboard(v)
    }

}