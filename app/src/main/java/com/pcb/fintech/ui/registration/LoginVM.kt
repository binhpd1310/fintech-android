package com.pcb.fintech.ui.registration

import android.app.Application
import androidx.lifecycle.LiveData
import com.pcb.fintech.data.model.body.EmailBody
import com.pcb.fintech.data.source.CommonDataSource
import com.pcb.fintech.data.source.local.PreferenceHelper
import com.pcb.fintech.language.Language
import com.pcb.fintech.ui.base.BaseViewModel
import com.pcb.fintech.ui.base.SingleLiveEvent
import com.pcb.fintech.utils.SchedulerProvider

class LoginVM constructor(
        application: Application, schedulerProvider: SchedulerProvider,
        private val prefHelper: PreferenceHelper,
        private val commonDataSource: CommonDataSource
) : BaseViewModel(application, schedulerProvider) {

    private val updateLanguageLD = SingleLiveEvent<Boolean>()

    fun getUpdateLanguageLD(): LiveData<Boolean> = updateLanguageLD

    fun getLanguage() = prefHelper.getLanguage()

    fun clearLocal() {
        prefHelper.clear()
    }

    fun login(userName: String, password: String, rememberMe: Boolean) = resultLiveData {
        commonDataSource.login(userName, password, rememberMe)
    }

    fun forgotPassword(emailBody: EmailBody) = resultLiveData {
        commonDataSource.forgotPassword(emailBody)
    }

    fun changeLanguage(language: Language) {
        saveLanguage(language)
        updateLanguageLD.postValue(true)
    }
}