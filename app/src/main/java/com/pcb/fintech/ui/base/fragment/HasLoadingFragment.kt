package com.pcb.fintech.ui.base.fragment

import android.app.ProgressDialog
import android.os.Bundle
import com.pcb.fintech.R

open class HasLoadingFragment : SyncBackFragment() {

    @Suppress("DEPRECATION")
    protected var mLoadingDialog: ProgressDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mLoadingDialog = ProgressDialog(activity, R.style.DialogCustomStyle)
    }

    protected open fun showLoading(cancelable: Boolean = false) {
        if (mLoadingDialog?.isShowing == true) {
            mLoadingDialog?.dismiss()
        }
        mLoadingDialog?.setCancelable(cancelable)
        mLoadingDialog?.show()
    }

    protected open fun hideLoading() {
        mLoadingDialog?.dismiss()
    }
}