package com.pcb.fintech.data.model.body

import com.google.gson.annotations.SerializedName

data class SearchReportBody constructor(
        @SerializedName("displayPages") val displayPages: Int? = null,
        @SerializedName("currentPage") val currentPage: Int? = null,
        @SerializedName("status") val status: Int? = null,
        @SerializedName("startDate") val startDate: String? = null,
        @SerializedName("endDate") val endDate: String? = null,
        @SerializedName("keywords") val keywords: String? = null
)