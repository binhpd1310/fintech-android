package com.pcb.fintech.utils.validation

import androidx.annotation.StringRes

interface CardValidator {
    fun validate(cardNumber: String): CardValidationResult
}

sealed class CardValidationResult {

    object CardValid : CardValidationResult()

    class CardInvalid(@StringRes val msgRes: Int) : CardValidationResult()
}