package com.pcb.fintech.ui.home

import android.app.Application
import android.graphics.Bitmap
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.pcb.fintech.R
import com.pcb.fintech.data.model.ResponseResult
import com.pcb.fintech.data.model.other.Report
import com.pcb.fintech.data.model.response.AccountInfo
import com.pcb.fintech.data.model.response.LocalLink
import com.pcb.fintech.data.model.response.Notification
import com.pcb.fintech.data.model.response.WebLink
import com.pcb.fintech.data.source.CommonDataSource
import com.pcb.fintech.data.source.ProfileDataSource
import com.pcb.fintech.data.source.ReportDataSource
import com.pcb.fintech.data.source.local.PreferenceHelper
import com.pcb.fintech.eventbus.event.*
import com.pcb.fintech.eventbus.observer.SimpleEventObserver
import com.pcb.fintech.language.Language
import com.pcb.fintech.ui.base.BaseViewModel
import com.pcb.fintech.ui.base.SingleLiveEvent
import com.pcb.fintech.ui.home.model.ReportInput
import com.pcb.fintech.ui.views.NotiPaginationHelper
import com.pcb.fintech.ui.views.ReportPaginationHelper
import com.pcb.fintech.utils.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.channels.BroadcastChannel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.asFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.debounce
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class HomeVM(application: Application, schedulerProvider: SchedulerProvider,
             internal val prefHelper: PreferenceHelper,
             private val commonDataSource: CommonDataSource,
             private val profileDataSource: ProfileDataSource,
             private val reportDataSource: ReportDataSource
) : BaseViewModel(application, schedulerProvider) {

    private var reportPaginationHelper: ReportPaginationHelper? = null

    private var notiPaginationHelper: NotiPaginationHelper? = null

    private val queryChannel = BroadcastChannel<ReportInput>(Channel.CONFLATED)

    private val profileLD = MutableLiveData<ResponseResult<AccountInfo>>()

    private val base64DecoderLD = MutableLiveData<Bitmap>()

    private val updateReportListLD = MutableLiveData<List<Report>>()

    private val updateNotiListLD = MutableLiveData<List<Notification>>()

    private val updateLanguageLD = SingleLiveEvent<Boolean>()

    private val totalReportsLD = MutableLiveData<Int>()

    private val logoutLD = SingleLiveEvent<Boolean>()

    private val quitAppLD = SingleLiveEvent<Boolean>()

    private val openCloseLeftMenuLD = SingleLiveEvent<Boolean>()

    private val needToVerifyOtpLD = SingleLiveEvent<Boolean>()

    private val openHomeTabLD = SingleLiveEvent<Int>()

    private val noteLinksLD = MutableLiveData<List<LocalLink>>()
    private val termLinkLD = MutableLiveData<LocalLink>()
    private val usageLinkLD = MutableLiveData<LocalLink>()
    private val faqLinkLD = MutableLiveData<LocalLink>()
    private val facebookLinkLD = MutableLiveData<LocalLink>()

    private val needFillProfile = SingleLiveEvent<Boolean>()

    private val eventBusObserver = SimpleEventObserver(
            onCreatedReport = { getReportsWithRefresh() },
            onCanceledReport = { getReportsWithRefresh() },
            onDeletedReport = { getReportsWithRefresh() },
            onPaymentSuccess = { getReportsWithRefresh() },
            onAccountChanged = { onAccountChanged() },
            onUpdatedReport = { getReportsWithRefresh() },
            onOpenHomeTab = { openHomeTab(it.position) },
            onCloseLeftMenu = { openCloseLeftMenuLD.postValue(false) },
            onDeletedNoti = { onNotiDeleted(it.notiId) }
    )

    init {
        registerEvent(CreatedReportEvent.TYPE, eventBusObserver)
        registerEvent(CancelledReportEvent.TYPE, eventBusObserver)
        registerEvent(DeletedReportEvent.TYPE, eventBusObserver)
        registerEvent(PaymentSuccessEvent.TYPE, eventBusObserver)
        registerEvent(UserAccountChangedEvent.TYPE, eventBusObserver)
        registerEvent(UpdatedReportEvent.TYPE, eventBusObserver)
        registerEvent(OpenHomeTabEvent.TYPE, eventBusObserver)
        registerEvent(CloseLeftMenuEvent.TYPE, eventBusObserver)
        registerEvent(DeletedNotiEvent.TYPE, eventBusObserver)

        getSlides()

        getLinks()
    }

    fun isLogin(): Boolean {
        return prefHelper.isLogin()
    }

    fun getLanguage(): Language {
        return prefHelper.getLanguage()
    }

    fun getProfile() {
        if (!isLogin()) {
            return
        }

        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                val profile = profileDataSource.getProfileData()
                profileLD.postValue(profile)
            }
        }
    }

    fun openHomeTab(position: Int) {
        openHomeTabLD.postValue(position)
    }

    // Start LiveData

    fun getProfileLD(): LiveData<ResponseResult<AccountInfo>> = profileLD

    fun getDecodeAvatarLD(): LiveData<Bitmap> = base64DecoderLD

    fun getUpdateReportListLD(): LiveData<List<Report>> = updateReportListLD

    fun getUpdateNotiListLD(): LiveData<List<Notification>> = updateNotiListLD

    fun getUpdateLanguageLD(): LiveData<Boolean> = updateLanguageLD

    fun getTotalReportsLD(): LiveData<Int> = totalReportsLD

    fun logoutLD(): LiveData<Boolean> = logoutLD

    fun quitAppLD(): LiveData<Boolean> = quitAppLD

    fun openLeftMenuLD(): LiveData<Boolean> = openCloseLeftMenuLD

    fun needToVerifyOtp(): LiveData<Boolean> = needToVerifyOtpLD

    fun openHomeTabLD(): LiveData<Int> = openHomeTabLD

    fun noteLinksLD(): LiveData<List<LocalLink>> = noteLinksLD

    fun getTermLinkLD(): LiveData<LocalLink> = termLinkLD

    fun getUsageLinkLD(): LiveData<LocalLink> = usageLinkLD

    fun getFaqLinkLD(): LiveData<LocalLink> = faqLinkLD

    fun getFacebookLinkLD(): LiveData<LocalLink> = facebookLinkLD

    fun needFillUserInfo(): LiveData<Boolean> = needFillProfile

    // End LiveData

    fun decodeBase64(base64String: String?) {
        base64String ?: return
        launchDataLoad {
            val bitmap = Base64Image.decodeBase64(base64String)
            base64DecoderLD.postValue(bitmap)
        }
    }

    private fun getReportsWithRefresh() {
        if (!isLogin()) {
            postHideLoading()
            return
        }
        reportPaginationHelper?.callWithRefresh {
            updateReportListLD.postValue(it)
            totalReportsLD.postValue(reportPaginationHelper?.getTotalReports() ?: 0)
        }
    }

    private fun onAccountChanged() {
        val accountInfo = prefHelper.getAccountInfo() ?: return
        profileLD.postValue(ResponseResult.Success(accountInfo))
    }

    /**
     * Get reports
     */
    fun observeSearch() {
        viewModelScope.launch {
            queryChannel.asFlow().debounce(400)
                    .collect { getReportsWithRefresh(it) }
        }
    }

    fun getReports() = reportPaginationHelper?.getReports()

    fun getReportsWithRefresh(reportInput: ReportInput) {
        if (!isLogin()) {
            postHideLoading()
            return
        }

        val reportHelper = ReportPaginationHelper(viewModelScope, reportDataSource, reportInput)
        reportHelper.callWithRefresh {
            updateReportListLD.postValue(it)
            totalReportsLD.postValue(reportHelper.getTotalReports())
        }
        reportPaginationHelper = reportHelper
    }

    fun getNextReports() {
        if (!isLogin()) {
            postHideLoading()
            return
        }

        reportPaginationHelper?.callNext { updateReportListLD.postValue(it) }
    }

    fun sendSearchRequest(reportInput: ReportInput) {
        if (!isLogin()) {
            postHideLoading()
            return
        }

        viewModelScope.launch { queryChannel.send(reportInput) }
    }

    /**
     * Get notifications
     */

    fun getNotisWithRefresh() {
        if (!isLogin()) {
            postHideLoading()
            return
        }

        val paginationHelper = NotiPaginationHelper(viewModelScope, commonDataSource)

        paginationHelper.callWithRefresh { updateNotiListLD.postValue(it) }
        notiPaginationHelper = paginationHelper
    }

    fun getNextNotis() {
        if (!isLogin()) {
            postHideLoading()
            return
        }

        notiPaginationHelper?.callNext { updateNotiListLD.postValue(it) }
    }

    private fun onNotiDeleted(notiId: Int) {
        notiPaginationHelper?.updateNotification(notiId, found = { index, notiList ->
            notiList.removeAt(index)
        }, finish = {
            updateNotiListLD.postValue(it)
        })
    }

    // End notifications

    fun getSlides() = resultLiveData(false) { commonDataSource.getSlides() }

    fun changeLanguage(language: Language) {
        saveLanguage(language)
        updateLanguageLD.postValue(true)
    }

    private fun logoutRemote(onTerminate: (() -> Unit)? = null) {
        if (!prefHelper.isLogin()) {
            onTerminate?.invoke()
            return
        }

        callApi(call = { commonDataSource.logout() }, isLoading = false, onTerminate = {
            prefHelper.clear()

            reportPaginationHelper = null

            notiPaginationHelper = null

            updateReportListLD.postValue(listOf())

            updateNotiListLD.postValue(listOf())

            profileLD.postValue(null)

            totalReportsLD.postValue(0)

            onTerminate?.invoke()
        })
    }

    fun logout() {
        openCloseLeftMenuLD.postValue(false)
        logoutRemote { logoutLD.postValue(true) }
    }

    fun quitApp() {
        logoutRemote { quitAppLD.postValue(true) }
    }

    fun requestOpenLeftMenu() {
        openCloseLeftMenuLD.postValue(true)
    }

    fun createReport() {
        if (!isLogin()) {
            return
        }

        if (prefHelper.getAccountInfo()?.validUserInfo() == false) {
            needFillProfile.postValue(true)
            return
        }

        val phone = prefHelper.getAccountInfo()?.phone

        if (phone.isNullOrEmpty() || prefHelper.verifiedOtp()) {
            needToVerifyOtpLD.postValue(false)
            return
        }

        callApi(call = { commonDataSource.createMobileOtp(phone) },
                onSuccess = { needToVerifyOtpLD.postValue(true) },
                onFailed = { needToVerifyOtpLD.postValue(false) },
                isLoading = false)
    }

    fun deleteAllNotifications() = resultLiveData {
        commonDataSource.deleteAllNoti()
    }

    // Service links

    private fun parseTermLink(webLinks: List<WebLink>) {
        val foundTermLink = webLinks.find { item -> item.linkCode == LinkType.TERM.value }
        val termDefaultLink = WebLink(linkEn = Constant.DEFAULT_TERM_LINK_EN, linkVi = Constant.DEFAULT_TERM_LINK_VI,
                titleEn = getContext().getString(R.string.title_term), titleVi = getContext().getString(R.string.title_term))
        val termLocalLink = getLocalLink(prefHelper, foundTermLink, termDefaultLink)
        termLinkLD.postValue(termLocalLink)
    }

    private fun parseUsageLink(webLinks: List<WebLink>) {
        val usgLink = webLinks.find { it.linkCode == LinkType.USER_MANUAL.value }
        val usgDefaultLink = WebLink(linkEn = Constant.DEFAULT_INSTRUCTION_LINK_EN, linkVi = Constant.DEFAULT_INSTRUCTION_LINK_VI,
                titleEn = getContext().getString(R.string.sp_instruction_for_use), titleVi = getContext().getString(R.string.sp_instruction_for_use))
        val usageLocalLink = getLocalLink(prefHelper, usgLink, usgDefaultLink)
        usageLinkLD.postValue(usageLocalLink)
    }

    private fun parseFAQLink(webLinks: List<WebLink>) {
        val faqLink = webLinks.find { it.linkCode == LinkType.FAQ.value }
        val faqDefaultLink = WebLink(linkEn = Constant.DEFAULT_FAQ_LINK_EN, linkVi = Constant.DEFAULT_FAQ_LINK_VI,
                titleEn = getContext().getString(R.string.sp_questions), titleVi = getContext().getString(R.string.sp_questions))
        val faqLocalLink = getLocalLink(prefHelper, faqLink, faqDefaultLink)
        faqLinkLD.postValue(faqLocalLink)
    }

    private fun parseFacebookLink(webLinks: List<WebLink>) {
        val facebookLink = webLinks.find { it.linkCode == LinkType.FB.value }
        val facebookDefaultLink = WebLink(linkEn = Constant.DEFAULT_FB_LINK, linkVi = Constant.DEFAULT_FB_LINK,
                titleEn = getContext().getString(R.string.sp_fbook), titleVi = getContext().getString(R.string.sp_fbook))
        val fbLocalLink = getLocalLink(prefHelper, facebookLink, facebookDefaultLink)
        facebookLinkLD.postValue(fbLocalLink)
    }

    private fun parseNotes(links: List<WebLink>) {
        ThreadUtils.onWorker(Runnable {
            val localLinks = mutableListOf<LocalLink>()
            links.forEach { webLink ->
                if (webLink.articleType == WebLink.ARTICLE_TYPE) {
                    val localLink = if (prefHelper.getLanguage() == Language.ENGLISH) {
                        LocalLink(webLink.linkEn, webLink.titleEn)
                    } else {
                        LocalLink(webLink.linkVi, webLink.titleVi)
                    }
                    localLinks.add(localLink)
                }
            }
            noteLinksLD.postValue(localLinks)
        })
    }

    private fun getLinks() {
        callApi({ commonDataSource.getServiceLinks() }, isLoading = false, onSuccess = {
            parseNotes(it)
            parseTermLink(it)
            parseUsageLink(it)
            parseFAQLink(it)
            parseFacebookLink(it)
        })
    }

    fun openUsageLink() {
        AndroidUtil.openWebLink(getContext(), usageLinkLD.value?.link)
    }

    fun openFaqLink() {
        AndroidUtil.openWebLink(getContext(), faqLinkLD.value?.link)
    }

    fun sendEmail() {
        AndroidUtil.sendEmail(getContext(), Constant.DEFAULT_EMAIL)
    }

    fun openPhone() {
        AndroidUtil.callPhoneNumber(getContext(), Constant.DEFAULT_SUPPORT_PHONE)
    }

    fun openFbLink() {
        AndroidUtil.openWebLink(getContext(), facebookLinkLD.value?.link)
    }

    fun openWebLink(url: String?) {
        AndroidUtil.openWebLink(getContext(), url)
    }
}