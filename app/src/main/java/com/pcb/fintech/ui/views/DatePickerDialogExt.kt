package com.pcb.fintech.ui.views

import com.wdullaer.materialdatetimepicker.date.DatePickerDialog
import java.util.*

fun newInstance(initializeDate: Date?, callBack: ((date: Date) -> Unit)?): DatePickerDialog {
    val initializeCal = Calendar.getInstance()
    if (initializeDate != null) initializeCal.time = initializeDate
    val datePickerDialog = DatePickerDialog.newInstance({ view, year, monthOfYear, dayOfMonth ->
        val c = Calendar.getInstance()
        c.set(Calendar.YEAR, year)
        c.set(Calendar.MONTH, monthOfYear)
        c.set(Calendar.DAY_OF_MONTH, dayOfMonth)
        c.set(Calendar.HOUR, 1)
        c.set(Calendar.MINUTE, 0)
        c.set(Calendar.SECOND, 0)
        callBack?.invoke(c.time)
    }, initializeCal.get(Calendar.YEAR), initializeCal.get(Calendar.MONTH), initializeCal.get(Calendar.DAY_OF_MONTH))
    datePickerDialog.showYearPickerFirst(true)
    datePickerDialog.vibrate(false)
    return datePickerDialog
}

fun newInstanceWithLimit(initializeDate: Date?, callBack: ((date: Date) -> Unit)?): DatePickerDialog {
    val cal = Calendar.getInstance()
    val datePickerDialog = newInstance(initializeDate, callBack)
    datePickerDialog.maxDate = cal
    return datePickerDialog
}

fun newInstanceWithLimit(initializeDate: Date?, maxDate: Date, callBack: ((date: Date) -> Unit)?): DatePickerDialog {
    val cal = Calendar.getInstance().apply { time = maxDate }
    val datePickerDialog = newInstance(initializeDate, callBack)
    datePickerDialog.maxDate = cal
    return datePickerDialog
}