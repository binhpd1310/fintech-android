package com.pcb.fintech.data.utils

import com.facebook.stetho.okhttp3.StethoInterceptor
import com.pcb.fintech.BuildConfig
import com.pcb.fintech.data.source.remote.config.ApiParams
import com.pcb.fintech.data.source.remote.config.ParamsName
import okhttp3.*
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.security.KeyStore
import java.security.cert.CertificateException
import java.security.cert.X509Certificate
import java.util.*
import java.util.concurrent.TimeUnit
import javax.net.ssl.SSLContext
import javax.net.ssl.TrustManager
import javax.net.ssl.TrustManagerFactory
import javax.net.ssl.X509TrustManager

object ServiceUtils {

    fun createUnsafeOkHttpClient(): OkHttpClient {
        return OkHttpClient.Builder()
                .hostnameVerifier { _, _ -> true }
                .addSSLSocketFactory()
                .addDevInterceptor(BuildConfig.BUILD_TYPE == "debug")
                .build()
    }

    fun createOkHttpClient(apiParams: ApiParams?,
                           authenticator: Authenticator? = null): OkHttpClient {
        val clientBuilder = OkHttpClient.Builder()
        val paramsInterceptor = Interceptor { chain ->
            val original = chain.request()
            val builder = original.newBuilder()
            apiParams?.run {
                if (getAuthorization().isNotEmpty()) {
                    builder.addHeader(ParamsName.AUTHORIZATION, getAuthorization())
                }
                if (getContentType().isNotEmpty()) {
                    builder.addHeader(ParamsName.CONTENT_TYPE, getContentType())
                }
                if (getCacheControl().isNotEmpty()) {
                    builder.addHeader(ParamsName.CACHE_CONTROL, getCacheControl())
                }
                if (getAPIKey().isNotEmpty()) {
                    builder.addHeader(ParamsName.API_KEY, getAPIKey())
                }
                builder.method(original.method(), original.body())
            }
            chain.proceed(builder.build())
        }

        if (authenticator != null) {
            clientBuilder.authenticator(authenticator)
        }

        return clientBuilder.connectTimeout(60L, TimeUnit.SECONDS)
                .readTimeout(60L, TimeUnit.SECONDS)
                .hostnameVerifier { _, _ -> true }
                .addInterceptor(paramsInterceptor)
                .addSSLSocketFactory()
                .addDevInterceptor(BuildConfig.BUILD_TYPE == "debug")
                .setConnectionSpecs()
                .build()
    }

    inline fun <reified T> createWebService(okHttpClient: OkHttpClient, domain: String): T {
        val retrofit = Retrofit.Builder()
                .baseUrl(domain)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
        return retrofit.create(T::class.java)
    }
}

fun OkHttpClient.Builder.addSSLSocketFactory(): OkHttpClient.Builder {
    val trustAllCerts = arrayOf<TrustManager>(object : X509TrustManager {
        @Throws(CertificateException::class)
        override fun checkClientTrusted(chain: Array<X509Certificate>, authType: String) {
        }

        @Throws(CertificateException::class)
        override fun checkServerTrusted(chain: Array<X509Certificate>, authType: String) {
        }

        override fun getAcceptedIssuers(): Array<X509Certificate> {
            return arrayOf()
        }
    })

    // Install the all-trusting trust manager
    val sslContext = SSLContext.getInstance("SSL")
    sslContext.init(null, trustAllCerts, java.security.SecureRandom())

    this.sslSocketFactory(sslContext.socketFactory, trustAllCerts[0] as X509TrustManager)
    return this
}

fun OkHttpClient.Builder.addSSLSocketFactoryV2(): OkHttpClient.Builder {
    val trustManagerFactory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm())
            .apply { init(null as KeyStore?) }
    val trustManagers = trustManagerFactory.trustManagers
    check(trustManagers?.getOrNull(0) is X509TrustManager) {
        "Unexpected default trust managers:" + Arrays.toString(trustManagers)
    }
    val trustManager = trustManagers[0] as X509TrustManager
    val sslContext = SSLContext.getInstance("SSL")
    sslContext.init(null, arrayOf<TrustManager>(trustManager), null)

    this.sslSocketFactory(sslContext.socketFactory, trustManager)
    return this
}

fun OkHttpClient.Builder.addSSLSocketFactoryV3(): OkHttpClient.Builder {
    val sslSocketFactory = CustomSSLSocketFactory()
    this.sslSocketFactory(sslSocketFactory, sslSocketFactory.getTrustManager())
    return this
}

fun OkHttpClient.Builder.addDevInterceptor(isDebug: Boolean): OkHttpClient.Builder {
    if (isDebug) {
        val httpLogInterceptor = HttpLoggingInterceptor().apply { level = HttpLoggingInterceptor.Level.BASIC }
        this.addInterceptor(httpLogInterceptor)
        this.addNetworkInterceptor(StethoInterceptor())
    }
    return this
}

fun OkHttpClient.Builder.setConnectionSpecs(): OkHttpClient.Builder {
    val spec = ConnectionSpec.Builder(ConnectionSpec.COMPATIBLE_TLS)
            .tlsVersions(TlsVersion.TLS_1_3, TlsVersion.TLS_1_2, TlsVersion.TLS_1_1, TlsVersion.TLS_1_0)
            .allEnabledTlsVersions()
            .allEnabledCipherSuites()
            .build()
    this.connectionSpecs(Collections.singletonList(spec))
    return this
}