package com.pcb.fintech.ui.registration

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatDialogFragment
import com.pcb.fintech.R
import com.pcb.fintech.ui.base.HasFragmentResult
import kotlinx.android.synthetic.main.dialog_confirm_register.*

class ConfirmRegisterDialog : AppCompatDialogFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.dialog_confirm_register, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupViews()
    }

    override fun onResume() {
        super.onResume()
        dialog?.window?.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
    }

    private fun setupViews() {
        tv_ok.setOnClickListener { clickOK() }
    }

    private fun clickOK() {
        if (targetFragment is HasFragmentResult) {
            (targetFragment as HasFragmentResult).onFragmentResult(targetRequestCode, HasFragmentResult.ACTION_OK, null)
        }
        dismiss()
    }

    companion object {

        fun newInstance() = ConfirmRegisterDialog()
    }
}