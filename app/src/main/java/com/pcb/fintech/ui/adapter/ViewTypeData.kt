package com.pcb.fintech.ui.adapter

import android.view.View
import android.view.ViewGroup
import com.pcb.fintech.ui.adapter.MultiTypeVH

class ViewTypeData<O : Any, T : O, VH : MultiTypeVH<T>>
constructor(internal val viewType: () -> Int,
            internal val layoutRes: () -> Int,
            internal val itemClazz: () -> Class<T>,
            internal val createViewHolder: (parent: ViewGroup, itemView: View) -> VH)