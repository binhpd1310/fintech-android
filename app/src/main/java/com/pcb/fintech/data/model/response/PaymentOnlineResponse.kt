package com.pcb.fintech.data.model.response

import com.google.gson.annotations.SerializedName
import com.pcb.fintech.vnpay.VnPayParams

data class PaymentOnlineResponse
constructor(@SerializedName(VnPayParams.vnPayUrl) val vnPayUrl: String,
            @SerializedName(VnPayParams.vnpAmount) val vnpAmount: Double,
            @SerializedName(VnPayParams.vnpCommand) val vnpCommand: String,
            @SerializedName(VnPayParams.vnpCreateDate) val vnpCreateDate: Long,
            @SerializedName(VnPayParams.vnpCurrCode) val vnpCurrCode: String,
            @SerializedName(VnPayParams.vnpIpAddr) val vnpIpAddr: String,
            @SerializedName(VnPayParams.vnpLocale) val vnpLocale: String,
            @SerializedName(VnPayParams.vnpOrderInfo) val vnpOrderInfo: String,
            @SerializedName(VnPayParams.vnpOrderType) val vnpOrderType: String,
            @SerializedName(VnPayParams.returnUrl) val returnUrl: String,
            @SerializedName(VnPayParams.vnpTmnCode) val vnpTmnCode: String,
            @SerializedName(VnPayParams.vnpTxnRef) val vnpTxnRef: Long,
            @SerializedName(VnPayParams.vnpVersion) val vnpVersion: String,
            @SerializedName(VnPayParams.vnpSecureHash) val vnpSecureHash: String)