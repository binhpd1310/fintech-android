package com.pcb.fintech.ui.registration

import android.Manifest
import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.pcb.fintech.data.model.body.*
import com.pcb.fintech.data.model.response.LocalLink
import com.pcb.fintech.data.source.CommonDataSource
import com.pcb.fintech.data.source.ProfileDataSource
import com.pcb.fintech.data.source.local.PreferenceHelper
import com.pcb.fintech.language.Language
import com.pcb.fintech.ui.base.BaseViewModel
import com.pcb.fintech.ui.base.SingleLiveEvent
import com.pcb.fintech.utils.Constant.DEFAULT_TERM_LINK_EN
import com.pcb.fintech.utils.Constant.DEFAULT_TERM_LINK_VI
import com.pcb.fintech.utils.PermissionUtil
import com.pcb.fintech.utils.SchedulerProvider
import com.pcb.fintech.utils.ValidationUtil.isValidEmail
import java.io.File

class RegistrationVM constructor(application: Application, schedulerProvider: SchedulerProvider,
                                 private val preferenceHelper: PreferenceHelper,
                                 private val commonDataSource: CommonDataSource,
                                 private val profileDataSource: ProfileDataSource)
    : BaseViewModel(application, schedulerProvider) {

    private val backActionLD = SingleLiveEvent<Boolean>()
    private val profileLD = MutableLiveData<ProfileBody>()
    private val createdOtpLD = SingleLiveEvent<String>()
    private val verifiedOtpLD = SingleLiveEvent<Boolean>()
    private val stopTimerOtpLD = SingleLiveEvent<Boolean>()
    private val termLinkLD = MutableLiveData<LocalLink>()

    fun getBackActionLD() = backActionLD

    fun getProfileLD() = profileLD

    fun getCreatedOtpLD() = createdOtpLD

    fun getVerifiedOtpLD() = verifiedOtpLD

    fun getStopTimerOtpLD() = stopTimerOtpLD

    fun getAccountInfo() = preferenceHelper.getAccountInfo()

    fun getUserId() = preferenceHelper.getUserId()

    fun getAccountInfoLD() = resultLiveData {
        profileDataSource.getProfileData()
    }

    fun setProfile(profileBody: ProfileBody?) {
        profileLD.postValue(profileBody)
    }

    fun goPreviousStep() {
        backActionLD.postValue(true)
    }

    fun validateEmail(email: String?): Boolean {
        return isValidEmail(email)
    }

    fun clearTimer() {
        stopTimerOtpLD.postValue(true)
    }

    fun isCameraPermissionGranted() =
            PermissionUtil.checkPermission(getApplication(), Manifest.permission.CAMERA)

    fun verifyRegisterProfile(profileBody: ProfileBody) = resultLiveData {
        commonDataSource.verifyRegisterProfile(profileBody)
    }

    fun createOtp(phone: String) = resultLiveData {
        commonDataSource.createOtp(phone)
    }

    fun notifyOtpCreated(otp: String) {
        createdOtpLD.postValue(otp)
    }

    fun verifyOtp(phoneNumber: String, otpCode: String) = resultLiveData {
        commonDataSource.verifyOtp(
                VerifyOtpBody(
                        phoneNumber,
                        otpCode
                )
        )
    }

    fun notifyOtpVerified() {
        verifiedOtpLD.postValue(true)
    }

    fun uploadFrontImage(phone: String, imageFile: File) = resultLiveData {
        commonDataSource.uploadImage(createTextBody(phone), createTextBody("1"), createFileBody("IMAGE_FILE", imageFile))
    }

    fun uploadBackImage(phone: String, imageFile: File) = resultLiveData {
        commonDataSource.uploadImage(createTextBody(phone), createTextBody("2"), createFileBody("IMAGE_FILE", imageFile))
    }

    fun uploadFaceImage(phone: String, imageFile: File) = resultLiveData {
        commonDataSource.uploadImage(createTextBody(phone), createTextBody("3"), createFileBody("IMAGE_FILE", imageFile))
    }

    fun verifyFace(sourceFaceId1: String, imagePath1: String, sourceFaceId2: String, imagePath2: String) = resultLiveData {
        commonDataSource.verifyFace(sourceFaceId1, imagePath1, sourceFaceId2, imagePath2)
    }

    fun extractCardInfo(body: ExtractCardBody) = resultLiveData { commonDataSource.extractCardInfo(body) }

    fun registerAccount(account: RegisterAccountBody) = resultLiveData {
        commonDataSource.registerCardInfo(account)
    }

    fun updateProfile(account: UpdateProfileBody) = resultLiveData {
        profileDataSource.updateProfile(account)
    }

    fun verifyProfileLD(profile: ProfileBody) = resultLiveData {
        profileDataSource.verifyUpdateProfile(profile)
    }

    fun getTermLink() {
        callApi(call = { commonDataSource.getTermLink() },
                onSuccess = { termLinkLD.postValue(it) },
                isLoading = false)
    }

    fun getTermUrl(): String {
        var termUrl = termLinkLD.value?.link
        if (termUrl == null) {
            termUrl = if (preferenceHelper.getLanguage() == Language.ENGLISH) {
                DEFAULT_TERM_LINK_EN
            } else {
                DEFAULT_TERM_LINK_VI
            }
        }
        return termUrl
    }
}