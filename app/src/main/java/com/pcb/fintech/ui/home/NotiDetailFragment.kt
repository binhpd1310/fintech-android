package com.pcb.fintech.ui.home

import android.os.Bundle
import android.view.View
import com.pcb.fintech.R
import com.pcb.fintech.data.model.response.Notification
import com.pcb.fintech.di.ViewModelFactory
import com.pcb.fintech.eventbus.EventBus
import com.pcb.fintech.eventbus.event.DeletedNotiEvent
import com.pcb.fintech.ui.base.BaseMVVMFragment
import com.pcb.fintech.utils.DateTimeUtil
import com.pcb.fintech.utils.getDateTimeAtHome
import com.pcb.fintech.utils.getTimeAtHome
import kotlinx.android.synthetic.main.fragment_noti_detail.*
import java.util.*

class NotiDetailFragment : BaseMVVMFragment<NotiDetailVM, ViewModelFactory>() {

    override fun getLayoutRes(): Int {
        return R.layout.fragment_noti_detail
    }

    override fun getViewModelType(): Class<NotiDetailVM> {
        return NotiDetailVM::class.java
    }

    override fun extractData(bundle: Bundle) {
        viewModel.notiId = bundle.getInt(EXTRA_NOTI_ID)
    }

    override fun setupViews(view: View) {
        toolbar.setNavigationOnClickListener { sendToNav { it.goBack() } }

        iv_delete_one.setOnClickListener { deleteNoti() }

        viewModel.getNotiById()
    }

    private fun showNoti(notification: Notification) {
        tv_title.text = notification.subject
        tv_content.text = notification.notifyContent

        val createdDate = notification.getCreatedDate() ?: Calendar.getInstance().time
        val txtHour = getTimeAtHome(createdDate, DateTimeUtil.DATE_TEXT_8)
        val txtDate = getDateTimeAtHome(context!!, createdDate)
        tv_send_date.text = String.format("%s %s", txtDate, txtHour)

    }

    private fun deleteNoti() {
        val notiId = viewModel.notiId ?: return
        showDialog(title = null, cancelable = false,
                message = getString(R.string.delete_one_noti_msg),
                positiveText = getString(R.string.ok),
                negativeText = getString(R.string.action_cancel),
                positiveTextColor = R.color.black_1,
                negativeTextColor = R.color.colorPrimary,
                onPositive = {
                    doObserveAll(viewModel.deleteOneNoti(notiId), onSuccess = {
                        EventBus.getInstance().notify(DeletedNotiEvent(notiId))
                        sendToNav { it.goBack() }
                    })
                }, onNegative = {})
    }

    override fun onObserve(): (NotiDetailVM.() -> Unit) = {
        doObserveAll(getNotiByIdLD, onSuccess = { showNoti(it) },
                onError = { showSnackBar(rootView, it) })
    }

    companion object {

        private const val EXTRA_NOTI_ID = "EXTRA_NOTI_ID"

        fun buildArgs(notiId: Int) = Bundle().apply { putInt(EXTRA_NOTI_ID, notiId) }
    }
}