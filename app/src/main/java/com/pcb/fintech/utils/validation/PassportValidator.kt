package com.pcb.fintech.utils.validation

import com.pcb.fintech.R
import javax.inject.Singleton

@Singleton
class PassportValidator : CardValidator {

    override fun validate(cardNumber: String): CardValidationResult {
        var isValid = true
        if (cardNumber.length != MAX_LENGTH) {
            isValid = false
        } else {
            try {
                cardNumber.toLong()
                isValid = false
            } catch (e: Exception) {
                // Do nothing
            }
        }
        return if (isValid) CardValidationResult.CardValid
        else CardValidationResult.CardInvalid(R.string.error_passport_format)
    }

    companion object {
        private const val MAX_LENGTH = 8
    }
}