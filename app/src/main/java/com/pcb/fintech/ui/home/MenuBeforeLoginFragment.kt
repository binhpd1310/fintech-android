package com.pcb.fintech.ui.home

import android.content.Intent
import android.view.View
import com.pcb.fintech.BuildConfig
import com.pcb.fintech.R
import com.pcb.fintech.di.ViewModelFactory
import com.pcb.fintech.ui.base.BaseMVVMFragment
import com.pcb.fintech.ui.base.HasFragmentResult
import com.pcb.fintech.ui.home.menuitem.FeatureId
import com.pcb.fintech.ui.home.menuitem.HomeMenuClickHandler
import com.pcb.fintech.ui.home.menuitem.MenuClickBeforeLoginHandler
import com.pcb.fintech.ui.registration.ConfirmRegisterDialog
import com.pcb.fintech.ui.views.SelectLanguageDialog
import com.pcb.fintech.utils.check
import kotlinx.android.synthetic.main.layout_menu_before_login.*
import kotlinx.android.synthetic.main.menu_item_feature_language.*
import kotlinx.android.synthetic.main.menu_item_feature_report.*
import kotlinx.android.synthetic.main.menu_item_feature_support.*
import kotlinx.android.synthetic.main.menu_item_feature_vote.*

class MenuBeforeLoginFragment : BaseMVVMFragment<HomeVM, ViewModelFactory>() {

    private val mClickHandler: HomeMenuClickHandler by lazy {
        MenuClickBeforeLoginHandler(context!!, { it?.let { startActivity(it) } }, { changeLanguage() })
    }

    override fun getLayoutRes(): Int {
        return R.layout.layout_menu_before_login
    }

    override fun getViewModelType(): Class<HomeVM> {
        return HomeVM::class.java
    }

    override fun viewModelInActivityScope() = true

    override fun setupViews(view: View) {
        tv_register.setOnClickListener { confirmToRegister() }

        tv_login.setOnClickListener { mClickHandler.clickFeature(FeatureId.FT_LOGIN) }

        layout_report.setOnClickListener { mClickHandler.clickFeature(FeatureId.FT_REPORT_ID) }

        layout_support.setOnClickListener { mClickHandler.clickFeature(FeatureId.FT_SUPPORT_ID) }

        layout_vote.setOnClickListener { mClickHandler.clickFeature(FeatureId.FT_RATE_ID) }

        layout_language.setOnClickListener { mClickHandler.clickFeature(FeatureId.FT_LANGUAGE_ID) }

        tv_version.text = getString(R.string.app_version, BuildConfig.VERSION_NAME)
    }

    private fun changeLanguage() {
        val language = viewModel.getLanguage()
        val dialog = SelectLanguageDialog.newInstance(language)
        dialog.setTargetFragment(this, LANGUAGE_CODE)
        dialog.show(parentFragmentManager, null)
    }

    private fun confirmToRegister() {
        val dialog = ConfirmRegisterDialog.newInstance()
        dialog.setTargetFragment(this, CONFIRM_REGISTER_CODE)
        dialog.show(parentFragmentManager, null)
    }

    override fun onFragmentResult(requestCode: Int, action: Int, extraData: Intent?) {
        check({ action == HasFragmentResult.ACTION_OK && requestCode == LANGUAGE_CODE }, ifTrue = func@{
            val language = SelectLanguageDialog.getExtraData(extraData) ?: return@func
            viewModel.changeLanguage(language)
        })
        check({ action == HasFragmentResult.ACTION_OK && requestCode == CONFIRM_REGISTER_CODE }, ifTrue = {
            mClickHandler.clickFeature(FeatureId.FT_REGISTER)
        })
    }

    companion object {

        private const val LANGUAGE_CODE = 100

        private const val CONFIRM_REGISTER_CODE = 101

        fun newInstance() = MenuBeforeLoginFragment()
    }
}