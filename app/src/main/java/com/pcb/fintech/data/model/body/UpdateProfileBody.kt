package com.pcb.fintech.data.model.body

import com.google.gson.annotations.SerializedName

class UpdateProfileBody
constructor(@SerializedName("idType") val idType: Int?,
            @SerializedName("idNumber") val idNumber: String?,
            @SerializedName("issuedDate") val issuedDate: String?,
            @SerializedName("expiredDate") val expiredDate: String?,
            @SerializedName("issuedPlace") val issuedPlace: String?,
            @SerializedName("frontCard") val frontCard: String?,
            @SerializedName("backsideCard") val backsideCard: String?,
            @SerializedName("imagePath") val imagePath: String?,
            @SerializedName("confidence") val confidence: Int?,
            @SerializedName("appUserId") var appUserId: Int? = null)