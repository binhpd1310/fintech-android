package com.pcb.fintech.vnpay

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class VnPayResponse
constructor(var vnp_Amount: Double = 0.0,
            var vnp_BankCode: String = "",
            var vnp_BankTranNo: String = "",
            var vnp_CardType: String = "",
            var vnp_OrderInfo: String = "",
            var vnp_PayDate: Long = 0,
            var vnp_ResponseCode: String = "",
            var vnp_TmnCode: String = "",
            var vnp_TransactionNo: String = "",
            var vnp_TxnRef: Long = 0,
            var vnp_SecureHashType: String = "",
            var vnp_SecureHash: String = "") : Parcelable