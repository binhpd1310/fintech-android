package com.pcb.fintech.data.model.response

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import com.pcb.fintech.data.model.response.District
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Ward constructor(
        @SerializedName("id") val id: Int? = null,
        @SerializedName("name") val name: String? = null,
        @SerializedName("type") val type: String? = null,
        @SerializedName("districtId") val districtId: Int? = null,
        @SerializedName("district") val district: District? = null) : Parcelable {

    var selected: Boolean = false
}