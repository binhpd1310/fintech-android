package com.pcb.fintech.ui.profile.viewholder

import android.view.View
import com.pcb.fintech.data.model.response.Ward
import kotlinx.android.synthetic.main.item_place.*
import com.pcb.fintech.ui.adapter.MultiTypeVH

class WardVH(private val itemV: View,
             onClickListener: (item: Ward) -> Unit) : MultiTypeVH<Ward>(itemV) {

    init {
        itemV.setOnClickListener { onClickListener.invoke(itemV.tag as Ward) }
    }

    override fun bindView(item: Ward) {
        itemV.tag = item
        tv_place.text = item.name
        iv_select.visibility = if (item.selected) View.VISIBLE else View.GONE
    }

}