package com.pcb.fintech.data.source.local

import android.content.Context
import android.content.SharedPreferences
import androidx.core.content.edit
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.pcb.fintech.data.model.response.*
import com.pcb.fintech.data.source.remote.config.TokenType
import com.pcb.fintech.language.Language
import com.pcb.fintech.utils.GsonUtil
import com.pcb.fintech.utils.deleteCache

class PreferenceHelper(private val context: Context) {

    private var mPref: SharedPreferences =
            context.getSharedPreferences("FintechAppStorage", Context.MODE_PRIVATE)

    fun saveAccessToken(token: String?) {
        token ?: return
        mPref.edit(commit = true) { putString(ACCESS_TOKEN, token) }
    }

    fun getAccessToken(): String {
        return mPref.getString(ACCESS_TOKEN, "") ?: ""
    }

    /**
     * time: in milliseconds
     */
    fun saveExpiredTime(time: Long?) {
        time ?: return
        mPref.edit(commit = true) { putLong(TOKEN_EXPIRED_TIME, time) }
    }

    fun getExpiredTime(): Long {
        return mPref.getLong(TOKEN_EXPIRED_TIME, 0L)
    }

    fun isLogin(): Boolean {
        return System.currentTimeMillis() < getExpiredTime()
    }

    fun saveTokenType(tokeType: String?) {
        tokeType ?: return
        mPref.edit(commit = true) { putString(TOKEN_TYPE, tokeType) }
    }

    fun getTokenType(): String {
        val default = TokenType.BASIC.value
        return mPref.getString(TOKEN_TYPE, default) ?: default
    }

    fun saveAccountInfo(accountInfo: AccountInfo?) {
        accountInfo ?: return
        mPref.edit(true) { putString(ACCOUNT_INFO, Gson().toJson(accountInfo)) }
    }

    fun getAccountInfo(): AccountInfo? {
        val accountInfoStr = mPref.getString(ACCOUNT_INFO, "")
        return GsonUtil.parse(accountInfoStr, AccountInfo::class.java)
    }

    fun getAddressInfo(): List<AddressInfo>? {
        return getAccountInfo()?.addresses
    }

    fun getUserId(): Int? {
        val accountInfo = getAccountInfo()
        return accountInfo?.id
    }

    fun saveProvinces(provinces: List<Province>?) {
        provinces ?: return
        mPref.edit { putString(KEY_PROVINCE, Gson().toJson(provinces)) }
    }

    fun getProvinces(): List<Province> {
        val json = mPref.getString(KEY_PROVINCE, "")
        return GsonUtil.parse(json, object : TypeToken<List<Province>>() {}.type) ?: mutableListOf()
    }

    fun saveDistricts(districts: List<District>?) {
        districts ?: return
        mPref.edit { putString(KEY_DISTRICT, Gson().toJson(districts)) }
    }

    fun getDistricts(): List<District> {
        val json = mPref.getString(KEY_DISTRICT, "")
        return GsonUtil.parse(json, object : TypeToken<List<District>>() {}.type) ?: mutableListOf()
    }

    fun saveWards(wards: List<Ward>?) {
        wards ?: return
        mPref.edit { putString(KEY_WARD, Gson().toJson(wards)) }
    }

    fun getWards(): List<Ward> {
        val json = mPref.getString(KEY_WARD, "")
        return GsonUtil.parse(json, object : TypeToken<List<Ward>>() {}.type) ?: mutableListOf()
    }

    fun saveLanguage(language: Language) {
        mPref.edit { putString(LANGUAGE, language.code) }
    }

    fun getLanguage(): Language {
        val code = mPref.getString(LANGUAGE, "")
        return Language.getByCode(code) ?: Language.VIETNAMESE
    }

    fun saveVeriedOtp(isRequire: Boolean) {
        mPref.edit { putBoolean(VERIFIED_OTP, isRequire) }
    }

    fun verifiedOtp(): Boolean {
        return mPref.getBoolean(VERIFIED_OTP, false)
    }

    fun clear() {
        saveAccessToken("")
        saveExpiredTime(0)
        saveTokenType(TokenType.BEARER.value)
        saveAccountInfo(AccountInfo())
        saveProvinces(listOf())
        saveDistricts(listOf())
        saveWards(listOf())
        saveVeriedOtp(false)

        deleteCache(context)
    }

    companion object {
        const val ACCESS_TOKEN = "ACCESS_TOKEN"
        const val TOKEN_TYPE = "TOKEN_TYPE"
        const val TOKEN_EXPIRED_TIME = "TOKEN_EXPIRED_TIME"
        const val ACCOUNT_INFO = "ACCOUNT_INFO"
        const val KEY_PROVINCE = "KEY_PROVINCE"
        const val KEY_DISTRICT = "KEY_DISTRICT"
        const val KEY_WARD = "KEY_WARD"
        const val LANGUAGE = "LANGUAGE"
        const val VERIFIED_OTP = "REQUIRE_OTP"
    }
}