package com.pcb.fintech.data.source.remote.authorization

import com.pcb.fintech.data.source.remote.config.TokenType

interface AuthProvider {

    fun getAuth(tokenType: TokenType?, accessToken: String): String
}