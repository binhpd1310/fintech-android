package com.pcb.fintech.data.model.body

import com.google.gson.annotations.SerializedName

data class VerifyOtpBody constructor(@SerializedName("phoneNumber") val phoneNumber: String,
                                     @SerializedName("otpCode") val code: String)