package com.pcb.fintech.data.model.other

import com.pcb.fintech.R
import com.pcb.fintech.data.model.Consumer2

object ReportStatusDisplay {

    private val valueMap: Map<ReportStatus, Consumer2<Int, Int>> = mapOf(
            ReportStatus.WAITING to Consumer2(R.string.type_requesting, R.color.status_waiting),
            ReportStatus.REJECTED to Consumer2(R.string.type_rejected, R.color.status_rejected),
            ReportStatus.APPROVED to Consumer2(R.string.type_approved, R.color.status_approved),
            ReportStatus.CONFIRM_PAYMENT to Consumer2(R.string.type_confirm_payment, R.color.status_waiting_payment_confirm),
            ReportStatus.PAID to Consumer2(R.string.type_paid, R.color.status_paid),
            ReportStatus.GOT_PDF to Consumer2(R.string.type_got_pdf, R.color.status_got_pdf),
            ReportStatus.PUBLISHED to Consumer2(R.string.type_published, R.color.status_published),
            ReportStatus.SENT to Consumer2(R.string.type_sent, R.color.status_sent),
            ReportStatus.CANCEL to Consumer2(R.string.type_cancel, R.color.status_cancel)
    )

    fun getStringRes(type: ReportStatus): Int {
        return valueMap[type]?.data1 ?: throw IllegalArgumentException("reportStatus is invalid")
    }

    fun getColorRes(type: ReportStatus): Int {
        return valueMap[type]?.data2 ?: throw IllegalArgumentException("reportStatus is invalid")
    }
}