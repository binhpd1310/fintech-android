package com.pcb.fintech.data.model.response

import com.google.gson.annotations.SerializedName

class LoginResponse constructor(@SerializedName("expiresIn") val expiresIn: Long?,
                                @SerializedName("role") val role: String?,
                                @SerializedName("token") val token: String?)