package com.pcb.fintech.data.model.body

import com.google.gson.annotations.SerializedName

class RegisterAccountBody
constructor(@SerializedName("userName") val userName: String?,
            @SerializedName("fullname") val fullname: String?,
            @SerializedName("password") val password: String?,
            @SerializedName("email") val email: String?,
            @SerializedName("phone") val phone: String?,
            @SerializedName("birthday") val birthday: String?,
            @SerializedName("confidence") val confidence: Float?,
            @SerializedName("idType") val idType: Int?,
            @SerializedName("frontCard") val frontCard: String?,
            @SerializedName("backsideCard") val backsideCard: String?,
            @SerializedName("imagePath") val imagePath: String?,
            @SerializedName("idNumber") val idNumber: String?,
            @SerializedName("issuedDate") val issuedDate: String?,
            @SerializedName("expiredDate") val expiredDate: String?,
            @SerializedName("issuedPlace") val issuedPlace: String?,
            @SerializedName("identityFaceId") val identityFaceId: String?,
            @SerializedName("portraitFaceId") val portraitFaceId: String?)