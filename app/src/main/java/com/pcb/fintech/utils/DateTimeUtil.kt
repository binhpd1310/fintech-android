package com.pcb.fintech.utils

import android.content.Context
import com.pcb.fintech.R
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

object DateTimeUtil {

    const val DATE_TEXT_1 = "yyyy-MM-dd'T'HH:mm:ss'Z'"
    const val DATE_TEXT_2 = "dd/MM/yyyy"
    const val DATE_TEXT_3 = "yyyy-MM-dd'T'HH:mm:ss"
    const val DATE_TEXT_4 = "HH:mm dd/MM/yyyy"
    const val DATE_TEXT_5 = "yyyy-MM-dd-HHmmss"
    const val DATE_TEXT_6 = "HH:mm"
    const val DATE_TEXT_7 = "dd/MM/yyyy HH:mm:ss"
    const val DATE_TEXT_8 = "HH:mm:ss"

    fun formatDate(date: Date?, format: String): String {
        date ?: return ""
        val formatter = SimpleDateFormat(format, Locale.US)
        return formatter.format(date)
    }

    fun toDate(time: String?, format: String): Date? {
        time ?: return null
        return try {
            val formatter = SimpleDateFormat(format, Locale.US)
            formatter.parse(time)
        } catch (e: ParseException) {
            e.printStackTrace()
            null
        }
    }
}

fun Date?.toDateString(format: String): String? {
    this ?: return null
    val formatter = SimpleDateFormat(format, Locale.US)
    return formatter.format(this)
}

fun String?.toDate(format: String): Date? {
    this ?: return null
    return try {
        val formatter = SimpleDateFormat(format, Locale.US)
        formatter.parse(this)
    } catch (e: ParseException) {
        e.printStackTrace()
        null
    }
}

fun getTimeAtHome(date: Date, timeFormat: String = DateTimeUtil.DATE_TEXT_6): String {
    return DateTimeUtil.formatDate(date, timeFormat)
}

fun getDateTimeAtHome(context: Context, date: Date): String {
    val dayOfWeeks = context.resources.getStringArray(R.array.day_of_weeks)
    val months = context.resources.getStringArray(R.array.months)

    val calendar = Calendar.getInstance().apply { time = date }
    val dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK)
    val dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH)
    val month = calendar.get(Calendar.MONTH)
    val year = calendar.get(Calendar.YEAR)
    val format = context.getString(R.string.date_time_at_home_format)
    return String.format(format, dayOfWeeks[dayOfWeek - 1], dayOfMonth, months[month], year)
}