package com.pcb.fintech.ui.profile

import android.Manifest
import android.app.Application
import android.graphics.Bitmap
import android.net.Uri
import android.util.Base64
import androidx.lifecycle.MutableLiveData
import com.pcb.fintech.data.model.Consumer3
import com.pcb.fintech.data.model.body.ProfileBody
import com.pcb.fintech.data.model.response.AccountInfo
import com.pcb.fintech.data.model.response.District
import com.pcb.fintech.data.model.response.Province
import com.pcb.fintech.data.model.response.Ward
import com.pcb.fintech.data.source.CommonDataSource
import com.pcb.fintech.data.source.ProfileDataSource
import com.pcb.fintech.data.source.local.PreferenceHelper
import com.pcb.fintech.ui.base.BaseViewModel
import com.pcb.fintech.utils.*
import java.io.File

class SettingUserVM
constructor(application: Application, schedulerProvider: SchedulerProvider,
            private val preferenceHelper: PreferenceHelper,
            private val commonDataSource: CommonDataSource,
            private val profileDataSource: ProfileDataSource)
    : BaseViewModel(application, schedulerProvider) {

    internal val base64EncoderLD = MutableLiveData<String>()
    internal val base64DecoderLD = MutableLiveData<Bitmap>()

    val prepareDataLD = MutableLiveData<Consumer3<List<Province>, List<District>, List<Ward>>>()

    fun isCameraPermissionGranted() =
            PermissionUtil.checkPermission(getApplication(), Manifest.permission.CAMERA)

    fun getAccountInfo() = preferenceHelper.getAccountInfo()

    fun getAccountInfoLD() = resultLiveData(isLoading = false) { profileDataSource.getProfileData() }

    fun prepareData() {
        callApi(call1 = { commonDataSource.getProvinces() },
                call2 = { commonDataSource.getDistricts() },
                call3 = { commonDataSource.getWards() },
                transform = { data1, data2, data3 -> Consumer3(data1, data2, data3) },
                onSuccess = { prepareDataLD.postValue(it) },
                onFailed = null,
                loading = true)
    }

    fun updateAccountInfoLD(accountInfo: AccountInfo) = resultLiveData {
        profileDataSource.updateAccountInfo(accountInfo)
    }

    fun verifyProfileLD(profile: ProfileBody) = resultLiveData {
        profileDataSource.verifyUpdateProfile(profile)
    }

    fun decodeBase64(base64String: String?) {
        base64String ?: return
        launchDataLoad {
            val bitmap = Base64Image.decodeBase64(base64String)
            base64DecoderLD.postValue(bitmap)
        }
    }

    fun processNewFaceImage(imageUrl: String?) {
        imageUrl ?: return
        val uri = Uri.parse(imageUrl) ?: return
        launchDataLoad {
            val bitmap = ImageUtil.getBitmapFromUri(getContext(), uri) ?: return@launchDataLoad
            val tempBitmap = ImageUtil.resizeBitmap(bitmap, 512f)
            val imageBase64 = Base64Image.getInstance(getContext())
            val imageBase64String = imageBase64.encodeImageToString(tempBitmap, Base64.NO_WRAP)
            base64EncoderLD.postValue(imageBase64String)
            try {
                imageBase64.clear()
                bitmap.recycle()
            } catch (e: Exception) {
                LogUtil.error(e)
            }
            decodeBase64(imageBase64String)
        }
    }

    fun processNewFaceImage(file: File) {
        launchDataLoad {
            val bitmap = ImageUtil.fixOrientationBugOfProcessedBitmap(file, getContext())
                    ?: return@launchDataLoad
            val tempBitmap = ImageUtil.resizeBitmap(bitmap, 512f)
            val imageBase64 = Base64Image.getInstance(getContext())
            val imageBase64String = imageBase64.encodeImageToString(tempBitmap, Base64.NO_WRAP)
            base64EncoderLD.postValue(imageBase64String)
            try {
                imageBase64.clear()
                bitmap.recycle()
            } catch (e: Exception) {
                LogUtil.error(e)
            }
            decodeBase64(imageBase64String)
        }
    }
}