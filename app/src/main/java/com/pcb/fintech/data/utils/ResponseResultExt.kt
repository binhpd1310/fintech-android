package com.pcb.fintech.data.utils

import com.pcb.fintech.data.model.ResponseResult

object ResponseResultExt {

    fun <T> getResult(data: T): ResponseResult<T> =
        ResponseResult.Success(data)
}