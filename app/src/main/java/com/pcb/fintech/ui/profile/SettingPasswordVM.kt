package com.pcb.fintech.ui.profile

import android.app.Application
import com.pcb.fintech.data.source.ProfileDataSource
import com.pcb.fintech.data.source.local.PreferenceHelper
import com.pcb.fintech.data.model.body.ChangePasswordBody
import com.pcb.fintech.ui.base.BaseViewModel
import com.pcb.fintech.utils.SchedulerProvider

class SettingPasswordVM
constructor(application: Application, schedulerProvider: SchedulerProvider,
            private val preferenceHelper: PreferenceHelper,
            private val profileDataSource: ProfileDataSource) : BaseViewModel(application, schedulerProvider) {

    fun getUserId() = preferenceHelper.getUserId()

    fun updatePasswordLD(body: ChangePasswordBody) = resultLiveData {
        profileDataSource.updatePassword(body)
    }
}
