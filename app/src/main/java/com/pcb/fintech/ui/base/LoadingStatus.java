package com.pcb.fintech.ui.base;

public enum LoadingStatus {
    SHOW_LOADING,
    HIDE_LOADING
}
