package com.pcb.fintech.data.source

import android.content.Context
import com.pcb.fintech.R
import com.pcb.fintech.data.model.ResponseResult
import com.pcb.fintech.data.model.body.AddressesBody
import com.pcb.fintech.data.model.body.ChangePasswordBody
import com.pcb.fintech.data.model.body.ProfileBody
import com.pcb.fintech.data.model.body.UpdateProfileBody
import com.pcb.fintech.data.model.response.AccountInfo
import com.pcb.fintech.data.model.response.ProfileVerificationResult
import com.pcb.fintech.data.model.response.SecretQuestionBody
import com.pcb.fintech.data.source.local.PreferenceHelper
import com.pcb.fintech.data.source.remote.ApiService
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ProfileDataSource @Inject
constructor(private val context: Context,
            private val apiService: ApiService,
            private val preferenceHelper: PreferenceHelper) : BaseDataSource() {

    suspend fun getProfileData(force: Boolean = true): ResponseResult<AccountInfo> {
        val cachedAccountInfo = preferenceHelper.getAccountInfo()
        return if (!force && cachedAccountInfo?.isValid() == true) {
            ResponseResult.Success(cachedAccountInfo)
        } else {
            getResult(call = { apiService.getAccountInfo() },
                    transform = {
                        preferenceHelper.saveAccountInfo(it)
                        it
                    },
                    errorMsgExtractor = { context.getString(R.string.error_account_info_invalid) })
        }
    }

    suspend fun updateAccountInfo(accountInfo: AccountInfo) =
            getResult(call = { apiService.updateAccountInfo(accountInfo) },
                    transform = { it })

    suspend fun getProfileAddress() =
            getResult(call = { apiService.getProfileAddress() },
                    transform = { it })

    suspend fun updatePassword(changePasswordBody: ChangePasswordBody) =
            getResult(call = { apiService.updatePassword(changePasswordBody) },
                    transform = { it })

    suspend fun updateAddresses(addressesBody: AddressesBody) =
            getResult(call = { apiService.updateAddress(addressesBody) },
                    transform = { it })

    suspend fun getSecretQuestions() =
            getResult(call = { apiService.getSecretQuestions() },
                    transform = { it })

    suspend fun updateSecretQuestion(secretQuestionBody: SecretQuestionBody) =
            getResult(call = { apiService.updateSecretQuestion(secretQuestionBody) },
                    transform = { it })

    suspend fun updateProfile(profile: UpdateProfileBody) =
            getResult(call = { apiService.updateProfile(profile) },
                    transform = { it })

    suspend fun verifyUpdateProfile(profile: ProfileBody) =
            getResult(call = { apiService.verifyUpdateProfile(profile) },
                    transform = { ProfileVerificationResult.fromValue(it) })
}