package com.pcb.fintech.utils.validation

import java.util.regex.Pattern

class PasswordValidator {

    fun isPhoneValid(phoneNumber: CharSequence?): Boolean {
        return (!phoneNumber.isNullOrEmpty()
                && phoneNumber.length >= 8
                && pattern1.matcher(phoneNumber).find()
                && pattern2.matcher(phoneNumber).find()
                && pattern3.matcher(phoneNumber).find()
                && pattern4.matcher(phoneNumber).find())
    }

    companion object {

        private val pattern1 = Pattern.compile("[$&+,:;=\\\\?@#|/'<>.^*()%!-]")
        private val pattern2 = Pattern.compile("[a-z]")
        private val pattern3 = Pattern.compile("[A-Z]")
        private val pattern4 = Pattern.compile("[0-9]")
    }

}