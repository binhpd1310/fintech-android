package com.pcb.fintech.ui.views

import android.graphics.Typeface
import android.text.InputFilter
import android.text.InputType
import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.method.LinkMovementMethod
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.StringRes
import androidx.core.content.ContextCompat

fun TextView?.getTrimText(): String {
    this ?: return ""
    return this.text.toString().trim(' ')
}

fun TextView?.getTrimTextOrNull(): String? {
    this ?: return null
    val text = this.text.toString().trim(' ')
    return if (text.isEmpty()) null
    else text
}

fun TextView?.isNullOrEmpty(): Boolean {
    this ?: return true
    return this.getTrimText().isEmpty()
}

fun TextView?.setMaxLength(length: Int) {
    if (length < 0) return
    val filterArray = arrayOfNulls<InputFilter>(1)
    filterArray[0] = InputFilter.LengthFilter(length)
    this?.filters = filterArray
}

fun EditText?.showOrHidePassword(effectIcon: ImageView?) {
    this ?: return
    val isSelected = effectIcon?.isSelected ?: true
    if (isSelected) {
        this.inputType =
                InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD
    } else {
        this.inputType = InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD
    }
    effectIcon?.isSelected = !isSelected
    this.setSelection(this.length())
}

fun TextView?.visibleAndSet(text: String) {
    this ?: return
    this.setText(text)
    this.visibility = View.VISIBLE
}

fun setVisibility(visibility: Int, vararg views: View?) {
    for (item in views) item?.visibility = visibility
}

fun View?.gone() {
    this?.visibility = View.GONE
}

fun View?.visible() {
    this?.visibility = View.VISIBLE
}

fun TextView.setSpannable(fullText: String,
                          textClickable: String,
                          spanTextColor: Int,
                          isUnderLine: Boolean? = null,
                          onClick: (() -> Unit)? = null) {
    val firstIndex = fullText.indexOf(textClickable)
    if (firstIndex == -1) return

    val clickableSpan = CustomClickableSpan(onClickSpan = { onClick?.invoke() },
            typeface = Typeface.create(this.typeface, Typeface.NORMAL),
            color = spanTextColor,
            isUnderLine = isUnderLine)

    val spannable = SpannableStringBuilder(fullText)
    spannable.setSpan(clickableSpan, firstIndex, firstIndex + textClickable.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)

    this.movementMethod = LinkMovementMethod.getInstance()
    this.text = spannable
}

fun TextView.setSpannable(fullText: String,
                          subText: Array<String>,
                          subTextColor: Array<Int>,
                          isUnderLines: Array<Boolean>? = null,
                          onClicks: Array<(() -> Unit)?>? = null) {
    val spannable = SpannableStringBuilder(fullText)
    for ((index, content) in subText.withIndex()) {
        val firstIndex = fullText.indexOf(content)
        if (firstIndex != -1 && content.isNotEmpty()) {
            val clickableSpan = CustomClickableSpan(onClickSpan = { onClicks?.get(index) },
                    typeface = Typeface.create(this.typeface, Typeface.NORMAL),
                    color = ContextCompat.getColor(this.context, subTextColor[index]),
                    isUnderLine = isUnderLines?.get(index))
            spannable.setSpan(clickableSpan, firstIndex, firstIndex + content.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        }
    }
    this.movementMethod = LinkMovementMethod.getInstance()
    this.text = spannable
}