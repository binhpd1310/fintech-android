package com.pcb.fintech.ui.home

import android.graphics.Bitmap
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.pcb.fintech.R
import com.pcb.fintech.data.model.response.Notification
import com.pcb.fintech.di.ViewModelFactory
import com.pcb.fintech.ui.adapter.MultiTypeAdapter
import com.pcb.fintech.ui.adapter.ViewTypeData
import com.pcb.fintech.ui.base.BaseMVVMFragment
import com.pcb.fintech.ui.base.OpenActivity
import com.pcb.fintech.ui.home.viewholder.NotificationVH
import com.pcb.fintech.ui.views.EndlessRecyclerViewScrollListener
import com.pcb.fintech.ui.views.gone
import com.pcb.fintech.ui.views.visible
import kotlinx.android.synthetic.main.fragment_notification.*

class NotificationFragment : BaseMVVMFragment<HomeVM, ViewModelFactory>() {

    private lateinit var mScrollListener: EndlessRecyclerViewScrollListener
    private lateinit var mAdapter: MultiTypeAdapter<Any>

    override fun getLayoutRes(): Int {
        return R.layout.fragment_notification
    }

    override fun getViewModelType(): Class<HomeVM> {
        return HomeVM::class.java
    }

    override fun viewModelInActivityScope(): Boolean = true

    override fun setupViews(view: View) {

        iv_back.setOnClickListener { handleBack() }

        iv_delete.setOnClickListener { deleteAll() }

        val layoutManager = LinearLayoutManager(context)
        mScrollListener = object : EndlessRecyclerViewScrollListener(layoutManager) {
            override fun onLoadMore(page: Int, totalItemsCount: Int) {
                viewModel.getNextNotis()
            }
        }

        refresh_layout.setOnRefreshListener { viewModel.getNotisWithRefresh() }

        mAdapter = MultiTypeAdapter.Builder()
                .addViewType(ViewTypeData({ 0 }, { R.layout.item_notification },
                        { Notification::class.java },
                        { _, itemView -> NotificationVH(itemView, onClickItem) }))
                .build()
        rv_data.adapter = mAdapter
        rv_data.layoutManager = layoutManager
        rv_data.addOnScrollListener(mScrollListener)

        viewModel.getNotisWithRefresh()
    }

    private fun deleteAll() {
        val notiList = viewModel.getUpdateNotiListLD().value
        if (notiList.isNullOrEmpty()) return

        showDialog(title = null, cancelable = false,
                message = getString(R.string.delete_all_noti_msg),
                positiveText = getString(R.string.ok),
                negativeText = getString(R.string.action_cancel),
                positiveTextColor = R.color.black_1,
                negativeTextColor = R.color.colorPrimary,
                onPositive = {
                    doObserveAll(viewModel.deleteAllNotifications(), onSuccess = {
                        viewModel.getNotisWithRefresh()
                    })
                }, onNegative = {})
    }

    private val onClickItem: (pos: Int) -> Unit = func@{
        val item = mAdapter.getItem(it)
        if (item is Notification) {
            item.updateReadStatus(true)
            mAdapter.notifyItemChanged(it)

            val notiId = item.id ?: return@func
            val args = NotiDetailFragment.buildArgs(notiId)
            val intent = OpenActivity.getIntent(context, NotiDetailFragment::class.java, args)
            startActivity(intent)
        }
    }

    override fun onObserve(): (HomeVM.() -> Unit) = {
        doObserve(getDecodeAvatarLD()) { fillAvatar(it) }
        doObserve(getUpdateNotiListLD()) { handleGetNotifications(it) }
    }

    private fun fillAvatar(bitmap: Bitmap?) {
        bitmap ?: return
    }

    private fun handleGetNotifications(notifications: List<Notification>) {
        mScrollListener.reset()
        mAdapter.submitList(notifications)
        refresh_layout.isRefreshing = false

        if (notifications.isEmpty()) {
            layout_empty.visible()
        } else {
            layout_empty.gone()
        }
    }

    override fun handleBack(): Boolean {
        viewModel.openHomeTab(0)
        return true
    }

    override fun onDestroyView() {
        rv_data.removeOnScrollListener(mScrollListener)
        super.onDestroyView()
    }

    companion object {

        fun newInstance() = NotificationFragment()
    }
}