package com.pcb.fintech.data.model.body

import com.google.gson.annotations.SerializedName

class ProfileBody
private constructor(@SerializedName("userName") val userName: String? = null,
                    @SerializedName("email") val email: String? = null,
                    @SerializedName("phone") val phone: String? = null,
                    @SerializedName("idNumber") val idNumber: String? = null) {

    companion object {
        fun makeBodyForVerifyInfo(userName: String, email: String?, phone: String): ProfileBody {
            return ProfileBody(userName, email, phone)
        }

        fun makeBodyForVerifyIdNumber(userName: String, idNumber: String): ProfileBody {
            return ProfileBody(userName, null, null, idNumber)
        }
    }

}