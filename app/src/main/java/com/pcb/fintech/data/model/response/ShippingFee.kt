package com.pcb.fintech.data.model.response

import com.google.gson.annotations.SerializedName
import com.pcb.fintech.utils.formatDoubleToLong

data class ShippingFee
constructor(@SerializedName("id") val id: Int? = null,
            @SerializedName("name") val name: String? = null,
            @SerializedName("value") val value: Double? = null) {

    fun getValue(): Long {
        return value.formatDoubleToLong()
    }
}