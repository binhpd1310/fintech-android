package com.pcb.fintech.data.model.response

import com.google.gson.annotations.SerializedName

data class Slide(@SerializedName("id") val id: Int?,
                 @SerializedName("imagePath") val imagePath: String?,
                 @SerializedName("imageLink") val imageLink: String?,
                 @SerializedName("isActive") val isActive: Boolean?)