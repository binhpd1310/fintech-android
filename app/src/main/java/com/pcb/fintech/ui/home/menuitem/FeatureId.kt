package com.pcb.fintech.ui.home.menuitem

enum class FeatureId {
    FT_REPORT_ID,
    FT_SETUP_ID,
    FT_LANGUAGE_ID,
    FT_NOTIFICATION_ID,
    FT_SUPPORT_ID,
    FT_REGISTER_NOTIFICATION_ID,
    FT_RATE_ID,
    FT_LOGOUT_ID,
    FT_LOGIN,
    FT_REGISTER,
    FT_PROFILE
}