package com.pcb.fintech.data.source.remote.authorization

import com.pcb.fintech.data.source.remote.config.ApiParams
import com.pcb.fintech.data.source.remote.config.ParamsName
import okhttp3.Authenticator
import okhttp3.Request
import okhttp3.Response
import okhttp3.Route

class AppAuthenticator(private val authListener: AuthListener,
                       private val apiParams: ApiParams
) : Authenticator {

    private var tryOnCount = 0

    override fun authenticate(route: Route?, response: Response?): Request? {
        if (!authListener.isAppLogin()) {
            logout()
            return null
        }

        if (response?.code() != 401) {
            logout()
            return null
        }

        tryOnCount++

        val newToken = if (tryOnCount < 3) {
            tryOnCount = 0
            refreshJWT()
        } else null

        if (newToken == null) {
            logout()
            return null
        }
        return response.request().newBuilder()
                .header(ParamsName.AUTHORIZATION, apiParams.getAuthorization())
                .build()
    }

    @Synchronized
    private fun refreshJWT(): String? {
        return authListener.refreshToken()
    }

    @Synchronized
    private fun logout() {
        authListener.logout()
    }

    interface AuthListener {
        fun refreshToken(): String?

        fun isAppLogin(): Boolean

        fun logout()
    }
}