package com.pcb.fintech.utils

enum class LinkType(val value: String) {
    TERM("PRI"),
    USER_MANUAL("USG"),
    FAQ("FAQ"),
    FB("FAB"),
    ANDROID_APP("ADR"),
    IOS_APP("IOS"),
    IMAGES("IMG")
}