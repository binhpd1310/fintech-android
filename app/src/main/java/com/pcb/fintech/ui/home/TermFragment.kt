package com.pcb.fintech.ui.home

import android.view.View
import com.pcb.fintech.R
import com.pcb.fintech.data.model.response.LocalLink
import com.pcb.fintech.di.ViewModelFactory
import com.pcb.fintech.eventbus.EventBus
import com.pcb.fintech.eventbus.event.OpenHomeTabEvent
import com.pcb.fintech.ui.base.BaseMVVMFragment
import com.pcb.fintech.utils.configWebView
import kotlinx.android.synthetic.main.fragment_term.*

class TermFragment : BaseMVVMFragment<HomeVM, ViewModelFactory>() {

    override fun getLayoutRes(): Int {
        return R.layout.fragment_term
    }

    override fun getViewModelType(): Class<HomeVM> {
        return HomeVM::class.java
    }

    override fun viewModelInActivityScope() = true

    override fun setupViews(view: View) {

        toolbar.setNavigationOnClickListener { handleBack() }

        webView.configWebView()
    }

    private fun openWebView(localLink: LocalLink) {
        tv_title.text = localLink.title

        webView.loadUrl(localLink.link)
    }

    override fun onObserve(): (HomeVM.() -> Unit) = {
        doObserve(getTermLinkLD()) { openWebView(it) }
    }

    override fun handleBack(): Boolean {
        EventBus.getInstance().notify(OpenHomeTabEvent(0))
        return true
    }

    companion object {

        fun newInstance() = TermFragment()
    }
}