package com.pcb.fintech.ui.registration

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.provider.MediaStore
import android.view.View
import android.widget.AdapterView
import android.widget.Toast
import com.pcb.fintech.R
import com.pcb.fintech.data.model.body.ExtractCardBody
import com.pcb.fintech.data.model.body.ProfileBody
import com.pcb.fintech.data.model.body.RegisterAccountBody
import com.pcb.fintech.data.model.other.CardType
import com.pcb.fintech.data.model.response.CardInfo
import com.pcb.fintech.data.model.response.UploadImage
import com.pcb.fintech.di.ViewModelFactory
import com.pcb.fintech.ui.base.BaseMVVMFragment
import com.pcb.fintech.ui.views.*
import com.pcb.fintech.utils.*
import com.pcb.fintech.utils.ImageUtil.fixOrientationBugOfProcessedBitmap
import com.pcb.fintech.utils.ImageUtil.generateTimeStampPhotoFile
import com.pcb.fintech.utils.ImageUtil.processBeforeUpload
import com.pcb.fintech.utils.ValidationUtil.isValidPassword
import com.pcb.fintech.utils.validation.CardValidationResult
import kotlinx.android.synthetic.main.fragment_registration_step3.*
import kotlinx.android.synthetic.main.layout_register_select_images.*
import kotlinx.android.synthetic.main.layout_register_verify_info.*
import java.io.File
import java.util.*

class RegistrationFragmentStep3 : BaseMVVMFragment<RegistrationVM, ViewModelFactory>() {

    private var frontImageFile: File? = null
    private var backImageFile: File? = null
    private var faceImageFile: File? = null

    private var uploadFrontImage: UploadImage? = null
    private var uploadBackImage: UploadImage? = null
    private var uploadProfileImage: UploadImage? = null

    private var frontImagePath: String? = null
    private var backImagePath: String? = null
    private var faceImagePath: String? = null

    private var cardType: CardType? = null
    private var idCardNumber: String? = null
    private var password: String? = null
    private var confirmPassword: String? = null
    private var issueOfDate: Date? = null
    private var expiredOfDate: Date? = null
    private var dateOfBirth: Date? = null
    private var issueOfPlace: String? = null
    private var profile: ProfileBody? = null
    private var confidence: Float = 0.0F

    private var currentTakeImageCode: Int = -1

    override fun getLayoutRes(): Int {
        return R.layout.fragment_registration_step3
    }

    override fun getViewModelType(): Class<RegistrationVM> {
        return RegistrationVM::class.java
    }

    override fun viewModelInActivityScope(): Boolean = true

    override fun setupViews(view: View) {
        tv_previous.setOnClickListener { viewModel.goPreviousStep() }

        registerEvent()

        viewModel.getTermLink()
    }

    private fun registerEvent() {
        iv_front.setOnClickListener { takePhoto(REQUEST_CODE_FRONT_IMAGE) }
        iv_back.setOnClickListener { takePhoto(REQUEST_CODE_BACK_IMAGE) }
        iv_face.setOnClickListener { takePhoto(REQUEST_CODE_FACE_IMAGE) }
        tv_select_issue_of_date.setOnClickListener { showPickIssueOfDate() }
        tv_select_expire_of_date.setOnClickListener { showPickExpiredOfDate() }
        iv_show_pass.setOnClickListener { tv_password.showOrHidePassword(iv_show_pass) }
        iv_show_confirm_pass.setOnClickListener { tv_confirm_password.showOrHidePassword(iv_show_confirm_pass) }
        tv_select_dob.setOnClickListener { showPickDateOfBirth() }
        tv_clear_id_number.setOnClickListener { edt_number_of_card.setText("") }
        tv_clear_issue_of_place.setOnClickListener { edt_issue_of_place.setText("") }
        tv_clear_fullname.setOnClickListener { edt_fullname.setText("") }
        tv_register.setOnClickListener(OnClickAndHideKeyboard { registerAccount() })

        sp_id_type.onItemSelectedListener = spIdTypeListener

        val textWatcher = EasyTextWatcher(afterTextChanged = { validateInputData() })
        edt_number_of_card.addTextChangedListener(textWatcher)
        tv_password.addTextChangedListener(textWatcher)
        tv_confirm_password.addTextChangedListener(textWatcher)
        tv_issue_of_date.addTextChangedListener(textWatcher)
        tv_expired_of_date.addTextChangedListener(textWatcher)
        edt_issue_of_place.addTextChangedListener(textWatcher)
        cb_agree.setOnCheckedChangeListener { _, _ -> validateCardNumber() }

        val fulltext = getString(R.string.agree_terms_note)
        val linkText = getString(R.string.term_label)
        tv_terms.setSpannable(fulltext, linkText, R.color.colorPrimary, onClick = { clickTerms() })
    }

    private fun clickTerms() {
        AndroidUtil.openWebLink(context!!, viewModel.getTermUrl())
    }

    private val spIdTypeListener = object : SimpleAdapterItemSelectedListener() {
        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            cardType = CardType.fromValue(position)
            edt_number_of_card.setText("")
            setRuleForCardInput(cardType)
            validateInputData()
            extractCardInfo()
        }
    }

    private fun showPickIssueOfDate() {
        val datePickerDialog = newInstanceWithLimit(issueOfDate) {
            issueOfDate = it
            tv_issue_of_date.text = DateTimeUtil.formatDate(it, DateTimeUtil.DATE_TEXT_2)
        }
        datePickerDialog.show(activity?.fragmentManager, "")
    }

    private fun showPickExpiredOfDate() {
        val datePickerDialog = newInstance(expiredOfDate) {
            expiredOfDate = it
            tv_expired_of_date.text = DateTimeUtil.formatDate(it, DateTimeUtil.DATE_TEXT_2)
        }
        datePickerDialog.show(activity?.fragmentManager, "")
    }

    private fun showPickDateOfBirth() {
        val cal = Calendar.getInstance().apply { add(Calendar.YEAR, -14) }
        val datePickerDialog = newInstanceWithLimit(dateOfBirth, cal.time) {
            dateOfBirth = it
            tv_dob.text = DateTimeUtil.formatDate(it, DateTimeUtil.DATE_TEXT_2)
        }
        datePickerDialog.show(activity?.fragmentManager, "")
    }

    private fun takePhoto(requestCode: Int) {
        val appContext = activity?.applicationContext ?: return
        if (requestCode == -1) return

        if (viewModel.isCameraPermissionGranted()) {
            val imageFile = generateTimeStampPhotoFile(appContext, MEDIA_DIR)
            if (requestCode == REQUEST_CODE_FRONT_IMAGE) {
                frontImageFile = imageFile
            }
            if (requestCode == REQUEST_CODE_BACK_IMAGE) {
                backImageFile = imageFile
            }
            if (requestCode == REQUEST_CODE_FACE_IMAGE) {
                faceImageFile = imageFile
            }
            val imageUri = getImageUri(appContext, imageFile)
            val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri)
            startActivityForResult(intent, requestCode)
        } else {
            currentTakeImageCode = requestCode
            val permissions = arrayOf(Manifest.permission.CAMERA)
            requestPermissions(permissions, REQUEST_CAMERA)
        }
    }

    private fun setRuleForCardInput(cardType: CardType?) {
        when (cardType) {
            CardType.IDENTITY_CARD -> edt_number_of_card.setMaxLength(9)
            CardType.ID_CARD -> edt_number_of_card.setMaxLength(12)
            CardType.PASSPORT -> edt_number_of_card.setMaxLength(8)
            else -> edt_number_of_card.setMaxLength(12)
        }
    }

    private fun validateInputData(): Boolean {
        idCardNumber = edt_number_of_card.getTrimText()
        password = tv_password.text.toString()
        confirmPassword = tv_confirm_password.text.toString()
        issueOfPlace = edt_issue_of_place.getTrimText()
        profile = viewModel.getProfileLD().value

        var isValid = true
        if (cardType == null || cardType == CardType.UNKNOWN) isValid = false
        if (profile == null) isValid = false
        if (frontImagePath == null) isValid = false
        if (backImagePath == null) isValid = false
        if (faceImagePath == null) isValid = false
        if (issueOfDate == null) isValid = false
        if (password.isNullOrEmpty()) isValid = false
        if (confirmPassword.isNullOrEmpty()) isValid = false
        if (idCardNumber.isNullOrEmpty()) isValid = false
        if (!cb_agree.isChecked) isValid = false

        tv_register.isEnabled = isValid

        return isValid
    }

    private fun takePhotoCallback(requestCode: Int) {
        if (requestCode == REQUEST_CODE_FRONT_IMAGE) {
            uploadFrontImage()
            return
        }
        if (requestCode == REQUEST_CODE_BACK_IMAGE) {
            uploadBackImage()
            return
        }
        if (requestCode == REQUEST_CODE_FACE_IMAGE) {
            uploadFaceImage()
            return
        }
    }

    /**
     * Request to upload front card image
     */
    private fun uploadFrontImage() {
        val phoneNumber = viewModel.getProfileLD().value?.phone ?: return
        val tempFile = frontImageFile ?: return
        val bitmap = fixOrientationBugOfProcessedBitmap(tempFile, context!!) ?: return
        val imageFile = processBeforeUpload(activity, tempFile, bitmap, MEDIA_DIR) ?: return
        frontImageFile = imageFile

        doObserveAll(viewModel.uploadFrontImage(phoneNumber, imageFile),
                { handleUploadFrontImageResult(it) },
                { handleUploadFrontImageFailed(it) })
    }

    private fun handleUploadFrontImageResult(result: UploadImage) {
        uploadFrontImage = result
        val subPath = result.imagePath
        val imagePath = Constant.DOMAIN + subPath
        iv_front.setImageURI(imagePath.formatPath())
        showSnackBar(rootView, R.string.text_upload_image_successful)
        frontImagePath = subPath
        verifyFace()
        extractCardInfo()
    }

    private fun handleUploadFrontImageFailed(msg: String) {
        uploadFrontImage = null
        frontImageFile = null
        showSnackBar(rootView, msg)
    }

    /**
     * Request to upload back card image
     */
    private fun uploadBackImage() {
        val phoneNumber = viewModel.getProfileLD().value?.phone ?: return
        val tempFile = backImageFile ?: return
        val bitmap = fixOrientationBugOfProcessedBitmap(tempFile, context!!) ?: return
        val imageFile = processBeforeUpload(activity, tempFile, bitmap, MEDIA_DIR) ?: return
        backImageFile = imageFile

        doObserveAll(viewModel.uploadBackImage(phoneNumber, backImageFile!!),
                { handleUploadBackImageResult(it) },
                { handleUploadBackImageFailed(it) })
    }

    private fun handleUploadBackImageResult(result: UploadImage) {
        uploadBackImage = result
        val subPath = result.imagePath
        val imagePath = Constant.DOMAIN + subPath
        iv_back.setImageURI(imagePath.formatPath())
        showSnackBar(rootView, R.string.text_upload_image_successful)
        backImagePath = subPath
        extractCardInfo()
    }

    private fun handleUploadBackImageFailed(msg: String) {
        backImageFile = null
        uploadBackImage = null
        showSnackBar(rootView, msg)
    }

    /**
     * Request to upload face image
     */
    private fun uploadFaceImage() {
        val phoneNumber = viewModel.getProfileLD().value?.phone ?: return
        val tempFile = faceImageFile ?: return
        val bitmap = fixOrientationBugOfProcessedBitmap(tempFile, context!!) ?: return
        val imageFile = processBeforeUpload(activity, tempFile, bitmap, MEDIA_DIR) ?: return
        faceImageFile = imageFile

        doObserveAll(viewModel.uploadFaceImage(phoneNumber, faceImageFile!!),
                { handleUploadFaceImageResult(it) },
                { handleUploadFaceImageFailed(it) })
    }

    private fun handleUploadFaceImageResult(result: UploadImage) {
        val subPath = result.imagePath
        val imagePath = Constant.DOMAIN + subPath
        iv_face.setImageURI(imagePath.formatPath())
        showSnackBar(rootView, R.string.text_upload_image_successful)
        faceImagePath = subPath
        uploadProfileImage = result
        verifyFace()
    }

    private fun handleUploadFaceImageFailed(msg: String) {
        uploadProfileImage = null
        faceImageFile = null
        showSnackBar(rootView, msg)
    }

    /**
     * Request to verify face
     */
    private fun verifyFace() {
        if (uploadFrontImage == null || uploadProfileImage != null) {
            doObserveAll(viewModel.verifyFace(uploadFrontImage?.faceId
                    ?: "", uploadFrontImage?.imagePath ?: "", uploadProfileImage?.faceId
                    ?: "", uploadProfileImage?.imagePath ?: ""),
                    { confidence = it },
                    { showSnackBar(rootView, it) })
        }
    }

    /**
     * Request to extract mock from card
     */
    private fun extractCardInfo() {
        val frontPath = frontImagePath ?: return
        val backPath = backImagePath ?: return
        val type = cardType?.value ?: return
        doObserveAll(viewModel.extractCardInfo(ExtractCardBody(type, frontPath, backPath)),
                { handleExtractCardInfo(it) },
                { showSnackBar(rootView, it) })
    }

    private fun handleExtractCardInfo(result: CardInfo) {
        edt_number_of_card.setText(result.idNumber)
        edt_issue_of_place.setText(result.issuedPlace)
        edt_fullname.setText(result.fullname)
        edt_issue_of_place.setText(result.issuedPlace)
        result?.birthday.let {
            dateOfBirth = DateTimeUtil.toDate(it, DateTimeUtil.DATE_TEXT_3)
            tv_dob.text = DateTimeUtil.formatDate(dateOfBirth, DateTimeUtil.DATE_TEXT_2)
        }

        result?.expiredDate.let {
            expiredOfDate = DateTimeUtil.toDate(it, DateTimeUtil.DATE_TEXT_3)
            tv_expired_of_date.text = DateTimeUtil.formatDate(expiredOfDate, DateTimeUtil.DATE_TEXT_2)
        }

        result?.issuedDate.let {
            issueOfDate = DateTimeUtil.toDate(it, DateTimeUtil.DATE_TEXT_3)
            tv_issue_of_date.text = DateTimeUtil.formatDate(issueOfDate, DateTimeUtil.DATE_TEXT_2)
        }

        validateInputData()
    }

    private fun validateCardNumber(): Boolean {
        val cardNumberText = edt_number_of_card.getTrimText()

        val result = ValidationUtil.isValidCardNumber(cardType, cardNumberText)
        if (result is CardValidationResult.CardInvalid) {
            showSnackBar(rootView, result.msgRes)
            return false
        }
        return true
    }

    /**
     * Register a new account
     */
    private fun registerAccount() {
        if (!validateInputData()) return
        if (!validateCardNumber()) return
        if (!isValidPassword(password)) {
            layout_pw_note.visible()
            return
        } else {
            layout_pw_note.gone()
        }
        if (password != confirmPassword) {
            showSnackBar(rootView, R.string.text_enter_confirm_new_password_invalid)
            return
        }

        val fullName = edt_fullname.getTrimText()
        val issueOfDateStr = issueOfDate.toDateString(DateTimeUtil.DATE_TEXT_3)
        val expiredOfDateStr = expiredOfDate.toDateString(DateTimeUtil.DATE_TEXT_3)
        val dob = dateOfBirth.toDateString(DateTimeUtil.DATE_TEXT_3)
        val accountBody = RegisterAccountBody(userName = profile?.userName,
                fullname = fullName, password = password, email = profile?.email,
                phone = profile?.phone, birthday = dob, confidence = confidence, idType = cardType?.value,
                frontCard = frontImagePath, backsideCard = backImagePath, imagePath = faceImagePath,
                idNumber = idCardNumber, issuedDate = issueOfDateStr, expiredDate = expiredOfDateStr,
                issuedPlace = issueOfPlace, identityFaceId = null, portraitFaceId = null
        )
        doObserveAll(viewModel.registerAccount(accountBody),
                { handleRegisterAccountResult(it) },
                { showToast(it) })
    }

    private fun handleRegisterAccountResult(result: Boolean) {
        if (result) {
            showToast(R.string.text_register_successful, time = Toast.LENGTH_LONG)
            activity?.finish()
        } else {
            showToast(R.string.error_register_failed)
        }
    }

    /**
     * onActivityResult
     */
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            takePhotoCallback(requestCode)
            validateInputData()
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, p: Array<out String>, r: IntArray) {
        if (requestCode == REQUEST_CAMERA) {
            val granted = r[0] == PackageManager.PERMISSION_GRANTED
            if (granted) takePhoto(currentTakeImageCode)
        }
    }

    companion object {
        private const val REQUEST_CAMERA = 100
        private const val REQUEST_CODE_FRONT_IMAGE = 101
        private const val REQUEST_CODE_BACK_IMAGE = 102
        private const val REQUEST_CODE_FACE_IMAGE = 103
        private const val MEDIA_DIR = "media"

        fun newInstance() = RegistrationFragmentStep3()
    }
}