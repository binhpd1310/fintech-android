package com.pcb.fintech.ui.home

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.pcb.fintech.R
import com.pcb.fintech.data.model.response.LocalLink
import com.pcb.fintech.data.model.response.Slide
import com.pcb.fintech.di.ViewModelFactory
import com.pcb.fintech.language.Language
import com.pcb.fintech.ui.adapter.MultiTypeAdapter
import com.pcb.fintech.ui.adapter.SlideHomePagerAdapter
import com.pcb.fintech.ui.adapter.ViewTypeData
import com.pcb.fintech.ui.base.BaseMVVMFragment
import com.pcb.fintech.ui.base.HasFragmentResult
import com.pcb.fintech.ui.base.OpenActivity
import com.pcb.fintech.ui.home.viewholder.ItemNoteVH
import com.pcb.fintech.ui.registration.ConfirmRegisterDialog
import com.pcb.fintech.ui.registration.LoginFragment
import com.pcb.fintech.ui.registration.RegistrationFragment
import com.pcb.fintech.ui.views.SelectLanguageDialog
import com.pcb.fintech.ui.views.SimpleOnPageChangeListener
import com.pcb.fintech.ui.views.setVisibility
import com.pcb.fintech.utils.check
import kotlinx.android.synthetic.main.fragment_home_v2.*

class HomeFragmentV2 : BaseMVVMFragment<HomeVM, ViewModelFactory>() {

    private lateinit var mSlideAdapter: SlideHomePagerAdapter

    private lateinit var mNoteAdapter: MultiTypeAdapter<Any>

    override fun getLayoutRes(): Int {
        return R.layout.fragment_home_v2
    }

    override fun getViewModelType(): Class<HomeVM> {
        return HomeVM::class.java
    }

    override fun viewModelInActivityScope() = true

    override fun setupViews(view: View) {
        toolbar.setNavigationOnClickListener { viewModel.requestOpenLeftMenu() }

        updateUi(viewModel.isLogin())

        tv_language.setOnClickListener { changeLanguage() }

        tv_register.setOnClickListener { confirmToRegister() }

        tv_login.setOnClickListener { goToLogin() }

        setupNotes()

        setLanguage(viewModel.getLanguage())

        delayOnMain({
            doObserveAll(viewModel.getSlides(), onSuccess = { setupSlides(it) })
        }, 500)

        viewModel.getProfile()
    }

    private fun setLanguage(language: Language) {
        tv_language.text = language.code.toUpperCase()
    }

    private fun setupSlides(slides: List<Slide>) {
        mSlideAdapter = SlideHomePagerAdapter(slides)

        view_pager.adapter = mSlideAdapter
        view_pager.addOnPageChangeListener(SimpleOnPageChangeListener { updateIndicator(it) })

        updateIndicator(0)
    }

    private fun updateIndicator(position: Int) {
        layout_indicator.removeAllViews()

        val totalCount = mSlideAdapter.count
        for (pos in 0 until totalCount) {
            val layoutId = if (pos == position) R.layout.indicator_selected else R.layout.indicator_non_selected
            val view = LayoutInflater.from(context).inflate(layoutId, layout_indicator, false)
            layout_indicator.addView(view)
        }
    }

    private fun setupNotes() {
        rv_notes.layoutManager = LinearLayoutManager(context)
        mNoteAdapter = MultiTypeAdapter.Builder()
                .addViewType(ViewTypeData({ 0 }, { R.layout.item_home_note }, { LocalLink::class.java }) { _, itemView: View ->
                    ItemNoteVH(itemView) { viewModel.openWebLink(it) }
                })
                .build()
        rv_notes.adapter = mNoteAdapter
    }

    private fun updateUi(isLogin: Boolean) {
        if (isLogin) {
            setVisibility(View.GONE, tv_language, tv_register, tv_login)
        } else {
            setVisibility(View.VISIBLE, tv_language, tv_register, tv_login)
        }
    }

    override fun onObserve(): (HomeVM.() -> Unit) = {
        doObserve(noteLinksLD()) { mNoteAdapter.submitList(it) }
    }

    /* Handle click action */

    private fun changeLanguage() {
        val dialog = SelectLanguageDialog.newInstance(viewModel.getLanguage())
        dialog.setTargetFragment(this, LANGUAGE_CODE)
        dialog.show(parentFragmentManager, null)
    }

    private fun goToLogin() {
        val intent = OpenActivity.getIntent(getAppContext(), LoginFragment::class.java)
        startActivity(intent)
    }

    private fun goToRegistration() {
        val intent = OpenActivity.getIntent(getAppContext(), RegistrationFragment::class.java)
        startActivity(intent)
    }

    private fun confirmToRegister() {
        val dialog = ConfirmRegisterDialog.newInstance()
        dialog.setTargetFragment(this, CONFIRM_REGISTER_CODE)
        dialog.show(parentFragmentManager, null)
    }

    override fun onFragmentResult(requestCode: Int, action: Int, extraData: Intent?) {
        check({ action == HasFragmentResult.ACTION_OK && requestCode == LANGUAGE_CODE }, ifTrue = func@{
            val language = SelectLanguageDialog.getExtraData(extraData) ?: return@func
            viewModel.changeLanguage(language)
        })
        check({ action == HasFragmentResult.ACTION_OK && requestCode == CONFIRM_REGISTER_CODE }, ifTrue = {
            goToRegistration()
        })
    }

    companion object {

        private const val LANGUAGE_CODE = 100

        private const val CONFIRM_REGISTER_CODE = 101

        fun newInstance() = HomeFragmentV2()
    }
}