package com.pcb.fintech.data.source

import android.app.Application
import android.os.Environment
import com.google.gson.reflect.TypeToken
import com.pcb.fintech.data.model.ResponseResult
import com.pcb.fintech.data.model.body.PaymentBody
import com.pcb.fintech.data.model.body.ReportBody
import com.pcb.fintech.data.model.body.ReportNoteBody
import com.pcb.fintech.data.model.body.SearchReportBody
import com.pcb.fintech.data.model.other.Report
import com.pcb.fintech.data.model.response.ReportsResponse
import com.pcb.fintech.data.source.remote.ApiService
import com.pcb.fintech.utils.AndroidUtil
import com.pcb.fintech.utils.Constant
import com.pcb.fintech.utils.GsonUtil
import com.pcb.fintech.utils.LogUtil
import com.pcb.fintech.vnpay.VnPayResponse
import java.io.File
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class ReportDataSource @Inject
constructor(private val application: Application,
            private val apiService: ApiService) : BaseDataSource() {

    suspend fun createReport(reportBody: ReportBody) =
            getResult(call = { apiService.createReport(reportBody) },
                    transform = { it })

    suspend fun canCreateReport() =
            getResult(call = { apiService.canCreateReport() },
                    transform = { it })

    suspend fun getAllReports(searchReportBody: SearchReportBody) =
            getResult(call = { apiService.getAllReports(searchReportBody) },
                    transform = { it })

    fun getFakeReports(searchReportBody: SearchReportBody): ResponseResult<ReportsResponse> {
        val reportsJson = AndroidUtil.readAssetFile(application, "mock/get_reports.json")
        val reports =
                GsonUtil.parse<List<Report>>(reportsJson, object : TypeToken<List<Report>>() {}.type)
                        ?: mutableListOf()

        val pageSize = searchReportBody.displayPages!!
        val currentPage = searchReportBody.currentPage ?: 0

        val totalPage = reports.size / pageSize + 1
        if (currentPage < totalPage - 1) {
            val result = reports.subList(currentPage * pageSize, (currentPage + 1) * pageSize)
            val reportResponse = ReportsResponse(currentPage, result.size, totalPage, reports.size, result)
            return ResponseResult.Success(reportResponse)
        } else if (currentPage == totalPage - 1) {
            val result = reports.subList(currentPage * pageSize, reports.size)
            val reportResponse = ReportsResponse(currentPage, result.size, totalPage, reports.size, result)
            return ResponseResult.Success(reportResponse)
        }
        return ResponseResult.Error(IllegalArgumentException())
    }

    suspend fun updateReportNote(reportNoteBody: ReportNoteBody) =
            getResult(call = { apiService.updateReportNote(reportNoteBody) },
                    transform = { it })

    suspend fun getReportById(reportId: Int) =
            getResult(call = { apiService.getReportById(reportId) },
                    transform = { it })

    suspend fun deleteReportById(reportId: Int) =
            getResult(call = { apiService.deleteReportById(reportId) },
                    transform = { it })

    suspend fun getPaymentReport(reportId: Int) =
            getResult(call = { apiService.getPaymentReport(reportId) },
                    transform = { it })

    suspend fun paymentOffline(body: PaymentBody) =
            getResult(call = { apiService.paymentOffline(body) },
                    transform = { it })

    suspend fun paymentOnline(body: PaymentBody) =
            getResult(call = { apiService.paymentOnline(body) },
                    transform = { it })

    suspend fun paymentOnlineStatus(vnPayResponse: VnPayResponse) =
            getResult(call = {
                apiService.paymentOnlineStatus(vnPayResponse.vnp_Amount, vnPayResponse.vnp_BankCode,
                        vnPayResponse.vnp_BankTranNo, vnPayResponse.vnp_CardType,
                        vnPayResponse.vnp_OrderInfo, vnPayResponse.vnp_PayDate,
                        vnPayResponse.vnp_ResponseCode, vnPayResponse.vnp_TmnCode,
                        vnPayResponse.vnp_TransactionNo, vnPayResponse.vnp_TxnRef,
                        vnPayResponse.vnp_SecureHashType, vnPayResponse.vnp_SecureHash)
            }, transform = { it })

    suspend fun getShippingFee() =
            getResult(call = { apiService.getShippingFee() },
                    transform = { it })

    /**
     * Downloads report and saves into cache.
     * All content in cache will be remove after finishing the session of user
     */
    suspend fun downloadReportCache(reportId: Int, reportKey: String): ResponseResult<File> {
        val fileName = String.format(Constant.REPORT_FILE_NAME_FORMAT, reportKey) + Constant.REPORT_FILE_EXTENSION
        val filePath = application.cacheDir.absolutePath + "/" + fileName
        val foundFile = File(filePath)
        return if (foundFile.exists()) {
            ResponseResult.Success(foundFile)
        } else {
            getResult(call = { apiService.downloadReport(reportId) },
                    transform = func@{
                        val reportFile = File(application.cacheDir, fileName)
                        val fileOS = reportFile.outputStream().apply {
                            use { output -> it.byteStream().copyTo(output) }
                        }
                        try {
                            fileOS.flush()
                            fileOS.close()
                        } catch (e: Exception) {
                            LogUtil.error(e)
                        }
                        return@func reportFile
                    })
        }
    }

    /**
     * Downloads report and saves into phone storage
     */
    suspend fun downloadReport(reportId: Int, reportKey: String): ResponseResult<File> {
        val storagePath = Environment.getExternalStorageDirectory().absolutePath
        val appFolderString = storagePath + "/" + Constant.APP_FOLDER_REPORT
        val appFolder = File(appFolderString)
        if (!appFolder.exists()) {
            appFolder.mkdir()
        }
        val fileName = String.format(Constant.REPORT_FILE_NAME_FORMAT, reportKey) + Constant.REPORT_FILE_EXTENSION
        val filePath = "$appFolderString/$fileName"
        val foundFile = File(filePath)
        if (foundFile.exists()) {
            return ResponseResult.Success(foundFile)
        }
        return getResult(call = { apiService.downloadReport(reportId) },
                transform = func@{
                    val parentFile = File(appFolderString)
                    val reportFile = File(parentFile, fileName)
                    val fileOS = reportFile.outputStream().apply {
                        use { output -> it.byteStream().copyTo(output) }
                    }
                    try {
                        fileOS.flush()
                        fileOS.close()
                    } catch (e: Exception) {
                        LogUtil.error(e)
                    }
                    return@func reportFile
                })
    }

}