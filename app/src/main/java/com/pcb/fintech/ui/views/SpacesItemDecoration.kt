package com.pcb.fintech.ui.views

import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ItemDecoration

class SpacesItemDecoration(private val space: Int, private val orientation: Int) : ItemDecoration() {
    override fun getItemOffsets(outRect: Rect, view: View,
                                parent: RecyclerView, state: RecyclerView.State) {
        if (orientation == LinearLayoutManager.HORIZONTAL) {
            outRect.right = space
        }
        if (orientation == LinearLayoutManager.VERTICAL) {
            val lm = parent.layoutManager as LinearLayoutManager
            val adapterPosition = (view.layoutParams as RecyclerView.LayoutParams).viewAdapterPosition
            if (adapterPosition == 0) {
                outRect.top = space
            } else {
                outRect.top = space / 2
            }
            if (adapterPosition == lm.itemCount - 1) {
                outRect.bottom = space
            } else {
                outRect.bottom = space / 2
            }
            outRect.left = space
            outRect.right = space
        }
    }

}