package com.pcb.fintech.data.model.other

enum class PaymentMethod(val value: Int) {
    UNKNOWN(0),
    VNPAY(1),
    INTERNET_BANKING(2);

    companion object {
        private val valueMap by lazy {
            return@lazy HashMap<Int, PaymentMethod>().apply {
                for (m in values()) {
                    put(m.value, m)
                }
            }
        }

        fun fromValue(value: Int?): PaymentMethod {
            return valueMap[value] ?: UNKNOWN
        }
    }
}