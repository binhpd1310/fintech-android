package com.pcb.fintech.ui

import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity
import com.pcb.fintech.R
import com.pcb.fintech.utils.getDateTimeAtHome
import com.pcb.fintech.utils.getHomeIntent
import com.pcb.fintech.utils.getTimeAtHome
import kotlinx.android.synthetic.main.activity_splash.*
import java.util.*

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        setupDateTime()

        Handler().postDelayed({ goToNext() }, 1000)
    }

    private fun setupDateTime() {
        tv_hour.text = getTimeAtHome(Calendar.getInstance().time)
        tv_date.text = getDateTimeAtHome(this, Calendar.getInstance().time)
    }

    private fun goToNext() {
        startActivity(getHomeIntent(this))
        finish()
    }
}
