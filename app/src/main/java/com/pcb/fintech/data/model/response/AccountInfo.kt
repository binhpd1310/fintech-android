package com.pcb.fintech.data.model.response

import android.text.TextUtils
import com.google.gson.annotations.SerializedName
import com.pcb.fintech.utils.DateTimeUtil
import java.util.*

data class AccountInfo
constructor(@SerializedName("addresses") val addresses: List<AddressInfo>? = null,
            @SerializedName("backsideCard") val backsideCard: String? = null,
            @SerializedName("birthday") val birthday: String? = null,
            @SerializedName("confidence") val confidence: Float? = null,
            @SerializedName("districtId") val districtId: Int? = null,
            @SerializedName("email") val email: String? = null,
            @SerializedName("frontCard") val frontCard: String? = null,
            @SerializedName("fullname") val fullName: String? = null,
            @SerializedName("gender") val gender: Boolean? = null,
            @SerializedName("id") val id: Int? = null,
            @SerializedName("idNumber") val idNumber: String? = null,
            @SerializedName("idType") val idType: Int? = null,
            @SerializedName("image") val image: String? = null,
            @SerializedName("imagePath") val imagePath: String? = null,
            @SerializedName("issuedDate") val issuedDate: String? = null,
            @SerializedName("issuedPlace") val issuedPlace: String? = null,
            @SerializedName("phone") val phone: String? = null,
            @SerializedName("placeOfBirth") val placeOfBirth: String? = null,
            @SerializedName("provinceId") val provinceId: Int? = null,
            @SerializedName("secretAnswer") val secretAnswer: String? = null,
            @SerializedName("secretQuestionId") val secretQuestionId: Int? = null,
            @SerializedName("userName") val userName: String? = null,
            @SerializedName("wardId") val wardId: Int? = null) {

    constructor() : this(null)

    private var birthdayDate: Date? = null
    private var issuedOfDate: Date? = null

    fun isValid(): Boolean {
        return id != null
    }

    fun getBirthDayText(format: String): String {
        if (birthdayDate == null) {
            birthdayDate = DateTimeUtil.toDate(birthday, DateTimeUtil.DATE_TEXT_3)
        }
        val date = birthdayDate ?: return ""
        return DateTimeUtil.formatDate(date, format)
    }

    fun getBirthDay(): Date? {
        if (birthdayDate == null) {
            birthdayDate = DateTimeUtil.toDate(birthday, DateTimeUtil.DATE_TEXT_3)
        }
        return birthdayDate
    }

    fun getIssuedOfDateText(format: String): String {
        if (issuedOfDate == null) {
            issuedOfDate = DateTimeUtil.toDate(issuedDate, DateTimeUtil.DATE_TEXT_3)
        }
        val date = issuedOfDate ?: return ""
        return DateTimeUtil.formatDate(date, format)
    }

    fun getIssuedOfDate(): Date? {
        if (issuedOfDate == null) {
            issuedOfDate = DateTimeUtil.toDate(issuedDate, DateTimeUtil.DATE_TEXT_3)
        }
        return issuedOfDate
    }

    fun validUserInfo(): Boolean {
        if (TextUtils.isEmpty(fullName) || TextUtils.isEmpty(image) || TextUtils.isEmpty(email) || TextUtils.isEmpty(phone) || TextUtils.isEmpty(placeOfBirth) ||
                TextUtils.isEmpty(issuedPlace) || !validInt(provinceId) || !validInt(wardId) || !validInt(districtId) ) {
            return false
        }

        return true
    }

    private fun validInt(number: Int?): Boolean {
        return number != null && number != 0
    }
}