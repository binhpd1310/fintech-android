package com.pcb.fintech.ui.home.viewholder

import android.content.Context
import android.view.View
import androidx.core.content.ContextCompat
import com.pcb.fintech.R
import com.pcb.fintech.data.model.other.Report
import com.pcb.fintech.data.model.other.ReportStatus
import com.pcb.fintech.data.model.other.ReportStatusDisplay
import com.pcb.fintech.ui.adapter.MultiTypeVH
import com.pcb.fintech.ui.views.gone
import com.pcb.fintech.ui.views.visibleAndSet
import com.pcb.fintech.utils.DateTimeUtil
import kotlinx.android.synthetic.main.item_report.*

class ReportVH(val parentContext: Context,
               val view: View,
               onClickItem: ((pos: Int) -> Unit)?) : MultiTypeVH<Report>(view) {

    init {
        view.setOnClickListener { onClickItem?.invoke(adapterPosition) }
    }

    override fun bindView(item: Report) {
        tv_name.text = parentContext.getText(R.string.report_name_default)

        val status = ReportStatus.fromValue(item.reportStatus)
        if (status != null) {
            val statusResId = ReportStatusDisplay.getStringRes(status)
            val statusColorId = ReportStatusDisplay.getColorRes(status)
            tv_status.text = parentContext.getString(statusResId)
            tv_status.setBackgroundColor(ContextCompat.getColor(parentContext, statusColorId))
        } else {
            tv_status.gone()
        }

        tv_request_date.text = item.getCreatedDate(DateTimeUtil.DATE_TEXT_2)

        if (item.note.isNullOrEmpty()) {
            tv_report_note.gone()
        } else {
            tv_report_note.visibleAndSet(parentContext.getString(R.string.text_report_note, item.note))
        }
    }

}