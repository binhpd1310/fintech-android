package com.pcb.fintech.data.source.remote.config

import com.pcb.fintech.data.source.local.PreferenceHelper
import com.pcb.fintech.data.source.remote.authorization.AuthProvider

class ApiParamsImpl(private val prefHelper: PreferenceHelper,
                    private val authProvider: AuthProvider
) : ApiParams {

    override fun getAuthorization(): String {
        val tokenType =
            TokenType.fromValue(prefHelper.getTokenType())
        val accessToken = prefHelper.getAccessToken()
        return authProvider.getAuth(tokenType, accessToken)
    }

    override fun getAccessToken(): String {
        return prefHelper.getAccessToken()
    }

    override fun getContentType(): String {
        return "application/json"
    }

    override fun getCacheControl(): String {
        return "no-cache"
    }

    override fun getAPIKey(): String {
        return ""
    }

}