package com.pcb.fintech.data.model

import com.pcb.fintech.data.source.remote.CustomHttpException

sealed class ResponseResult<out T> {

    open val data: T? = null

    abstract fun <R> map(func: (T) -> R): ResponseResult<R>

    data class Success<out T> constructor(override val data: T) : ResponseResult<T>() {
        override fun <R> map(func: (T) -> R): ResponseResult<R> =
                Success(func(data))
    }

    data class Error constructor(val throwable: Throwable) : ResponseResult<Nothing>() {

        override fun <R> map(func: (Nothing) -> R): ResponseResult<R> = this

        fun getMessageAvoidNull(): String {
            if (throwable is CustomHttpException) {
                return throwable.getErrorMsg()
            }
            return throwable.localizedMessage ?: ""
        }
    }

    object Loading : ResponseResult<Nothing>() {
        override fun <R> map(func: (Nothing) -> R): ResponseResult<R> = this
    }
}