package com.pcb.fintech.ui.base

fun BaseViewModel.getString(stringResId: Int): String {
    return getContext().getString(stringResId)
}