package com.pcb.fintech.data.model.response

import com.google.gson.annotations.SerializedName

class CardInfo
constructor(@SerializedName("idNumber") val idNumber: String?,
            @SerializedName("fullname") val fullname: String?,
            @SerializedName("birthday") val birthday: String?,
            @SerializedName("gender") val gender: String?,
            @SerializedName("issuedDate") val issuedDate: String?,
            @SerializedName("expiredDate") val expiredDate: String?,
            @SerializedName("issuedPlace") val issuedPlace: String?)