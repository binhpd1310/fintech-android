package com.pcb.fintech.ui.views

interface PaginationHelper<T> {
    fun callWithRefresh(callback: (result: T) -> Any)
    fun callNext(callback: (result: T) -> Any)
    fun canCallNext(): Boolean
}