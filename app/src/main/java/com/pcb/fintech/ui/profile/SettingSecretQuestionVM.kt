package com.pcb.fintech.ui.profile

import android.app.Application
import com.pcb.fintech.data.model.response.SecretQuestionBody
import com.pcb.fintech.data.source.ProfileDataSource
import com.pcb.fintech.data.source.local.PreferenceHelper
import com.pcb.fintech.ui.base.BaseViewModel
import com.pcb.fintech.utils.SchedulerProvider

class SettingSecretQuestionVM
constructor(application: Application, schedulerProvider: SchedulerProvider,
            private val preferenceHelper: PreferenceHelper,
            private val profileDataSource: ProfileDataSource) : BaseViewModel(application, schedulerProvider) {

    fun getUserId() = preferenceHelper.getUserId()

    fun getSecretQuestionsLD() = resultLiveData { profileDataSource.getSecretQuestions() }

    fun updateSecretQuestionLD(secretQuestionBody: SecretQuestionBody) = resultLiveData {
        profileDataSource.updateSecretQuestion(secretQuestionBody)
    }

    fun getAccountInfoLD() = resultLiveData(false) { profileDataSource.getProfileData() }
}
