package com.pcb.fintech.ui.base

interface ViewModelObserver {
    fun onCleared()
}