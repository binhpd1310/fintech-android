package com.pcb.fintech.data.source.remote.authorization

import com.pcb.fintech.data.source.remote.config.TokenType
import com.pcb.fintech.utils.Constant
import okhttp3.Credentials

class AppAuthProvider : AuthProvider {

    override fun getAuth(tokenType: TokenType?, accessToken: String): String {
        if (tokenType == TokenType.BEARER) {
            return "Bearer $accessToken"
        }
        if (tokenType == TokenType.BASIC) {
            return Credentials.basic(Constant.AUTH_USERNAME, Constant.AUTH_PASSWORD)
        }
        return accessToken
    }
}