package com.pcb.fintech.ui.registration

import android.content.Intent
import android.view.View
import com.pcb.fintech.R
import com.pcb.fintech.di.ViewModelFactory
import com.pcb.fintech.language.Language
import com.pcb.fintech.ui.base.BaseMVVMFragment
import com.pcb.fintech.ui.base.HasFragmentResult
import com.pcb.fintech.ui.base.OpenActivity
import com.pcb.fintech.ui.views.*
import com.pcb.fintech.utils.getHomeIntent
import kotlinx.android.synthetic.main.fragment_login.*

class LoginFragment : BaseMVVMFragment<LoginVM, ViewModelFactory>() {

    override fun getLayoutRes(): Int {
        return R.layout.fragment_login
    }

    override fun getViewModelType(): Class<LoginVM> {
        return LoginVM::class.java
    }

    override fun setupViews(view: View) {
        viewModel.clearLocal()

        toolbar.setNavigationOnClickListener { sendToNav { it.goBack() } }

        tv_language.setOnClickListener(OnClickAndHideKeyboard { changeLanguage() })

        val textWatcher = EasyTextWatcher(afterTextChanged = { validateInputData() })
        edt_login_name.addTextChangedListener(textWatcher)
        edt_password.addTextChangedListener(textWatcher)

        tv_register_now.setOnClickListener(OnClickAndHideKeyboard {
            confirmToRegister()
        })

        tv_clear_username.setOnClickListener { edt_login_name.setText("") }

        iv_show_pass.setOnClickListener { edt_password.showOrHidePassword(iv_show_pass) }

        tv_forgot_pw.setOnClickListener(OnClickAndHideKeyboard { forgotPassword() })

        tv_login.setOnClickListener(OnClickAndHideKeyboard { clickLogin() })

        setLanguage(viewModel.getLanguage())

        validateInputData()
    }

    private fun setLanguage(language: Language) {
        tv_language.text = language.code.toUpperCase()
    }

    private fun validateInputData() {
        tv_clear_username.visibility = if (edt_login_name.isNullOrEmpty()) View.GONE else View.VISIBLE

        var isValid = true
        if (edt_login_name.text.isNullOrEmpty() || edt_password.text.isNullOrEmpty()) isValid =
                false
        tv_login.isEnabled = isValid
    }

    private fun clickLogin() {
        val username = edt_login_name.getTrimText()
        val password = edt_password.getTrimText()

        doObserveAll(viewModel.login(username, password, true),
                onSuccess = { goToHome() },
                onError = {
                    showDialogMessage(
                            R.string.error_common_title,
                            R.string.error_login_info_not_correct,
                            R.string.close
                    )
                })
    }

    private fun confirmToRegister() {
        val dialog = ConfirmRegisterDialog.newInstance()
        dialog.setTargetFragment(this, CONFIRM_REGISTER_CODE)
        dialog.show(parentFragmentManager, null)
    }

    private fun goToRegistration() {
        val intent = OpenActivity.getIntent(getAppContext(), RegistrationFragment::class.java)
        startActivity(intent)
    }

    private fun forgotPassword() {
        sendToNav { it.openFragment(ForgotPasswordFragment.newInstance()) }
    }

    private fun changeLanguage() {
        val dialog = SelectLanguageDialog.newInstance(viewModel.getLanguage())
        dialog.setTargetFragment(this, LANGUAGE_CODE)
        dialog.show(parentFragmentManager, null)
    }

    override fun onObserve(): (LoginVM.() -> Unit) = {
        doObserve(getUpdateLanguageLD()) { onChangeLanguage() }
    }

    private fun onChangeLanguage() {
        startActivity(getHomeIntent(context!!))
        activity?.finish()
    }

    private fun goToHome() {
        startActivity(getHomeIntent(context!!))
        activity?.finish()
    }

    override fun onFragmentResult(requestCode: Int, action: Int, extraData: Intent?) {
        if (action == HasFragmentResult.ACTION_OK && requestCode == LANGUAGE_CODE) {
            val language = SelectLanguageDialog.getExtraData(extraData) ?: return
            viewModel.changeLanguage(language)
        }
        if (action == HasFragmentResult.ACTION_OK && requestCode == CONFIRM_REGISTER_CODE) {
            goToRegistration()
        }
    }

    companion object {
        private const val LANGUAGE_CODE = 100

        private const val CONFIRM_REGISTER_CODE = 101
    }
}