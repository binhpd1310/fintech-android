package com.pcb.fintech.ui.views

import android.text.Editable
import android.text.TextWatcher

open class EasyTextWatcher(private val onTextChanged: (() -> Unit)? = null,
                           private val beforeTextChanged: (() -> Unit)? = null,
                           private val afterTextChanged: (() -> Unit)? = null) : TextWatcher {
    override fun afterTextChanged(s: Editable?) {
        afterTextChanged?.invoke()
    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
        beforeTextChanged?.invoke()
    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        onTextChanged?.invoke()
    }
}