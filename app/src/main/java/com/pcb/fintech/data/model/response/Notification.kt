package com.pcb.fintech.data.model.response

import com.google.gson.annotations.SerializedName
import com.pcb.fintech.utils.DateTimeUtil
import java.util.*

class Notification
constructor(@SerializedName("id") val id: Int?,
            @SerializedName("userId") val userId: Int?,
            @SerializedName("subject") val subject: String?,
            @SerializedName("notifyContent") val notifyContent: String?,
            @SerializedName("isRead") var isRead: Boolean?,
            @SerializedName("timeCreated") val timeCreated: String?) {

    private var createdDate: Date? = null

    fun getCreatedDateText(format: String): String {
        if (createdDate == null) {
            createdDate = DateTimeUtil.toDate(timeCreated, DateTimeUtil.DATE_TEXT_3)
        }
        val date = createdDate ?: return ""
        return DateTimeUtil.formatDate(date, format)
    }

    fun updateReadStatus(status: Boolean) {
        this.isRead = status
    }

    fun getCreatedDate(): Date? {
        if (createdDate == null) {
            createdDate = DateTimeUtil.toDate(timeCreated, DateTimeUtil.DATE_TEXT_3)
        }
        return createdDate
    }
}