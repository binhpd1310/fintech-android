package com.pcb.fintech.ui.registration

import android.view.View
import com.pcb.fintech.R
import com.pcb.fintech.data.model.body.ProfileBody
import com.pcb.fintech.data.model.response.OtpCreated
import com.pcb.fintech.data.model.response.ProfileVerificationResult
import com.pcb.fintech.di.ViewModelFactory
import com.pcb.fintech.ui.base.BaseMVVMFragment
import com.pcb.fintech.ui.views.EasyTextWatcher
import com.pcb.fintech.ui.views.getTrimText
import com.pcb.fintech.utils.ValidationUtil.USERNAME_MIN_LENGTH
import com.pcb.fintech.utils.ValidationUtil.isValidPhone
import com.pcb.fintech.utils.validation.PhoneNumberValidator.Companion.RESULT_PHONE_VALID
import kotlinx.android.synthetic.main.fragment_registration_step1.*

class RegistrationFragmentStep1 : BaseMVVMFragment<RegistrationVM, ViewModelFactory>() {

    override fun getLayoutRes(): Int {
        return R.layout.fragment_registration_step1
    }

    override fun getViewModelType(): Class<RegistrationVM> {
        return RegistrationVM::class.java
    }

    override fun viewModelInActivityScope(): Boolean = true

    override fun setupViews(view: View) {
        toolbar.setNavigationOnClickListener { viewModel.goPreviousStep() }
        tv_next.setOnClickListener { clickNext() }

        val textWatcher = EasyTextWatcher(afterTextChanged = { validateInputData() })
        edt_login_name.addTextChangedListener(textWatcher)
        edt_login_email.addTextChangedListener(textWatcher)
        edt_login_phone.addTextChangedListener(textWatcher)

        tv_clear_username.setOnClickListener { edt_login_name.setText("") }
        tv_clear_phone.setOnClickListener { edt_login_phone.setText("") }
        tv_clear_email.setOnClickListener { edt_login_email.setText("") }

        validateInputData()
    }

    private fun validateInputData() {
        var isValid = true
        if (edt_login_name.getTrimText().isEmpty()) isValid = false
        if (edt_login_email.getTrimText().isEmpty()) isValid = false
        if (edt_login_phone.getTrimText().isEmpty()) isValid = false
        if (!viewModel.validateEmail(edt_login_email.getTrimText())) isValid = false
        tv_next.isEnabled = isValid
    }

    private fun clickNext() {
        val username = edt_login_name.getTrimText()
        val phoneNumber = edt_login_phone.getTrimText()
        if (username.length < USERNAME_MIN_LENGTH) {
            showSnackBar(rootView, R.string.error_username_min_length)
            return
        }
        if (isValidPhone(phoneNumber) != RESULT_PHONE_VALID) {
            showSnackBar(rootView, R.string.error_phone_invalid)
            return
        }
        val email = edt_login_email.getTrimText()
        val profile = ProfileBody.makeBodyForVerifyInfo(username, email, phoneNumber)
        viewModel.setProfile(profile)

        doObserveAll(viewModel.verifyRegisterProfile(profile),
                onSuccess = { onVerifyProfileResult(it) })
    }

    private fun onVerifyProfileResult(result: ProfileVerificationResult) {
        if (result == ProfileVerificationResult.Success) {
            val profile = viewModel.getProfileLD().value ?: return
            val phoneNumber = profile.phone ?: return
            doObserveAll(viewModel.createOtp(phoneNumber), onSuccess = { onCreateOtpResult(it) })
        } else {
            handleProfileError(result)
        }
    }

    private fun onCreateOtpResult(result: OtpCreated) {
        val otpCode = result.code ?: return
        viewModel.notifyOtpCreated(otpCode)
    }

    private fun handleProfileError(result: ProfileVerificationResult?) {
        val errorMsg: Int = when (result) {
            ProfileVerificationResult.UserNameExisted -> R.string.error_username_exists
            ProfileVerificationResult.EmailExisted -> R.string.error_email_exists
            ProfileVerificationResult.PhoneExisted -> R.string.error_phone_exists
            else -> R.string.error_profile_invalid
        }
        showSnackBar(rootView, errorMsg)
    }

    companion object {

        fun newInstance() = RegistrationFragmentStep1()
    }
}