package com.pcb.fintech

import android.app.Activity
import com.facebook.drawee.backends.pipeline.Fresco
import com.facebook.imagepipeline.backends.okhttp3.OkHttpImagePipelineConfigFactory
import com.facebook.stetho.Stetho
import com.pcb.fintech.data.source.local.PreferenceHelper
import com.pcb.fintech.data.source.remote.authorization.AppAuthenticator
import com.pcb.fintech.data.utils.ServiceUtils
import com.pcb.fintech.di.component.DaggerAppComponent
import com.pcb.fintech.utils.getHomeIntent
import com.yariksoffice.lingver.Lingver
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import dagger.android.support.DaggerApplication
import java.util.*
import javax.inject.Inject


class FintechApp : DaggerApplication(), HasActivityInjector, AppAuthenticator.AuthListener {

    @Inject
    lateinit var preferenceHelper: PreferenceHelper

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        return DaggerAppComponent.builder().create(this)
    }

    @Inject
    lateinit var activityInjector: DispatchingAndroidInjector<Activity>

    override fun onCreate() {
        super.onCreate()

        val locale = Locale(preferenceHelper.getLanguage().code)
        Lingver.init(this, locale)

        val okHttpClient = ServiceUtils.createUnsafeOkHttpClient()
        val config = OkHttpImagePipelineConfigFactory
                .newBuilder(applicationContext, okHttpClient)
                .build()
        Fresco.initialize(this, config)

        Stetho.initialize(
                Stetho.newInitializerBuilder(this)
                        .enableDumpapp(Stetho.defaultDumperPluginsProvider(this))
                        .enableWebKitInspector(Stetho.defaultInspectorModulesProvider(this))
                        .build()
        )
    }

    override fun activityInjector(): DispatchingAndroidInjector<Activity>? {
        return activityInjector
    }

    // AppAuthenticator.AuthListener methods

    override fun refreshToken(): String? {
        return null
    }

    override fun isAppLogin(): Boolean {
        return preferenceHelper.isLogin()
    }

    override fun logout() {
        preferenceHelper.clear()

        val intent = getHomeIntent(this, true)
        startActivity(intent)
    }
}