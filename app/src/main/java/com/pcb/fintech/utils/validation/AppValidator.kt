package com.pcb.fintech.utils.validation

import com.pcb.fintech.data.model.other.CardType
import java.util.regex.Pattern
import javax.inject.Singleton

@Singleton
class AppValidator private constructor() {

    private val cardValidationUtil = CardValidationUtil()

    private val phoneNumberValidator = PhoneNumberValidator()

    private val passwordValidator = PasswordValidator()

    fun isValidEmail(target: CharSequence?): Boolean {
        return target != null && EMAIL_ADDRESS.matcher(target).matches()
    }

    fun isValidPhone(target: CharSequence?): Int {
        return phoneNumberValidator.isPhoneValid(target)
    }

    fun isValidCardNumber(cardType: CardType?, cardNumber: String?): CardValidationResult {
        return cardValidationUtil.validateCardNumber(cardType, cardNumber)
    }

    fun isValidPassword(target: CharSequence?): Boolean {
        return passwordValidator.isPhoneValid(target)
    }

    companion object {

        private var instance: AppValidator? = null

        private val lock = Object()

        fun getInstance(): AppValidator {
            if (instance == null) {
                synchronized(lock) {
                    if (instance == null) {
                        instance = AppValidator()
                    }
                }
            }
            return instance!!
        }

        private val EMAIL_ADDRESS = Pattern.compile(
                "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" +
                        "\\@" +
                        "[a-zA-Z0-9][a-zA-Z\\-]{1,64}" +
                        "(" +
                        "\\." +
                        "[a-zA-Z0-9][a-zA-Z\\-]{1,25}" +
                        ")+"
        )
    }
}