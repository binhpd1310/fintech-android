package com.pcb.fintech.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.viewpager.widget.PagerAdapter
import com.pcb.fintech.R
import com.pcb.fintech.data.model.response.Slide
import com.pcb.fintech.utils.AndroidUtil
import kotlinx.android.synthetic.main.layout_home_slide.view.*

class SlideHomePagerAdapter(private val slides: List<Slide>) : PagerAdapter() {

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val view = LayoutInflater.from(container.context).inflate(R.layout.layout_home_slide, container, false)
        view.iv_slide.setImageURI(slides[position].imagePath)
        view.iv_slide.setOnClickListener { AndroidUtil.openWebLink(container.context, slides[position].imageLink) }
        container.addView(view)
        return view
    }

    override fun destroyItem(container: ViewGroup, position: Int, item: Any) {
        if (item is View) container.removeView(item)
    }

    override fun isViewFromObject(view: View, data: Any): Boolean {
        return view == data
    }

    override fun getCount(): Int {
        return slides.size
    }
}