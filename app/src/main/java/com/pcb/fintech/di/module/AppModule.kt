package com.pcb.fintech.di.module

import android.app.Application
import android.content.Context
import com.pcb.fintech.FintechApp
import com.pcb.fintech.data.source.local.PreferenceHelper
import com.pcb.fintech.data.source.remote.ApiService
import com.pcb.fintech.data.source.remote.authorization.AppAuthProvider
import com.pcb.fintech.data.source.remote.authorization.AppAuthenticator
import com.pcb.fintech.data.source.remote.authorization.AuthProvider
import com.pcb.fintech.data.source.remote.config.ApiParams
import com.pcb.fintech.data.source.remote.config.ApiParamsImpl
import com.pcb.fintech.data.utils.ServiceUtils
import com.pcb.fintech.utils.Constant
import dagger.Module
import dagger.Provides
import okhttp3.Authenticator
import okhttp3.OkHttpClient
import javax.inject.Singleton

@Module
class AppModule {

    @Provides
    fun provideApp(application: FintechApp): Application {
        return application
    }

    @Provides
    fun provideContext(application: FintechApp): Context {
        return application.applicationContext
    }

    @Provides
    @Singleton
    fun providePreferenceHelper(context: Context): PreferenceHelper {
        return PreferenceHelper(context)
    }

    @Provides
    @Singleton
    fun provideAppAuthenticator(application: FintechApp, apiParams: ApiParams): Authenticator {
        return AppAuthenticator(application, apiParams)
    }

    @Provides
    @Singleton
    fun provideAuthorization(): AuthProvider {
        return AppAuthProvider()
    }

    @Provides
    @Singleton
    fun provideAPIParams(prefHelper: PreferenceHelper, authProvider: AuthProvider): ApiParams {
        return ApiParamsImpl(prefHelper, authProvider)
    }

    @Provides
    @Singleton
    fun provideOkHttpClient(apiParams: ApiParams, authenticator: Authenticator): OkHttpClient {
        return ServiceUtils.createOkHttpClient(apiParams, authenticator)
    }

    @Provides
    @Singleton
    fun provideApiService(okHttpClient: OkHttpClient): ApiService {
        return ServiceUtils.createWebService(okHttpClient, Constant.DOMAIN)
    }
}
