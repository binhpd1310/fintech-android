package com.pcb.fintech.ui.profile.viewholder

import android.view.View
import com.pcb.fintech.data.model.response.SecretQuestion
import kotlinx.android.synthetic.main.item_place.*
import com.pcb.fintech.ui.adapter.MultiTypeVH

class SecretQuestionVH(private val itemV: View,
                       onClickListener: (item: SecretQuestion) -> Unit) : MultiTypeVH<SecretQuestion>(itemV) {

    init {
        itemV.setOnClickListener { onClickListener.invoke(itemV.tag as SecretQuestion) }
    }

    override fun bindView(item: SecretQuestion) {
        itemV.tag = item
        tv_place.text = item.name
    }

}