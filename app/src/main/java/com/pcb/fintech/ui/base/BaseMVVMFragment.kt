package com.pcb.fintech.ui.base

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.StringRes
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.snackbar.Snackbar
import com.pcb.fintech.R
import com.pcb.fintech.data.model.ResponseResult
import com.pcb.fintech.data.source.remote.CustomHttpException
import com.pcb.fintech.ui.base.fragment.HasLoadingFragment
import com.pcb.fintech.utils.ThreadUtils.executeDelayOnMain
import dagger.android.support.AndroidSupportInjection
import java.net.HttpURLConnection
import javax.inject.Inject

abstract class BaseMVVMFragment<V : BaseViewModel, VMF : ViewModelProvider.Factory> : HasLoadingFragment() {

    @Inject
    lateinit var viewModelFactory: VMF

    internal val viewModel: V by lazy { createViewModel() }

    private var mDialog: AppDialog? = null

    protected abstract fun getLayoutRes(): Int

    protected abstract fun getViewModelType(): Class<V>

    protected abstract fun setupViews(view: View)

    protected open fun extractData(bundle: Bundle) {}

    protected open fun onObserve(): (V.() -> Unit)? = null

    /** Allow to create ViewModel in activity scope.
     * Default is false */
    protected open fun viewModelInActivityScope(): Boolean = false

    protected fun getAppContext(): Context? = context?.applicationContext

    override fun onAttach(context: Context) {
        super.onAttach(context)
        AndroidSupportInjection.inject(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val bundle = savedInstanceState ?: arguments
        if (bundle != null) {
            extractData(bundle)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(getLayoutRes(), container, false)
        initViewModel()
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupViews(view)
    }

    private fun createViewModel(): V {
        if (::viewModelFactory.isInitialized) {
            return if (viewModelInActivityScope()) {
                injectVMWithActivityScope(viewModelFactory, getViewModelType())
            } else {
                injectVM(viewModelFactory, getViewModelType())
            }
        }
        throw NullPointerException("ViewModelFactory is not initialized!")
    }

    private fun initViewModel() {
        doObserve(viewModel.getLoadingLV()) { status ->
            if (LoadingStatus.SHOW_LOADING == status) {
                showLoading()
            } else if (LoadingStatus.HIDE_LOADING == status) {
                hideLoading()
            }
        }

        doObserve(viewModel.errorLV) { alertData ->
            showDialog(alertData.title, alertData.message, positiveText = alertData.positiveText)
        }

        onObserve()?.invoke(viewModel)
    }

    protected fun <T> doObserve(liveData: LiveData<T>, result: ((data: T) -> Unit)? = null) {
        liveData.observe(viewLifecycleOwner, Observer { result?.invoke(it) })
    }

    protected fun <T> doObserveAll(liveData: LiveData<ResponseResult<T>>,
                                   onSuccess: ((result: T) -> Unit)? = null,
                                   onError: ((message: String) -> Unit)? = null) {
        liveData.observe(viewLifecycleOwner, Observer { result ->
            when (result) {
                is ResponseResult.Loading -> viewModel.postShowLoading()
                is ResponseResult.Error -> {
                    viewModel.postHideLoading()
                    onError?.invoke(result.getMessageAvoidNull()) ?: let {
                        if (result.throwable is CustomHttpException
                                && result.throwable.code() == HttpURLConnection.HTTP_UNAUTHORIZED) {
                            // Do nothing
                        } else {
                            showDefaultDialog()
                        }
                    }
                }
                is ResponseResult.Success -> {
                    viewModel.postHideLoading()
                    onSuccess?.invoke(result.data)
                }
            }
        })
    }

    protected fun delayOnMain(block: () -> Unit, delayMillis: Long = 300) {
        executeDelayOnMain(Runnable { block.invoke() }, delayMillis)
    }

    protected fun showDefaultDialog() {
        showDialogMessage(R.string.error_common_title, R.string.error_common_message, R.string.close)
    }

    protected fun showDialogMessage(@StringRes titleId: Int, @StringRes messageId: Int, @StringRes positiveTextId: Int) {
        showDialog(title = getString(titleId), message = getString(messageId), positiveText = getString(positiveTextId))
    }

    protected fun showDialogMessage(@StringRes messageId: Int, @StringRes positiveTextId: Int) {
        showDialog(title = null, message = getString(messageId), positiveText = getString(positiveTextId))
    }

    protected fun showDialog(title: String?, message: String?, cancelable: Boolean? = null,
                             positiveText: String? = null, negativeText: String? = null,
                             onPositive: (() -> Unit)? = null, onNegative: (() -> Unit)? = null,
                             positiveTextColor: Int? = null, negativeTextColor: Int? = null) {
        if (mDialog?.dialog?.isShowing == true) mDialog?.dismiss()

        val fm = activity?.supportFragmentManager ?: return

        mDialog = AppDialog.newInstance(
                title, message, cancelable = cancelable, positiveText = positiveText,
                negativeText = negativeText, onPositive = onPositive, onNegative = onNegative,
                positiveTextColor = positiveTextColor, negativeTextColor = negativeTextColor)
        mDialog?.show(fm, AppDialog::class.java.simpleName)
    }

    protected fun showSnackBar(parentView: View, @StringRes resId: Int, time: Int = Snackbar.LENGTH_SHORT) {
        Snackbar.make(parentView, getString(resId), time).show()
    }

    protected fun showSnackBar(parentView: View, resString: String, time: Int = Snackbar.LENGTH_SHORT) {
        Snackbar.make(parentView, resString, time).show()
    }

    protected fun showToast(@StringRes resId: Int, time: Int = Toast.LENGTH_SHORT) {
        Toast.makeText(context, getString(resId), time).show()
    }

    protected fun showToast(resString: String, time: Int = Toast.LENGTH_SHORT) {
        Toast.makeText(context, resString, time).show()
    }

    protected fun showCustomToast(@StringRes resId: Int, time: Int = Toast.LENGTH_SHORT) {
        val inflate = LayoutInflater.from(context)
        val view = inflate.inflate(R.layout.layout_custom_toast, null)
        val tv = view.findViewById<TextView>(R.id.message)
        tv.text = getString(resId)

        val toast = Toast(context)
        toast.view = view
        toast.duration = time
        toast.show()
    }

}