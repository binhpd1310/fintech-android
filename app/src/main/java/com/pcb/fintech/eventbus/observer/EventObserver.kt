package com.pcb.fintech.eventbus.observer

import com.pcb.fintech.eventbus.event.*

interface EventObserver {

    fun onEvent(event: CreatedReportEvent)

    fun onEvent(event: CancelledReportEvent)

    fun onEvent(event: DeletedReportEvent)

    fun onEvent(event: PaymentSuccessEvent)

    fun onEvent(event: RequestPaymentEvent)

    fun onEvent(event: UserAccountChangedEvent)

    fun onEvent(event: UpdatedReportEvent)

    fun onEvent(event: OpenHomeTabEvent)

    fun onEvent(event: CloseLeftMenuEvent)

    fun onEvent(event: DeletedNotiEvent)
}