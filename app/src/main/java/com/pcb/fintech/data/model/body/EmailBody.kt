package com.pcb.fintech.data.model.body

import com.google.gson.annotations.SerializedName

class EmailBody constructor(@SerializedName("email") val email: String)