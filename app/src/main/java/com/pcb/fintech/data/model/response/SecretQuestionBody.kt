package com.pcb.fintech.data.model.response

import com.google.gson.annotations.SerializedName

data class SecretQuestionBody constructor(
        @SerializedName("secretQuestionId") val secretQuestionId: Int,
        @SerializedName("secretAnswer") val secretAnswer: String,
        @SerializedName("appUserId") val appUserId: Int)