package com.pcb.fintech.data.model.response

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class District constructor(
        @SerializedName("id") val id: Int? = null,
        @SerializedName("name") val name: String? = null,
        @SerializedName("type") val type: String? = null,
        @SerializedName("provinceId") val provinceId: Int? = null,
        @SerializedName("province") val province: Province? = null,
        @SerializedName("wards") val wards: List<Ward>? = null) : Parcelable {

    var selected: Boolean = false
}