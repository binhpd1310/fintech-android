package com.pcb.fintech.ui.views

import androidx.viewpager.widget.ViewPager

class SimpleOnPageChangeListener(private val handlePageSelected: (pos: Int) -> Unit) : ViewPager.OnPageChangeListener {

    override fun onPageScrollStateChanged(state: Int) {
    }

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
    }

    override fun onPageSelected(position: Int) {
        handlePageSelected(position)
    }
}