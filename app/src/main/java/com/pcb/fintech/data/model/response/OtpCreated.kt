package com.pcb.fintech.data.model.response

import com.google.gson.annotations.SerializedName

data class OtpCreated
constructor(@SerializedName("phoneNumber") val phoneNumber: String?,
            @SerializedName("code") val code: String?)