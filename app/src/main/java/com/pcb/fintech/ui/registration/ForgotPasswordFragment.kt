package com.pcb.fintech.ui.registration

import android.view.View
import com.google.android.material.snackbar.Snackbar
import com.pcb.fintech.R
import com.pcb.fintech.data.model.body.EmailBody
import com.pcb.fintech.di.ViewModelFactory
import com.pcb.fintech.ui.base.BaseMVVMFragment
import com.pcb.fintech.ui.views.EasyTextWatcher
import com.pcb.fintech.ui.views.OnClickAndHideKeyboard
import com.pcb.fintech.ui.views.getTrimText
import com.pcb.fintech.utils.ValidationUtil
import kotlinx.android.synthetic.main.fragment_forgot_password.*

class ForgotPasswordFragment : BaseMVVMFragment<LoginVM, ViewModelFactory>() {

    override fun getLayoutRes(): Int {
        return R.layout.fragment_forgot_password
    }

    override fun getViewModelType(): Class<LoginVM> {
        return LoginVM::class.java
    }

    override fun setupViews(view: View) {
        toolbar.setNavigationOnClickListener { sendToNav { it.goBack() } }

        edt_email.addTextChangedListener(EasyTextWatcher(afterTextChanged = { validateInputData() }))

        tv_clear_email.setOnClickListener { edt_email.setText("") }

        tv_cancel.setOnClickListener { sendToNav { it.goBack() } }

        tv_next.setOnClickListener(OnClickAndHideKeyboard { performNext() })
    }

    private fun validateInputData() {
        var isValid = true
        if (!ValidationUtil.isValidEmail(edt_email.getTrimText())) isValid = false
        tv_next.isEnabled = isValid
    }

    private fun performNext() {
        val email = edt_email.getTrimText()
        doObserveAll(viewModel.forgotPassword(EmailBody(email)),
                onSuccess = { onVerificationResult(it) },
                onError = { showDialog(title = getString(R.string.error_common_title), message = it) })
    }

    private fun onVerificationResult(result: Int) {
        if (result == 1) {
            showSnackBar(rootView, R.string.notify_check_email, Snackbar.LENGTH_LONG)
            delayOnMain({ sendToNav { it.goBack() } }, 2000)
        } else {
            showDialog(title = getString(R.string.error_common_title),
                    message = getString(R.string.error_common_message))
        }
    }

    companion object {

        fun newInstance() = ForgotPasswordFragment()

    }
}