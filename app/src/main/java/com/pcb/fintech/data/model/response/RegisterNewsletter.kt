package com.pcb.fintech.data.model.response

import com.google.gson.annotations.SerializedName

data class RegisterNewsletter(
        @SerializedName("id") val id: Int?,
        @SerializedName("appUserId") val appUserId: Int?,
        @SerializedName("email") val email: String?,
        @SerializedName("status") val status: Int?,
        @SerializedName("regTime") val regTime: String?)