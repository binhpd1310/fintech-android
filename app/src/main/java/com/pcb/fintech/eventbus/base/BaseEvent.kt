package com.pcb.fintech.eventbus.base

abstract class BaseEvent<T>
constructor(private val data: T) : AbstractEvent {

    fun getData(): T {
        return data
    }

}