package com.pcb.fintech.data.model.body

import com.google.gson.annotations.SerializedName

class UpdateNewsletter(@SerializedName("id") val newsletterId: Int?,
                       @SerializedName("email") val email: String?,
                       @SerializedName("status") val status: Int = 1)