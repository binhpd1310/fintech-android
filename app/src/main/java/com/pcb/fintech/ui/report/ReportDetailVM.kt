package com.pcb.fintech.ui.report

import android.Manifest
import android.app.Application
import androidx.lifecycle.LiveData
import com.pcb.fintech.data.model.body.ReportNoteBody
import com.pcb.fintech.data.model.other.Report
import com.pcb.fintech.data.source.ReportDataSource
import com.pcb.fintech.eventbus.event.RequestPaymentEvent
import com.pcb.fintech.eventbus.observer.SimpleEventObserver
import com.pcb.fintech.ui.base.BaseViewModel
import com.pcb.fintech.ui.base.SingleLiveEvent
import com.pcb.fintech.utils.PermissionUtil
import com.pcb.fintech.utils.SchedulerProvider

class ReportDetailVM
constructor(application: Application, schedulerProvider: SchedulerProvider,
            private val reportDataSource: ReportDataSource)
    : BaseViewModel(application, schedulerProvider) {

    private var mReport: Report? = null

    private val _requestPaymentLD = SingleLiveEvent<Boolean>()
    internal val requestPaymentLD: LiveData<Boolean> = _requestPaymentLD

    private val eventBusObserver = SimpleEventObserver(onRequestedPayment = { _requestPaymentLD.postValue(true) })

    init {
        registerEvent(RequestPaymentEvent.TYPE, eventBusObserver)
    }

    fun setReport(report: Report?) {
        this.mReport = report
    }

    fun getReport(): Report? = mReport

    fun getReportId(): Int? = mReport?.id

    fun updateReportNote(reportNoteBody: ReportNoteBody) = resultLiveData { reportDataSource.updateReportNote(reportNoteBody) }

    fun getReportById(reportId: Int) = resultLiveData { reportDataSource.getReportById(reportId) }

    fun downloadReportCache(reportId: Int, reportKey: String) = resultLiveData { reportDataSource.downloadReportCache(reportId, reportKey) }

    fun downloadReport(reportId: Int, reportKey: String) = resultLiveData { reportDataSource.downloadReport(reportId, reportKey) }

    fun isWriteExternalStorageGranted() =
            PermissionUtil.checkPermission(getApplication(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
}