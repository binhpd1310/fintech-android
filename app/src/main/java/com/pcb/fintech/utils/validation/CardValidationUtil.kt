package com.pcb.fintech.utils.validation

import com.pcb.fintech.R
import com.pcb.fintech.data.model.other.CardType
import javax.inject.Singleton

@Singleton
class CardValidationUtil {

    private val validatorMap = mutableMapOf(
            CardType.IDENTITY_CARD to IdentityCardValidator(),
            CardType.ID_CARD to IDCardValidator(),
            CardType.PASSPORT to PassportValidator()
    )

    fun validateCardNumber(cardType: CardType?, cardNumber: String?): CardValidationResult {
        val validator = validatorMap[cardType]
        if (validator == null || cardNumber.isNullOrEmpty()) return CardValidationResult.CardInvalid(R.string.error_common_title)
        return validator.validate(cardNumber)
    }
}