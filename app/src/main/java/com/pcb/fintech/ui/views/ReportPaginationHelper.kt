package com.pcb.fintech.ui.views

import com.pcb.fintech.data.model.ResponseResult
import com.pcb.fintech.data.model.body.SearchReportBody
import com.pcb.fintech.data.model.other.Report
import com.pcb.fintech.data.source.ReportDataSource
import com.pcb.fintech.ui.home.model.ReportInput
import com.pcb.fintech.utils.DateTimeUtil
import com.pcb.fintech.utils.toDateString
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.util.concurrent.atomic.AtomicBoolean
import javax.inject.Inject

class ReportPaginationHelper @Inject constructor(
        private val scope: CoroutineScope,
        private val reportDataSource: ReportDataSource,
        private val reportInput: ReportInput) : PaginationHelper<List<Report>> {

    private var pageLoading = PAGE_DEFAULT

    private var isLoadMore = true

    private var isApiCalling = AtomicBoolean(false)

    private val mReports = mutableListOf<Report>()

    private var mTotalReports = 0

    fun getReports() = mReports

    fun getTotalReports() = mTotalReports

    override fun canCallNext(): Boolean = isLoadMore

    override fun callWithRefresh(callback: (result: List<Report>) -> Any) {
        if (isApiCalling.get()) return

        isApiCalling.set(true)

        isLoadMore = true

        pageLoading = PAGE_DEFAULT

        mReports.clear()

        request(callback)
    }

    override fun callNext(callback: (result: List<Report>) -> Any) {
        if (isApiCalling.get()) return

        if (!canCallNext()) return

        isApiCalling.set(true)

        pageLoading += 1

        request(callback)
    }

    private fun request(callback: (result: List<Report>) -> Any) {
        scope.launch {
            withContext(Dispatchers.IO) {
                val requestBody = SearchReportBody(
                        displayPages = PAGE_SIZE,
                        currentPage = pageLoading,
                        status = reportInput.filterStatus?.value,
                        startDate = reportInput.filterStartDate.toDateString(DateTimeUtil.DATE_TEXT_1),
                        endDate = reportInput.filterEndDate.toDateString(DateTimeUtil.DATE_TEXT_1),
                        keywords = reportInput.keywords)
                val result = reportDataSource.getAllReports(requestBody)

                if (result is ResponseResult.Success) {
                    mTotalReports = result.data.totalNumberOfRecords ?: 0

                    val curPageNumber = result.data.pageNumber ?: 0
                    val totalPageNumber = result.data.totalNumberOfPages ?: 0

                    isLoadMore = curPageNumber < totalPageNumber

                    val reports = result.data.results ?: mutableListOf()
                    mReports.addAll(reports)

                    val data = mutableListOf<Report>().apply { addAll(mReports) }
                    handleResult(data, callback)

                    return@withContext
                } else if (result is ResponseResult.Error) {
                    handleResult(null, callback)

                    return@withContext
                }
                handleResult(null, callback)
            }
        }
    }

    private fun handleResult(result: List<Report>? = null,
                             callback: ((result: List<Report>) -> Any)? = null) {
        isApiCalling.set(false)

        result ?: return
        callback?.invoke(result)
    }

    companion object {
        private const val PAGE_DEFAULT = 1
        private const val PAGE_SIZE = 10
    }
}