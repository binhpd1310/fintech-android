package com.pcb.fintech.language

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
enum class Language(val code: String, val country: String, val display: String) : Parcelable {
    VIETNAMESE("vi", "VN", "Tiếng Việt"),
    ENGLISH("en", "US", "English");

    companion object {
        fun getByCode(code: String?): Language? {
            for (item in values()) if (item.code == code) return item
            return null
        }
    }
}