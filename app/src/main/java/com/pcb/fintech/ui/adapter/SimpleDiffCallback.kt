package com.pcb.fintech.ui.adapter

import androidx.recyclerview.widget.DiffUtil

class SimpleDiffCallback<T>(private val sameItems: ((oldItem: T, newItem: T) -> Boolean)? = null,
                            private val sameContents: ((oldItem: T, newItem: T) -> Boolean)? = null,
                            private val getChangePayload: ((oldItem: T, newItem: T) -> Any?)? = null)
    : DiffUtil.ItemCallback<T>() {

    override fun areItemsTheSame(oldItem: T, newItem: T): Boolean {
        sameItems ?: return oldItem == newItem
        return sameItems.invoke(oldItem, newItem)
    }

    override fun areContentsTheSame(oldItem: T, newItem: T): Boolean {
        sameContents ?: return oldItem == newItem
        return sameContents.invoke(oldItem, newItem)
    }

    override fun getChangePayload(oldItem: T, newItem: T): Any? {
        return getChangePayload?.invoke(oldItem, newItem)
    }
}