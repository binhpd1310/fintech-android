package com.pcb.fintech.ui.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.DialogFragment
import com.pcb.fintech.R
import kotlinx.android.synthetic.main.fragment_dialog.*

class AppDialog : DialogFragment {

    var title: String? = null
    var message: String? = null
    var positiveText: String? = null
    var negativeText: String? = null
    var positiveTextColor: Int? = null
    var negativeTextColor: Int? = null
    var onPositive: (() -> Unit)? = null
    var onNegative: (() -> Unit)? = null
    var cancelable: Boolean? = null

    constructor() : this(null, null, null, null,
            null, null, null, null, null)

    private constructor(title: String?, message: String?, positiveText: String?, negativeText: String?,
                        positiveTextColor: Int? = null, negativeTextColor: Int? = null,
                        onPositive: (() -> Unit)?, onNegative: (() -> Unit)?, cancelable: Boolean?) {
        this.title = title
        this.message = message
        this.positiveText = positiveText
        this.negativeText = negativeText
        this.positiveTextColor = positiveTextColor
        this.negativeTextColor = negativeTextColor
        this.onPositive = onPositive
        this.onNegative = onNegative
        this.cancelable = cancelable
    }

    override fun onResume() {
        super.onResume()
        dialog?.window?.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        return inflater.inflate(R.layout.fragment_dialog, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initUi()
    }

    private fun initUi() {
        val thisContext = context ?: throw RuntimeException("Error: Context is null!")
        if (title == null) {
            tvTitle.visibility = View.GONE
        } else {
            tvTitle.visibility = View.VISIBLE
            tvTitle.text = title
        }
        if (tvMessage == null) {
            tvMessage.visibility = View.GONE
        } else {
            tvMessage.visibility = View.VISIBLE
            tvMessage.text = message
        }
        if (positiveText != null) {
            btnPositive.text = positiveText
        }
        if (negativeText != null) {
            btnNegative.text = negativeText
        }
        if (positiveTextColor != null) {
            btnPositive.setTextColor(ContextCompat.getColor(thisContext, positiveTextColor!!))
        }
        if (negativeTextColor != null) {
            btnNegative.setTextColor(ContextCompat.getColor(thisContext, negativeTextColor!!))
        }
        if (onNegative == null) {
            btnNegative.visibility = View.GONE
        }
        btnPositive.setOnClickListener {
            onPositive?.invoke()
            dismiss()
        }
        btnNegative.setOnClickListener {
            onNegative?.invoke()
            dismiss()
        }
        dialog?.setCancelable(cancelable ?: true)
        dialog?.setCanceledOnTouchOutside(cancelable ?: true)
    }

    companion object {
        fun newInstance(title: String?,
                        message: String?,
                        positiveText: String? = null,
                        negativeText: String? = null,
                        positiveTextColor: Int? = null,
                        negativeTextColor: Int? = null,
                        cancelable: Boolean? = null,
                        onPositive: (() -> Unit)? = null,
                        onNegative: (() -> Unit)? = null): AppDialog {
            return AppDialog(title, message, positiveText, negativeText, positiveTextColor,
                    negativeTextColor, onPositive, onNegative, cancelable)
        }
    }
}