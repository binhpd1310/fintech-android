package com.pcb.fintech.data.source.remote

import retrofit2.HttpException
import retrofit2.Response

class CustomHttpException(private val response: Response<*>,
                          private val customErrorMsg: String? = null) : HttpException(response) {

    fun getErrorMsg() = customErrorMsg ?: response.message() ?: "Error!"
}