package com.pcb.fintech.ui.report

import android.os.Bundle
import android.transition.TransitionManager
import android.view.View
import android.widget.AdapterView
import com.pcb.fintech.R
import com.pcb.fintech.data.model.Consumer3
import com.pcb.fintech.data.model.other.PaymentMethod
import com.pcb.fintech.data.model.other.ShippingMethod
import com.pcb.fintech.data.model.response.*
import com.pcb.fintech.di.ViewModelFactory
import com.pcb.fintech.eventbus.EventBus
import com.pcb.fintech.eventbus.event.RequestPaymentEvent
import com.pcb.fintech.ui.base.BaseMVVMFragment
import com.pcb.fintech.ui.base.OpenActivity
import com.pcb.fintech.ui.views.SimpleAdapterItemSelectedListener
import com.pcb.fintech.ui.views.getTrimText
import com.pcb.fintech.ui.views.gone
import com.pcb.fintech.ui.views.visible
import com.pcb.fintech.utils.getMoneyInText
import com.pcb.fintech.vnpay.VnPayGate
import kotlinx.android.synthetic.main.fragment_payment_main.*

class PaymentMainFragment : BaseMVVMFragment<PaymentMainVM, ViewModelFactory>() {

    private var provinces: ArrayList<Province>? = null
    private var districts: ArrayList<District>? = null
    private var wards: ArrayList<Ward>? = null

    private var paymentMethod: PaymentMethod? = null
    private var shippingMethod: ShippingMethod? = null

    private var totalReportFee: Double? = null

    override fun getLayoutRes(): Int {
        return R.layout.fragment_payment_main
    }

    override fun getViewModelType(): Class<PaymentMainVM> {
        return PaymentMainVM::class.java
    }

    override fun extractData(bundle: Bundle) {
        viewModel.setReportId(bundle.getInt(EXTRA_REPORT_ID))
    }

    override fun setupViews(view: View) {
        toolbar.setNavigationOnClickListener { sendToNav { it.goBack() } }

        sp_pay_type.onItemSelectedListener = object : SimpleAdapterItemSelectedListener() {
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                onPaymentTypeChanged(position)
                validateInputData()
            }
        }
        sp_receive_type.onItemSelectedListener = object : SimpleAdapterItemSelectedListener() {
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                onReceiveTypeChanged(position)
                validateInputData()
            }
        }
        tv_back.setOnClickListener { sendToNav { it.goBack() } }
        tv_pay.setOnClickListener { clickPay() }

        delayOnMain({ viewModel.prepareData() })

        val reportId = viewModel.getReportId() ?: return
        doObserveAll(viewModel.getPaymentReport(reportId),
                { handleGetPaymentReportSuccessful(it) },
                { handleGetPaymentReportFailed(it) })
    }

    private fun handleGetPaymentReportSuccessful(response: PaymentReportResponse) {
        fillPaymentReport(response)
    }

    private fun handleGetPaymentReportFailed(message: String) {
        showSnackBar(rootView, message)
    }

    private fun fillPaymentReport(report: PaymentReportResponse?) {
        report ?: return

        viewModel.setPaymentReport(report)

        tv_report_fee.text = getMoneyInText(report.getReportFee())
        tv_ship_fee.text = getMoneyInText(report.getShippingFee())
        tv_total.text = getMoneyInText(report.getTotal())

        this.totalReportFee = report.total
    }


    private fun onGetAdministrativeUnitsSuccessful(result: Consumer3<List<Province>, List<District>, List<Ward>>) {
        provinces = ArrayList<Province>().apply { addAll(result.data1) }
        districts = ArrayList<District>().apply { addAll(result.data2) }
        wards = ArrayList<Ward>().apply { addAll(result.data3) }
    }

    private fun onPaymentTypeChanged(pos: Int) {
        paymentMethod = PaymentMethod.fromValue(pos)
        if (paymentMethod == PaymentMethod.INTERNET_BANKING) {
            TransitionManager.beginDelayedTransition(parent_view)
            tv_offline_info.visible()
        } else {
            TransitionManager.beginDelayedTransition(parent_view)
            tv_offline_info.gone()
        }
    }

    private fun onReceiveTypeChanged(pos: Int) {
        shippingMethod = ShippingMethod.fromValue(pos)

        if (shippingMethod == ShippingMethod.ONLINE) {
            layout_shipping.visibility = View.GONE
            fillShippingFee(null)
            fillTotalFee(totalReportFee, null)
            return
        }
        if (shippingMethod == ShippingMethod.OFFLINE) {
            fillShippingOff()
            doObserveAll(viewModel.getShippingFee(), {
                handleShippingFeeSuccessful(it)
                fillTotalFee(totalReportFee, it)
            }, { handleShippingFeeFailed(it) })
            return
        }
        layout_shipping.visibility = View.GONE
        tv_offline_note.visibility = View.GONE
    }

    private fun fillTotalFee(reportFee: Double?, shippingFee: ShippingFee?) {
        tv_total.text = getMoneyInText((reportFee ?: 0.0) + (shippingFee?.value ?: 0.0))
    }

    private fun handleShippingFeeSuccessful(shippingFee: ShippingFee) {
        fillShippingFee(shippingFee)
        viewModel.saveShippingFee(shippingFee)
    }

    private fun handleShippingFeeFailed(message: String) {
        showSnackBar(rootView, message)
    }

    private fun fillShippingFee(shippingFee: ShippingFee?) {
        if (shippingFee == null) {
            tv_ship_fee.text = getMoneyInText(0)
        } else {
            tv_ship_fee.text = getMoneyInText(shippingFee.getValue())
        }
    }

    private fun fillShippingOff() {
        val addressInfo = viewModel.getShippingAddress()
        if (addressInfo == null) {
            tv_offline_note.visibility = View.VISIBLE
            layout_shipping.visibility = View.GONE
        } else {
            tv_offline_note.visibility = View.GONE
            layout_shipping.visibility = View.VISIBLE
            // Fill shipping address info
            tv_customer_name.text = addressInfo.contactName
            tv_province.text = provinces?.find { it.id == addressInfo.provinceId }?.name
            tv_district.text = districts?.find { it.id == addressInfo.districtId }?.name
            tv_ward.text = wards?.find { it.id == addressInfo.wardId }?.name
            tv_address.text = addressInfo.fullAddress
        }
    }

    private fun validatePaymentMethod(): Boolean {
        return paymentMethod != null && paymentMethod != PaymentMethod.UNKNOWN
    }

    private fun validateShippingMethod(shippingMethod: ShippingMethod?): Boolean {
        var isValid = true
        if (shippingMethod == ShippingMethod.OFFLINE) {
            if (tv_customer_name.getTrimText().isEmpty()) isValid = false
            if (tv_province.getTrimText().isEmpty()) isValid = false
            if (tv_district.getTrimText().isEmpty()) isValid = false
            if (tv_ward.getTrimText().isEmpty()) isValid = false
            if (tv_address.getTrimText().isEmpty()) isValid = false
        }
        if (shippingMethod == null || shippingMethod == ShippingMethod.UNKNOWN) {
            isValid = false
        }
        return isValid
    }

    private fun validateInputData(): Boolean {
        var isValid = true

        val isValidMethod = validatePaymentMethod()
        if (!isValidMethod) isValid = false

        val isShippingValid = validateShippingMethod(shippingMethod)
        if (!isShippingValid) isValid = false

        tv_pay.isEnabled = isValid

        return isValid
    }

    private fun clickPay() {
        viewModel.makePayment(paymentMethod, shippingMethod)
    }

    private fun handlePaymentOnlineSuccessful(response: PaymentOnlineResponse) {
        if (response.vnpAmount > 0) {
            goToVnPayPayment(response)
        } else {
            goToPaymentSuccess()
        }
    }

    private fun goToVnPayPayment(response: PaymentOnlineResponse) {
        val url = VnPayGate.buildUrl(response)
        val args = PaymentViewFragment.buildBundle(url)
        val intent = OpenActivity.getIntent(context, PaymentViewFragment::class.java, args)
        startActivity(intent)
        EventBus.getInstance().notify(RequestPaymentEvent())
        activity?.finish()
    }

    private fun goToPaymentSuccess() {
        val intent = OpenActivity.getIntent(context, PaymentResultFragment::class.java)
        startActivity(intent)
        EventBus.getInstance().notify(RequestPaymentEvent())
        activity?.finish()
    }

    override fun onObserve(): (PaymentMainVM.() -> Unit) = {
        doObserve(paymentOnlineLD) { handlePaymentOnlineSuccessful(it) }
        doObserve(paymentOfflineLD) { goToPaymentSuccess() }
        doObserve(prepareDataLD) { onGetAdministrativeUnitsSuccessful(it) }
    }

    companion object {
        private const val EXTRA_REPORT_ID = "EXTRA_REPORT_ID"

        fun buildBundle(reportId: Int) = Bundle().apply { putInt(EXTRA_REPORT_ID, reportId) }

    }
}