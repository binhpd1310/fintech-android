package com.pcb.fintech.utils.validation

import java.util.regex.Pattern

class PhoneNumberValidator {

    fun isPhoneValid(phoneNumber: CharSequence?): Int {
        if (phoneNumber.isNullOrEmpty()) return RESULT_PHONE_EMPTY

        for (prefix in PHONE_PREFIX) {
            val pattern = Pattern.compile(String.format(PHONE_REGEX, prefix))
            val isMatch = pattern.matcher(phoneNumber)
            if (isMatch.find()) return RESULT_PHONE_VALID
        }
        return RESULT_PHONE_INVALID
    }

    companion object {

        const val RESULT_PHONE_EMPTY = 0
        const val RESULT_PHONE_INVALID = 1
        const val RESULT_PHONE_VALID = 2

        private const val PHONE_REGEX = "^(%s)+([0-9]{7})\$"

        private val PHONE_PREFIX = mutableListOf(
                /* Viettel */
                "086", "096", "097", "098", "032", "033", "034", "035", "036", "037", "038", "039",
                "8486", "8496", "8497", "8498", "8432", "8433", "8434", "8435", "8436", "8437", "8438", "8439",
                /* Mobi */
                "089", "090", "093", "070", "079", "077", "076", "078",
                "8489", "8490", "8493", "8470", "8479", "8477", "8476", "8478",
                /* Vina */
                "088", "091", "094", "083", "084", "085", "081", "082",
                "8488", "8491", "8494", "8483", "8484", "8485", "8481", "8482",
                /* Vietnam mobile */
                "8492", "8456", "8458", "8492", "8456", "8458",
                /* Gmobile*/
                "099", "059", "8499", "8459"
        )
    }

}