package com.pcb.fintech.ui.home.viewholder

import android.view.View
import com.pcb.fintech.data.model.response.LocalLink
import com.pcb.fintech.ui.adapter.MultiTypeVH
import kotlinx.android.synthetic.main.item_home_note.*

class ItemNoteVH(view: View,
                 private val onClick: (link: String?) -> Unit) : MultiTypeVH<LocalLink>(view) {
    init {
        view.setOnClickListener { onClick.invoke(itemView.tag as String?) }
    }

    override fun bindView(item: LocalLink) {
        tv_content.text = String.format("%s. %s", (adapterPosition + 1), item.title)
        itemView.tag = item.link
    }
}