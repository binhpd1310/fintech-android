package com.pcb.fintech.ui.profile.viewholder

import android.view.View
import com.pcb.fintech.data.model.response.Province
import kotlinx.android.synthetic.main.item_place.*
import com.pcb.fintech.ui.adapter.MultiTypeVH

class ProvinceVH(private val itemV: View,
                 onClickListener: (item: Province) -> Unit) : MultiTypeVH<Province>(itemV) {

    init {
        itemV.setOnClickListener { onClickListener.invoke(itemV.tag as Province) }
    }

    override val containerView: View?
        get() = itemV

    override fun bindView(item: Province) {
        itemV.tag = item
        tv_place.text = item.name
        iv_select.visibility = if (item.selected) View.VISIBLE else View.GONE
    }

}