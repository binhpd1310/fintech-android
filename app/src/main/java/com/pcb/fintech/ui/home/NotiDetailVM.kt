package com.pcb.fintech.ui.home

import android.app.Application
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.pcb.fintech.data.model.ResponseResult
import com.pcb.fintech.data.model.response.Notification
import com.pcb.fintech.data.source.CommonDataSource
import com.pcb.fintech.ui.base.BaseViewModel
import com.pcb.fintech.utils.SchedulerProvider
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class NotiDetailVM(
        application: Application, schedulerProvider: SchedulerProvider,
        private val commonDataSource: CommonDataSource
) : BaseViewModel(application, schedulerProvider) {

    internal var notiId: Int? = null

    internal val getNotiByIdLD = MutableLiveData<ResponseResult<Notification>>()

    fun getNotiById() {
        notiId ?: return
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                val notification = commonDataSource.getNotiById(notiId!!)
                getNotiByIdLD.postValue(notification)
            }
        }
    }

    fun deleteOneNoti(notiId: Int) = resultLiveData {
        commonDataSource.deleteOneNoti(notiId)
    }
}