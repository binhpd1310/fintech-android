package com.pcb.fintech.ui.report

import android.view.View
import com.pcb.fintech.R
import com.pcb.fintech.di.ViewModelFactory
import com.pcb.fintech.ui.base.BaseMVVMFragment
import com.pcb.fintech.ui.base.OpenActivity
import com.pcb.fintech.ui.views.EasyTextWatcher
import com.pcb.fintech.ui.views.OnClickAndHideKeyboard
import com.pcb.fintech.ui.views.getTrimText
import com.pcb.fintech.utils.AndroidUtil
import com.pcb.fintech.utils.getHiddenPhone
import kotlinx.android.synthetic.main.fragment_verify_otp.*

class VerifyOtpFragment : BaseMVVMFragment<VerifyOtpVM, ViewModelFactory>() {

    override fun getLayoutRes(): Int {
        return R.layout.fragment_verify_otp
    }

    override fun getViewModelType(): Class<VerifyOtpVM> {
        return VerifyOtpVM::class.java
    }

    override fun viewModelInActivityScope(): Boolean = true

    override fun setupViews(view: View) {
        toolbar.setNavigationOnClickListener { sendToNav { it.goBack() } }

        tv_next.setOnClickListener(OnClickAndHideKeyboard { verifyOtp() })

        edt_otp.addTextChangedListener(EasyTextWatcher(afterTextChanged = { validateInputData() }))

        tv_send_otp.setOnClickListener(OnClickAndHideKeyboard { resendOtp() })

        tv_notify.text = getString(R.string.enter_otp_notify, getHiddenPhone(viewModel.getPhoneNumber()))

        edt_otp.setOnFocusChangeListener { _, hasFocus ->
            if (hasFocus) {
                edt_otp.setSelection(edt_otp.length())
            }
        }

        edt_otp.setOnClickListener {
            edt_otp.setSelection(edt_otp.length())
        }

        edt_otp.requestFocus()

        AndroidUtil.showKeyboard(activity)

        validateInputData()
    }

    private fun resendOtp() {
        edt_otp.setText("")

        viewModel.postShowLoading()

        delayOnMain({ doObserveAll(viewModel.resendOtp()) }, 1000)
    }

    private fun verifyOtp() {
        doObserveAll(viewModel.verifyOtp(edt_otp.getTrimText()), onSuccess = {
            if (it) {
                viewModel.saveVerifiedOtp()
                goToCreateReport()
            } else {
                showDialogMessage(R.string.error_common_title, R.string.text_result_otp_not_correct, R.string.close)
            }
        })
    }

    private fun validateInputData() {
        val otp = edt_otp.getTrimText()
        if (otp.length != 6) {
            tv_next.isEnabled = false
            return
        }
        tv_next.isEnabled = true
        AndroidUtil.hideKeyboard(activity)
    }

    private fun goToCreateReport() {
        val intent = OpenActivity.getIntent(context, CreateReportFragment::class.java)
        startActivity(intent)
        activity?.finish()
    }

    companion object {

        fun newInstance() = VerifyOtpFragment()
    }
}