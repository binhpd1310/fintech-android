package com.pcb.fintech.ui.profile

import android.content.Intent
import android.view.View
import com.pcb.fintech.R
import com.pcb.fintech.di.ViewModelFactory
import com.pcb.fintech.eventbus.EventBus
import com.pcb.fintech.eventbus.event.UserAccountChangedEvent
import com.pcb.fintech.ui.base.BaseMVVMFragment
import com.pcb.fintech.ui.base.HasFragmentResult
import kotlinx.android.synthetic.main.fragment_setting_menu.*

class SettingMenuFragment : BaseMVVMFragment<SettingMenuVM, ViewModelFactory>() {

    override fun getLayoutRes(): Int {
        return R.layout.fragment_setting_menu
    }

    override fun getViewModelType(): Class<SettingMenuVM> {
        return SettingMenuVM::class.java
    }

    override fun setupViews(view: View) {
        toolbar.setNavigationOnClickListener {
            sendToNav { it.goBack() }
        }

        tv_profile.setOnClickListener {
            sendToNav {
                it.openFragment(SettingUserFragment.newInstance(), forResult = Pair(this, CODE_SETTING_USER))
            }
        }

        tv_security.setOnClickListener {
            sendToNav {
                it.openFragment(SettingSecretQuestionFragment.newInstance(), forResult = Pair(this, CODE_SETTING_SECRET_Q))
            }
        }

        tv_address.setOnClickListener {
            sendToNav {
                it.openFragment(SettingAddressFragment.newInstance(), forResult = Pair(this, CODE_SETTING_ADDRESS))
            }
        }

        tv_update_profile.setOnClickListener {
            sendToNav {
                it.openFragment(SettingProfileFragment.newInstance(), forResult = Pair(this, CODE_SETTING_PROFILE))
            }
        }

        tv_change_password.setOnClickListener {
            sendToNav {
                it.openFragment(SettingPasswordFragment.newInstance(), forResult = Pair(this, CODE_SETTING_PASSWORD))
            }
        }
    }

    override fun onObserve(): (SettingMenuVM.() -> Unit) = {
        doObserve(hasProfileChangedLD()) {
            doObserveAll(getProfileLD(), { EventBus.getInstance().notify(UserAccountChangedEvent()) })
        }
    }

    override fun onFragmentResult(requestCode: Int, action: Int, extraData: Intent?) {
        if (action == HasFragmentResult.ACTION_OK) {
            viewModel.needFetchProfile()
        }
    }

    companion object {

        private const val CODE_SETTING_USER = 101
        private const val CODE_SETTING_SECRET_Q = 102
        private const val CODE_SETTING_ADDRESS = 103
        private const val CODE_SETTING_PROFILE = 104
        private const val CODE_SETTING_PASSWORD = 105

        fun newInstance() = SettingMenuFragment()
    }
}