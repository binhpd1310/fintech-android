package com.pcb.fintech.ui.report

import android.os.Bundle
import android.view.View
import com.pcb.fintech.R
import com.pcb.fintech.data.model.response.PaymentOnlineResultResponse
import com.pcb.fintech.di.ViewModelFactory
import com.pcb.fintech.eventbus.EventBus
import com.pcb.fintech.eventbus.event.PaymentSuccessEvent
import com.pcb.fintech.ui.base.BaseMVVMFragment
import com.pcb.fintech.vnpay.VnPayPaymentStatus
import com.pcb.fintech.vnpay.VnPayResponse
import kotlinx.android.synthetic.main.fragment_payment_result.*

class PaymentResultFragment : BaseMVVMFragment<PaymentResultVM, ViewModelFactory>() {

    private var vnPayResponse: VnPayResponse? = null

    override fun getLayoutRes(): Int {
        return R.layout.fragment_payment_result
    }

    override fun getViewModelType(): Class<PaymentResultVM> {
        return PaymentResultVM::class.java
    }

    override fun extractData(bundle: Bundle) {
        vnPayResponse = bundle.getParcelable(EXTRA_VNPAY_RESPONSE)
    }

    override fun setupViews(view: View) {
        btn_back.setOnClickListener { sendToNav { it.goBack() } }

        if (vnPayResponse == null) {
            showPaymentSuccess(VnPayPaymentStatus.SUCCESSFUL.msg)
        } else {
            doObserveAll(viewModel.paymentOnlineStatusLD(vnPayResponse!!),
                    { handleResultSuccessful(it) },
                    { sendToNav { it.goBack() } })
        }
        EventBus.getInstance().notify(PaymentSuccessEvent())
    }

    private fun handleResultSuccessful(result: PaymentOnlineResultResponse) {
        val paymentStatus = VnPayPaymentStatus.fromCode(result.rspCode)
        if (paymentStatus == VnPayPaymentStatus.SUCCESSFUL ||
                paymentStatus == VnPayPaymentStatus.ORDER_ALREADY_CONFIRMED) {
            showPaymentSuccess(paymentStatus.msg)
        } else {
            showPaymentFailed(paymentStatus?.msg)
        }
    }

    private fun showPaymentSuccess(msg: String) {
        layout_successful.visibility = View.VISIBLE
        layout_failed.visibility = View.GONE
        tv_msg.text = msg
    }

    private fun showPaymentFailed(msg: String?) {
        layout_successful.visibility = View.GONE
        layout_failed.visibility = View.VISIBLE
        tv_error.text = msg
    }

    companion object {
        private const val EXTRA_VNPAY_RESPONSE = "EXTRA_VNPAY_RESPONSE"

        fun makeBundle(vnPayResponse: VnPayResponse) = Bundle().apply {
            putParcelable(EXTRA_VNPAY_RESPONSE, vnPayResponse)
        }
    }
}