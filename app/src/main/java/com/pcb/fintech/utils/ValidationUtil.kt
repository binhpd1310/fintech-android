package com.pcb.fintech.utils

import com.pcb.fintech.data.model.other.CardType
import com.pcb.fintech.utils.validation.AppValidator
import com.pcb.fintech.utils.validation.CardValidationResult

object ValidationUtil {
    const val USERNAME_MIN_LENGTH = 6

    private val validator = AppValidator.getInstance()

    fun isValidEmail(target: CharSequence?): Boolean {
        return validator.isValidEmail(target)
    }

    fun isValidPhone(target: CharSequence?): Int {
        return validator.isValidPhone(target)
    }

    fun isValidCardNumber(cardType: CardType?, cardNumber: String?): CardValidationResult {
        return validator.isValidCardNumber(cardType, cardNumber)
    }

    fun isValidPassword(target: CharSequence?): Boolean {
        return validator.isValidPassword(target)
    }
}