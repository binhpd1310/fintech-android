package com.pcb.fintech.utils

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.util.Base64

import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileNotFoundException
import java.lang.ref.WeakReference

/**
 * Author: Kosalgeek
 */
class Base64Image private constructor() {

    private var width = 256
    private var height = 256 // default

    private var noScale: Boolean = false

    private var weakReferenceContext: WeakReference<Context>? = null

    private fun setContext(context: Context?) {
        if (context == null) {
            throw IllegalArgumentException("Context must not be null.")
        }
        weakReferenceContext = WeakReference(context)
    }

    /**
     * Request width and height. These are used to calculate to find the best fit for width and height.
     * The image will only scale down but not stretching. Be default, width = 128px and height = 128.
     * @param width
     * @param height
     * @return Base64Image
     */
    fun requestSize(width: Int, height: Int): Base64Image? {
        this.height = width
        this.width = height
        return instance
    }

    /**
     * set no scale
     * @return Base64Image
     */

    fun noScale(): Base64Image? {
        this.noScale = true
        return instance
    }

    /**
     * Encode an resource image (drawable) into a base64 image.
     * @param resId
     * @return String: an encoded image in base64 String format.
     */
    fun encodeResource(resId: Int): String? {
        var bitmap: Bitmap? = null
        if (noScale) {
            bitmap = decodeResource(resId)
        } else {
            bitmap = decodeSampledBitmapFromResource(resId, width, height)
        }
        bitmap ?: return null
        return encodeImageToString(bitmap)
    }

    /**
     * Encode an image into a base64 image.
     * @param filePath
     * @return String: an encoded image in base64 String format.
     * @throws FileNotFoundException
     */
    @Throws(FileNotFoundException::class)
    fun encodeFile(filePath: String): String {

        var bitmap: Bitmap? = null
        if (noScale) {
            bitmap = decodeFile(filePath)
        } else {
            bitmap = decodeSampledBitmapFromFile(filePath, width, height)
        }

        return encodeImageToString(bitmap)
    }

    fun encodeImageToString(bitmap: Bitmap, mode: Int = Base64.DEFAULT): String {
        val tempBitmap: Bitmap? = bitmap
        val bao = ByteArrayOutputStream()
        val quality = 100 //100: compress nothing
        tempBitmap?.compress(Bitmap.CompressFormat.JPEG, quality, bao)

        if (tempBitmap != null) {//important! prevent out of memory
            bitmap.recycle()
        }

        val ba = bao.toByteArray()
        return Base64.encodeToString(ba, mode)
    }

    ///////////////////PRIVATE METHODS

    private fun calculateInSampleSize(options: BitmapFactory.Options, reqWidth: Int, reqHeight: Int): Int {
        // Raw height and width of image
        val height = options.outHeight
        val width = options.outWidth
        var inSampleSize = 1

        if (height > reqHeight || width > reqWidth) {

            val halfHeight = height / 2
            val halfWidth = width / 2

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while (halfHeight / inSampleSize > reqHeight && halfWidth / inSampleSize > reqWidth) {
                inSampleSize *= 2
            }
        }

        return inSampleSize
    }

    @Throws(FileNotFoundException::class)
    private fun decodeSampledBitmapFromFile(filePath: String, reqWidth: Int, reqHeight: Int): Bitmap {

        val file = File(filePath)
        if (!file.exists()) {
            throw FileNotFoundException()
        }

        val options = BitmapFactory.Options()
        options.inJustDecodeBounds = true

        BitmapFactory.decodeFile(filePath, options)

        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight)

        options.inJustDecodeBounds = false
        return BitmapFactory.decodeFile(filePath, options)
    }

    @Throws(FileNotFoundException::class)
    private fun decodeFile(filePath: String): Bitmap {

        val file = File(filePath)
        if (!file.exists()) {
            throw FileNotFoundException()
        }

        return BitmapFactory.decodeFile(filePath)
    }

    fun decodeSampledBitmapFromResource(resId: Int, reqWidth: Int, reqHeight: Int): Bitmap? {
        val ct = weakReferenceContext?.get() ?: return null
        // First decode getInstance inJustDecodeBounds=true to check dimensions
        val options = BitmapFactory.Options()
        options.inJustDecodeBounds = true
        BitmapFactory.decodeResource(ct.getResources(), resId, options)

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight)

        // Decode bitmap getInstance inSampleSize requestSize
        options.inJustDecodeBounds = false

        return BitmapFactory.decodeResource(ct.getResources(), resId, options)
    }

    private fun decodeResource(resId: Int): Bitmap? {
        val ct = weakReferenceContext?.get() ?: return null
        return BitmapFactory.decodeResource(ct.getResources(), resId)
    }

    fun clear() {
        weakReferenceContext?.clear()
        weakReferenceContext = null
    }

    companion object {

        private var instance: Base64Image? = null

        fun getInstance(context: Context?): Base64Image {
            if (instance == null) {
                synchronized(Base64Image::class.java) {
                    if (instance == null) {
                        instance = Base64Image().apply { setContext(context) }
                    }
                }
            }
            return instance!!
        }

        fun decodeBase64(base64String: String): Bitmap? {
            val byteArray = Base64.decode(base64String, Base64.DEFAULT)
            return BitmapFactory.decodeByteArray(byteArray, 0, byteArray.size)
        }
    }

}