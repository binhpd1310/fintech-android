package com.pcb.fintech.ui.home

import android.app.Application
import com.pcb.fintech.R
import com.pcb.fintech.ui.base.BaseViewModel
import com.pcb.fintech.utils.AndroidUtil
import com.pcb.fintech.utils.SchedulerProvider

class RateAppVM(application: Application, schedulerProvider: SchedulerProvider)
    : BaseViewModel(application, schedulerProvider) {

    fun rateApp() {
        AndroidUtil.openGooglePlay(getContext(), getContext().packageName)
    }

    fun shareApp() {
        val url = getContext().getString(R.string.text_sharing_url, getContext().packageName)
        val title = getContext().getString(R.string.text_sharing_title)
        val message = getContext().getString(R.string.text_sharing_message)
        val content = getContext().getString(R.string.text_sharing_content, url)
        AndroidUtil.shareMessage(getContext(), title, content, message)
    }
}