package com.pcb.fintech.ui.report

import android.os.Bundle
import android.util.Log
import android.view.View
import android.webkit.*
import com.pcb.fintech.R
import com.pcb.fintech.di.ViewModelFactory
import com.pcb.fintech.eventbus.EventBus
import com.pcb.fintech.eventbus.event.PaymentSuccessEvent
import com.pcb.fintech.ui.base.BaseMVVMFragment
import kotlinx.android.synthetic.main.fragment_payment_view.*

class PaymentViewFragment : BaseMVVMFragment<PaymentMainVM, ViewModelFactory>() {

    private var url: String? = null

    override fun getLayoutRes(): Int {
        return R.layout.fragment_payment_view
    }

    override fun getViewModelType(): Class<PaymentMainVM> {
        return PaymentMainVM::class.java
    }

    override fun extractData(bundle: Bundle) {
        url = bundle.getString(EXTRA_URL)
    }

    override fun setupViews(view: View) {
        toolbar.setNavigationOnClickListener { sendToNav { it.goBack() } }

        WebView.setWebContentsDebuggingEnabled(true)
        webView.scrollBarStyle = WebView.SCROLLBARS_OUTSIDE_OVERLAY
        webView.setInitialScale(1)
        with(webView.settings) {
            javaScriptEnabled = true
            domStorageEnabled = true
            loadWithOverviewMode = true
            layoutAlgorithm = WebSettings.LayoutAlgorithm.SINGLE_COLUMN
            defaultZoom = WebSettings.ZoomDensity.FAR
            useWideViewPort = true
            setSupportZoom(true)
            builtInZoomControls = true
            displayZoomControls = false
            javaScriptCanOpenWindowsAutomatically = true
            pluginState = WebSettings.PluginState.ON
        }
        webView.webViewClient = WebViewClient()
        webView.webChromeClient = WebChromeClient()
        webView.loadUrl(url)
    }

    override fun handleBack(): Boolean {
        EventBus.getInstance().notify(PaymentSuccessEvent())
        return super.handleBack()
    }

    class MyWebClient : WebViewClient() {

        override fun shouldOverrideUrlLoading(webView: WebView, url: String): Boolean {
            webView.loadUrl(url)
            return true
        }
    }

    class MyWebChromeClient : WebChromeClient() {

        override fun onJsAlert(
            view: WebView?, url: String?, message: String?,
            result: JsResult?
        ): Boolean {
            return true
        }

        override fun onJsConfirm(
            view: WebView?, url: String?, message: String?,
            result: JsResult?
        ): Boolean {
            return true
        }

        override fun onJsPrompt(
            view: WebView?, url: String?, message: String?,
            defaultValue: String?, result: JsPromptResult?
        ): Boolean {
            return true
        }

        override fun onConsoleMessage(consoleMessage: ConsoleMessage?): Boolean {
            Log.d(
                "MyApplication", "MyApplication -"
                        + consoleMessage?.message() + " -- From line "
                        + consoleMessage?.lineNumber() + " of "
                        + consoleMessage?.sourceId()
            )
            return super.onConsoleMessage(consoleMessage)
        }
    }

    companion object {

        private const val EXTRA_URL = "EXTRA_URL"

        fun buildBundle(url: String) = Bundle().apply { putString(EXTRA_URL, url) }
    }
}
