package com.pcb.fintech.ui.profile

import android.content.Intent
import android.os.Parcelable
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.transition.TransitionManager
import com.pcb.fintech.R
import com.pcb.fintech.data.model.Consumer3
import com.pcb.fintech.data.model.body.Address
import com.pcb.fintech.data.model.body.AddressType
import com.pcb.fintech.data.model.body.AddressesBody
import com.pcb.fintech.data.model.response.District
import com.pcb.fintech.data.model.response.Province
import com.pcb.fintech.data.model.response.Ward
import com.pcb.fintech.data.utils.isCommonFilled
import com.pcb.fintech.data.utils.isFullFilled
import com.pcb.fintech.di.ViewModelFactory
import com.pcb.fintech.ui.base.BaseMVVMFragment
import com.pcb.fintech.ui.base.HasFragmentResult
import com.pcb.fintech.ui.views.*
import com.pcb.fintech.utils.ValidationUtil.isValidPhone
import com.pcb.fintech.utils.validation.PhoneNumberValidator.Companion.RESULT_PHONE_EMPTY
import com.pcb.fintech.utils.validation.PhoneNumberValidator.Companion.RESULT_PHONE_INVALID
import com.pcb.fintech.utils.validation.PhoneNumberValidator.Companion.RESULT_PHONE_VALID
import kotlinx.android.synthetic.main.fragment_setting_address.*

class SettingAddressFragment : BaseMVVMFragment<SettingAddressVM, ViewModelFactory>() {

    private var perProvinceId: Int? = null
    private var perDistrictId: Int? = null
    private var perWardId: Int? = null

    private var tempProvinceId: Int? = null
    private var tempDistrictId: Int? = null
    private var tempWardId: Int? = null

    private var reportProvinceId: Int? = null
    private var reportDistrictId: Int? = null
    private var reportWardId: Int? = null

    private var provinces: ArrayList<Province>? = null
    private var districts: ArrayList<District>? = null
    private var wards: ArrayList<Ward>? = null

    private var mPerAddress: Address? = null
    private var mTempAddress: Address? = null
    private var mReportAddress: Address? = null

    override fun getLayoutRes(): Int {
        return R.layout.fragment_setting_address
    }

    override fun getViewModelType(): Class<SettingAddressVM> {
        return SettingAddressVM::class.java
    }

    override fun setupViews(view: View) {
        toolbar.setNavigationOnClickListener { sendToNav { it.goBack() } }

        tv_back.setOnClickListener { sendToNav { it.goBack() } }

        tv_permanent_address.setOnClickListener {
            val isSelected = tv_permanent_address.isSelected
            tv_permanent_address.isSelected = !isSelected
            showHidePermanentAddress()
        }
        tv_temporary_address.setOnClickListener {
            val isSelected = tv_temporary_address.isSelected
            tv_temporary_address.isSelected = !isSelected
            showHideTemporaryAddress()
        }
        tv_report_address.setOnClickListener {
            val isSelected = tv_report_address.isSelected
            tv_report_address.isSelected = !isSelected
            showHideReportAddress()
        }

        edt_per_province.setOnClickListener { showProvinces(RequestCode.PROVINCE) }
        edt_per_district.setOnClickListener { showDistricts(perProvinceId, RequestCode.DISTRICT) }
        edt_per_ward.setOnClickListener { showWards(perDistrictId, RequestCode.WARD) }

        edt_temp_province.setOnClickListener { showProvinces(RequestCode.TEMP_PROVINCE) }
        edt_temp_district.setOnClickListener { showDistricts(tempProvinceId, RequestCode.TEMP_DISTRICT) }
        edt_temp_ward.setOnClickListener { showWards(tempDistrictId, RequestCode.TEMP_WARD) }

        edt_report_province.setOnClickListener { showProvinces(RequestCode.REPORT_PROVINCE) }
        edt_report_district.setOnClickListener { showDistricts(reportProvinceId, RequestCode.REPORT_DISTRICT) }
        edt_report_ward.setOnClickListener { showWards(reportDistrictId, RequestCode.REPORT_WARD) }

        select_per_address.setOnClickListener {
            rb_per_address.isChecked = true
            rb_temp_address.isChecked = false
            setShippingAddressFromPer()
        }

        select_temp_address.setOnClickListener {
            rb_per_address.isChecked = false
            rb_temp_address.isChecked = true
            setShippingAddressFromTemp()
        }

        tv_update.setOnClickListener { clickUpdate() }

        addTextWatcher()

        delayOnMain({ viewModel.prepareData() })
    }

    private fun setShippingAddressFromPer() {
        removeTextWatcher()
        setShippingAddress(perProvinceId, perDistrictId, perWardId,
                edt_per_province.getTrimText(), edt_per_district.getTrimText(), edt_per_ward.getTrimText(),
                "", "", edt_per_specific_address.getTrimText())
        addTextWatcher()
        validateInputData()
    }

    private fun setShippingAddressFromTemp() {
        removeTextWatcher()
        setShippingAddress(tempProvinceId, tempDistrictId, tempWardId,
                edt_temp_province.getTrimText(), edt_temp_district.getTrimText(), edt_temp_ward.getTrimText(),
                "", "", edt_temp_specific_address.getTrimText())
        addTextWatcher()
        validateInputData()
    }

    private fun setShippingAddress(provinceId: Int?, districtId: Int?, wardId: Int?,
                                   provinceName: String?, districtName: String?, wardName: String?,
                                   contactName: String?, phone: String?, fullAddress: String?) {
        reportProvinceId = provinceId
        reportDistrictId = districtId
        reportWardId = wardId
        setAddressView(edt_report_province, edt_report_district, edt_report_ward, provinceName, districtName, wardName)
        showAddressInfo(edt_report_contact_name, edt_report_phone, edt_report_specific_address, contactName, phone, fullAddress)
    }

    override fun onObserve(): (SettingAddressVM.() -> Unit) = {
        doObserve(prepareDataLD) { onPrepareDataSuccess(it) }
    }

    private fun onPrepareDataSuccess(result: Consumer3<List<Province>, List<District>, List<Ward>>) {
        provinces = ArrayList<Province>().apply { addAll(result.data1) }
        districts = ArrayList<District>().apply { addAll(result.data2) }
        wards = ArrayList<Ward>().apply { addAll(result.data3) }

        doObserveAll(viewModel.getAddressesLD(), onSuccess = {
            removeTextWatcher()
            fillAddressInfo(it)
            validateInputData()
            addTextWatcher()
        }, onError = { showSnackBar(rootView, it) })
    }

    private fun fillAddressInfo(addressesBody: AddressesBody) {
        for (item in addressesBody.addresses) {
            val province = provinces?.find { it.id == item.provinceId }
            val district = districts?.find { it.id == item.districtId }
            val ward = wards?.find { it.id == item.wardId }
            val addressType = AddressType.fromValue(item.addressType)
            when (addressType) {
                AddressType.PERMANENT -> {
                    perProvinceId = province?.id
                    perDistrictId = district?.id
                    perWardId = ward?.id
                    setAddressView(edt_per_province, edt_per_district, edt_per_ward, province?.name, district?.name, ward?.name)
                    showAddressInfo(null, null, edt_per_specific_address, item.contactName, item.phone, item.fullAddress)
                }
                AddressType.TEMPORARY -> {
                    tempProvinceId = province?.id
                    tempDistrictId = district?.id
                    tempWardId = ward?.id
                    setAddressView(edt_temp_province, edt_temp_district, edt_temp_ward, province?.name, district?.name, ward?.name)
                    showAddressInfo(null, null, edt_temp_specific_address, item.contactName, item.phone, item.fullAddress)
                }
                AddressType.REPORT -> {
                    reportProvinceId = province?.id
                    reportDistrictId = district?.id
                    reportWardId = ward?.id
                    setAddressView(edt_report_province, edt_report_district, edt_report_ward, province?.name, district?.name, ward?.name)
                    showAddressInfo(edt_report_contact_name, edt_report_phone, edt_report_specific_address, item.contactName, item.phone, item.fullAddress)
                }
            }
        }
    }

    private fun showAddressInfo(tvName: TextView?, tvPhone: TextView?, tvAddress: TextView,
                                contactName: String?, phone: String?, fullAddress: String?) {
        tvName?.text = contactName ?: ""
        tvPhone?.text = phone ?: ""
        tvAddress.text = fullAddress ?: ""
    }

    private fun showProvinces(requestCode: Int) {
        val fragment = SelectPlaceDialog.newInstance(requestCode,
                getString(R.string.text_select_province_title), provinces as ArrayList<out Parcelable>?)
        fragment.setTargetFragment(this, requestCode)
        fragment.show(parentFragmentManager, null)
    }

    private fun showDistricts(provinceId: Int?, requestCode: Int) {
        provinceId ?: return
        val districtList = districts?.filter { it.provinceId == provinceId } ?: return
        val fragment = SelectPlaceDialog.newInstance(requestCode,
                getString(R.string.text_select_district_title), districtList as ArrayList<out Parcelable>?)
        fragment.setTargetFragment(this, requestCode)
        fragment.show(parentFragmentManager, null)
    }

    private fun showWards(districtId: Int?, requestCode: Int) {
        districtId ?: return
        val wardList = wards?.filter { it.districtId == districtId } ?: return
        val fragment = SelectPlaceDialog.newInstance(
                requestCode, getString(R.string.text_select_ward_title), wardList as ArrayList<out Parcelable>?)
        fragment.setTargetFragment(this, requestCode)
        fragment.show(parentFragmentManager, null)
    }

    private val textWatcher = EasyTextWatcher(afterTextChanged = { validateInputData() })

    private fun addTextWatcher() {
        edt_per_specific_address.addTextChangedListener(textWatcher)
        edt_temp_specific_address.addTextChangedListener(textWatcher)
        edt_report_contact_name.addTextChangedListener(textWatcher)
        edt_report_phone.addTextChangedListener(textWatcher)
        edt_report_specific_address.addTextChangedListener(textWatcher)
    }

    private fun removeTextWatcher() {
        edt_per_specific_address.removeTextChangedListener(textWatcher)
        edt_temp_specific_address.removeTextChangedListener(textWatcher)
        edt_report_contact_name.removeTextChangedListener(textWatcher)
        edt_report_phone.removeTextChangedListener(textWatcher)
        edt_report_specific_address.removeTextChangedListener(textWatcher)
    }

    private fun showHidePermanentAddress() {
        TransitionManager.beginDelayedTransition(rootView as ViewGroup)
        if (tv_permanent_address.isSelected) {
            layout_permanent_address.visibility = View.VISIBLE
        } else {
            layout_permanent_address.visibility = View.GONE
        }
    }

    private fun showHideTemporaryAddress() {
        TransitionManager.beginDelayedTransition(rootView as ViewGroup)
        layout_temporary_address.visibility = if (tv_temporary_address.isSelected) View.VISIBLE
        else View.GONE
    }

    private fun showHideReportAddress() {
        TransitionManager.beginDelayedTransition(rootView as ViewGroup)
        layout_report_address.visibility = if (tv_report_address.isSelected) View.VISIBLE
        else View.GONE
    }

    private fun validateInputData(): Boolean {
        val userId = viewModel.getUserId()
        val perAddress = Address(userId, AddressType.PERMANENT.value, null,
                null, edt_per_specific_address.getTrimText(),
                perProvinceId, perDistrictId, perWardId)
        val tempAddress = Address(userId, AddressType.TEMPORARY.value, null,
                null, edt_temp_specific_address.getTrimText(),
                tempProvinceId, tempDistrictId, tempWardId)
        val reportAddress = Address(userId, AddressType.REPORT.value, edt_report_contact_name.getTrimText(),
                edt_report_phone.getTrimText(), edt_report_specific_address.getTrimText(),
                reportProvinceId, reportDistrictId, reportWardId)
        var isValid = true

        if (!reportAddress.isFullFilled() || !checkPhone(reportAddress.phone, tv_report_phone_error)) isValid = false

        tv_update.isEnabled = isValid

        if (isValid) {
            mPerAddress = if (perAddress.isCommonFilled()) perAddress else null
            mTempAddress = if (tempAddress.isCommonFilled()) tempAddress else null
            mReportAddress = if (reportAddress.isFullFilled()) reportAddress else null
        }

        return isValid
    }

    private fun checkPhone(phone: String?, tvError: TextView): Boolean {
        var isValid = true
        when (isValidPhone(phone)) {
            RESULT_PHONE_EMPTY -> {
                isValid = false
                tvError.visibleAndSet(getString(R.string.error_phone_empty))
            }
            RESULT_PHONE_INVALID -> {
                isValid = false
                tvError.visibleAndSet(getString(R.string.error_phone_invalid))
            }
            RESULT_PHONE_VALID -> tvError.gone()
        }
        return isValid
    }

    private fun clickUpdate() {
        if (!validateInputData()) return

        val listOfAddress = mutableListOf<Address>()
        mPerAddress?.let { listOfAddress.add(it) }
        mTempAddress?.let { listOfAddress.add(it) }
        mReportAddress?.let { listOfAddress.add(it) }

        if (listOfAddress.size <= 0) return

        doObserveAll(viewModel.updateAddressesLD(AddressesBody(listOfAddress)),
                onSuccess = { onUpdateAddressesResult(it) })
    }

    private fun onUpdateAddressesResult(result: Boolean) {
        if (result) {
            viewModel.postHideLoading()
            showToast(R.string.text_update_info_successful, Toast.LENGTH_LONG)
            setFragmentResult(HasFragmentResult.ACTION_OK)
            sendToNav { it.goBack() }
        } else {
            showDialogMessage(R.string.error_common_title, R.string.text_update_info_failed, R.string.close)
        }
    }

    private fun setAddressView(textView: TextView, text: String?) {
        textView.text = text ?: ""
    }

    private fun setAddressView(provinceView: TextView, districtView: TextView, wardView: TextView,
                               province: String?, district: String?, ward: String?) {
        provinceView.text = province ?: ""
        setAddressView(districtView, wardView, district, ward)
    }

    private fun setAddressView(districtView: TextView, wardView: TextView,
                               district: String?, ward: String?) {
        districtView.text = district ?: ""
        setAddressView(wardView, ward)
    }

    override fun onFragmentResult(requestCode: Int, action: Int, extraData: Intent?) {
        if (action == HasFragmentResult.ACTION_OK) {
            val bundle = extraData?.extras ?: return
            when (requestCode) {
                // Permanent address
                RequestCode.PROVINCE -> {
                    val province: Province? = bundle.getParcelable(SelectPlaceDialog.EXTRA_DATA)
                    perProvinceId = province?.id
                    setAddressView(edt_per_province, edt_per_district, edt_per_ward, province?.name, null, null)
                }
                RequestCode.DISTRICT -> {
                    val district: District? = bundle.getParcelable(SelectPlaceDialog.EXTRA_DATA)
                    perDistrictId = district?.id
                    setAddressView(edt_per_district, edt_per_ward, district?.name, null)
                }
                RequestCode.WARD -> {
                    val ward: Ward? = bundle.getParcelable(SelectPlaceDialog.EXTRA_DATA)
                    perWardId = ward?.id
                    setAddressView(edt_per_ward, ward?.name)
                }
                // Temporary address
                RequestCode.TEMP_PROVINCE -> {
                    val province: Province? = bundle.getParcelable(SelectPlaceDialog.EXTRA_DATA)
                    tempProvinceId = province?.id
                    setAddressView(edt_temp_province, edt_temp_district, edt_temp_ward, province?.name, null, null)
                }
                RequestCode.TEMP_DISTRICT -> {
                    val district: District? = bundle.getParcelable(SelectPlaceDialog.EXTRA_DATA)
                    tempDistrictId = district?.id
                    setAddressView(edt_temp_district, edt_temp_ward, district?.name, null)
                }
                RequestCode.TEMP_WARD -> {
                    val ward: Ward? = bundle.getParcelable(SelectPlaceDialog.EXTRA_DATA)
                    tempWardId = ward?.id
                    setAddressView(edt_temp_ward, ward?.name)
                }
                // Report address
                RequestCode.REPORT_PROVINCE -> {
                    val province: Province? = bundle.getParcelable(SelectPlaceDialog.EXTRA_DATA)
                    reportProvinceId = province?.id
                    setAddressView(edt_report_province, edt_report_district, edt_report_ward, province?.name, null, null)
                }
                RequestCode.REPORT_DISTRICT -> {
                    val district: District? = bundle.getParcelable(SelectPlaceDialog.EXTRA_DATA)
                    reportDistrictId = district?.id
                    setAddressView(edt_report_district, edt_report_ward, district?.name, null)
                }
                RequestCode.REPORT_WARD -> {
                    val ward: Ward? = bundle.getParcelable(SelectPlaceDialog.EXTRA_DATA)
                    reportWardId = ward?.id
                    setAddressView(edt_report_ward, ward?.name)
                }
            }
            validateInputData()
        }
    }

    override fun onDestroyView() {
        removeTextWatcher()
        super.onDestroyView()
    }

    companion object {

        fun newInstance() = SettingAddressFragment()
    }
}