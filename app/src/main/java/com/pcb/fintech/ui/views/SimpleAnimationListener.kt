package com.pcb.fintech.ui.views

import android.view.animation.Animation

class SimpleAnimationListener(private val animEnd: (() -> Unit)? = null) : Animation.AnimationListener {
    override fun onAnimationRepeat(animation: Animation?) {
        // Do nothing
    }

    override fun onAnimationEnd(animation: Animation?) {
        animEnd?.invoke()
    }

    override fun onAnimationStart(animation: Animation?) {
        // Do nothing
    }
}