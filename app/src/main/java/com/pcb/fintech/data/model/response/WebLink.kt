package com.pcb.fintech.data.model.response

import com.google.gson.annotations.SerializedName

data class WebLink(@SerializedName("id") val id: Int? = null,
                   @SerializedName("articleType") val articleType: Int? = null,
                   @SerializedName("isActive") val isActive: Boolean? = null,
                   @SerializedName("linkCode") val linkCode: String? = null,
                   @SerializedName("titleVi") val titleVi: String? = null,
                   @SerializedName("linkVi") val linkVi: String? = null,
                   @SerializedName("titleEn") val titleEn: String? = null,
                   @SerializedName("linkEn") val linkEn: String? = null) {

    constructor() : this(null)

    companion object {
        const val ARTICLE_TYPE = 7
    }
}