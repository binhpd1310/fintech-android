package com.pcb.fintech.eventbus.observer

import com.pcb.fintech.eventbus.event.*

class SimpleEventObserver(
        private val onCanceledReport: ((event: CancelledReportEvent) -> Unit)? = null,
        private val onDeletedReport: ((event: DeletedReportEvent) -> Unit)? = null,
        private val onPaymentSuccess: ((event: PaymentSuccessEvent) -> Unit)? = null,
        private val onCreatedReport: ((event: CreatedReportEvent) -> Unit)? = null,
        private val onRequestedPayment: ((event: RequestPaymentEvent) -> Unit)? = null,
        private val onAccountChanged: ((event: UserAccountChangedEvent) -> Unit)? = null,
        private val onUpdatedReport: ((event: UpdatedReportEvent) -> Unit)? = null,
        private val onOpenHomeTab: ((event: OpenHomeTabEvent) -> Unit)? = null,
        private val onCloseLeftMenu: ((event: CloseLeftMenuEvent) -> Unit)? = null,
        private val onDeletedNoti: ((event: DeletedNotiEvent) -> Unit)? = null
) : EventObserver {

    override fun onEvent(event: CancelledReportEvent) {
        onCanceledReport?.invoke(event)
    }

    override fun onEvent(event: DeletedReportEvent) {
        onDeletedReport?.invoke(event)
    }

    override fun onEvent(event: PaymentSuccessEvent) {
        onPaymentSuccess?.invoke(event)
    }

    override fun onEvent(event: CreatedReportEvent) {
        onCreatedReport?.invoke(event)
    }

    override fun onEvent(event: RequestPaymentEvent) {
        onRequestedPayment?.invoke(event)
    }

    override fun onEvent(event: UserAccountChangedEvent) {
        onAccountChanged?.invoke(event)
    }

    override fun onEvent(event: UpdatedReportEvent) {
        onUpdatedReport?.invoke(event)
    }

    override fun onEvent(event: OpenHomeTabEvent) {
        onOpenHomeTab?.invoke(event)
    }

    override fun onEvent(event: CloseLeftMenuEvent) {
        onCloseLeftMenu?.invoke(event)
    }

    override fun onEvent(event: DeletedNotiEvent) {
        onDeletedNoti?.invoke(event)
    }
}