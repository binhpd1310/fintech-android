package com.pcb.fintech.ui.base

class AlertData(var title: String?, var message: String?, var positiveText: String? = null) {

    fun getMessageAvoidNull(): String = message ?: ""
}