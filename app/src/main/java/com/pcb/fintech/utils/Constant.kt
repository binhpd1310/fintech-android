package com.pcb.fintech.utils

object Constant {
    const val DOMAIN_2 = "https://creditconnect.pcb.vn/"
    const val DOMAIN = "https://thongtintindung.pcb.vn/"
    const val AUTH_USERNAME = ""
    const val AUTH_PASSWORD = ""

    const val APP_FOLDER_REPORT = "PCB Reports"
    const val REPORT_FILE_NAME_FORMAT = "Report-%s"
    const val REPORT_FILE_EXTENSION = ".pdf"

    const val DEFAULT_TERM_LINK_EN = "https://pcb.vn/en/terms-and-conditions.html"
    const val DEFAULT_TERM_LINK_VI = "http://pcb.vn/dieu-khoan-su-dung.html"

    const val DEFAULT_INSTRUCTION_LINK_EN = "https://pcb.vn/en/"
    const val DEFAULT_INSTRUCTION_LINK_VI = "https://pcb.vn/"

    const val DEFAULT_FAQ_LINK_EN = "https://www.pcb.vn/en/faq/for-individual.html"
    const val DEFAULT_FAQ_LINK_VI = "https://www.pcb.vn/cau-hoi-thuong-gap/danh-cho-kh-vay.html"

    const val DEFAULT_EMAIL = "support@pcb.vn"
    const val DEFAULT_SUPPORT_PHONE = "+842839312266"
    const val DEFAULT_FB_LINK = "https://www.facebook.com/pcb.thongtintindung/"
}