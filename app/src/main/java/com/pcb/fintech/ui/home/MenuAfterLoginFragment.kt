package com.pcb.fintech.ui.home

import android.content.Intent
import android.graphics.Bitmap
import android.view.View
import com.pcb.fintech.BuildConfig
import com.pcb.fintech.R
import com.pcb.fintech.data.model.response.AccountInfo
import com.pcb.fintech.di.ViewModelFactory
import com.pcb.fintech.ui.base.BaseMVVMFragment
import com.pcb.fintech.ui.base.HasFragmentResult
import com.pcb.fintech.ui.home.menuitem.FeatureId
import com.pcb.fintech.ui.home.menuitem.HomeMenuClickHandler
import com.pcb.fintech.ui.home.menuitem.MenuClickAfterLoginHandler
import com.pcb.fintech.ui.views.SelectLanguageDialog
import com.pcb.fintech.utils.check
import kotlinx.android.synthetic.main.layout_menu_after_login.*
import kotlinx.android.synthetic.main.menu_item_feature_language.*
import kotlinx.android.synthetic.main.menu_item_feature_logout.*
import kotlinx.android.synthetic.main.menu_item_feature_noti.*
import kotlinx.android.synthetic.main.menu_item_feature_register_noti.*
import kotlinx.android.synthetic.main.menu_item_feature_report.*
import kotlinx.android.synthetic.main.menu_item_feature_setup.*
import kotlinx.android.synthetic.main.menu_item_feature_support.*
import kotlinx.android.synthetic.main.menu_item_feature_vote.*

class MenuAfterLoginFragment : BaseMVVMFragment<HomeVM, ViewModelFactory>() {

    private val mClickHandler: HomeMenuClickHandler by lazy {
        MenuClickAfterLoginHandler(context!!, { viewModel.logout() },
                { it?.let { startActivity(it) } }, { changeLanguage() })
    }

    override fun getLayoutRes(): Int {
        return R.layout.layout_menu_after_login
    }

    override fun getViewModelType(): Class<HomeVM> {
        return HomeVM::class.java
    }

    override fun viewModelInActivityScope() = true

    override fun setupViews(view: View) {
        layout_profile.setOnClickListener { mClickHandler.clickFeature(FeatureId.FT_PROFILE) }

        layout_report.setOnClickListener { mClickHandler.clickFeature(FeatureId.FT_REPORT_ID) }

        layout_setup.setOnClickListener { mClickHandler.clickFeature(FeatureId.FT_SETUP_ID) }

        layout_language.setOnClickListener { mClickHandler.clickFeature(FeatureId.FT_LANGUAGE_ID) }

        layout_noti.setOnClickListener { mClickHandler.clickFeature(FeatureId.FT_NOTIFICATION_ID) }

        layout_support.setOnClickListener { mClickHandler.clickFeature(FeatureId.FT_SUPPORT_ID) }

        layout_register_noti.setOnClickListener { mClickHandler.clickFeature(FeatureId.FT_REGISTER_NOTIFICATION_ID) }

        layout_vote.setOnClickListener { mClickHandler.clickFeature(FeatureId.FT_RATE_ID) }

        layout_logout.setOnClickListener { mClickHandler.clickFeature(FeatureId.FT_LOGOUT_ID) }

        tv_version.text = getString(R.string.app_version, BuildConfig.VERSION_NAME)
    }

    override fun onObserve(): (HomeVM.() -> Unit) = {
        doObserveAll(getProfileLD(), onSuccess = { fillAccountInfo(it) })
        doObserve(getDecodeAvatarLD()) { fillAvatar(it) }
    }

    private fun fillAccountInfo(accountInfo: AccountInfo?) {
        accountInfo ?: return

        tv_name.text = accountInfo.fullName
        tv_username.text = accountInfo.userName
        tv_phone.text = accountInfo.phone

        if (accountInfo.image != null) {
            viewModel.decodeBase64(accountInfo.image)
        }
    }

    private fun fillAvatar(bitmap: Bitmap?) {
        bitmap ?: return
        iv_avatar.setImageBitmap(bitmap)
    }

    private fun changeLanguage() {
        val language = viewModel.getLanguage()
        val dialog = SelectLanguageDialog.newInstance(language)
        dialog.setTargetFragment(this, LANGUAGE_CODE)
        dialog.show(parentFragmentManager, null)
    }

    override fun onFragmentResult(requestCode: Int, action: Int, extraData: Intent?) {
        check({ action == HasFragmentResult.ACTION_OK && requestCode == LANGUAGE_CODE }, ifTrue = func@{
            val language = SelectLanguageDialog.getExtraData(extraData) ?: return@func
            viewModel.changeLanguage(language)
        })
    }

    companion object {

        private const val LANGUAGE_CODE = 100

        fun newInstance() = MenuAfterLoginFragment()
    }
}