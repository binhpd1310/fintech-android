package com.pcb.fintech.data.model.response

enum class ProfileVerificationResult(val value: Int) {
    Success(0),
    UserNameExisted(1),
    EmailExisted(2),
    PhoneExisted(3),
    IdNumberExisted(4),
    Other(5);

    companion object {
        private val valueMap: HashMap<Int, ProfileVerificationResult> by lazy {
            return@lazy HashMap<Int, ProfileVerificationResult>().apply {
                for (item in ProfileVerificationResult.values()) {
                    put(item.value, item)
                }
            }
        }

        fun fromValue(value: Int?): ProfileVerificationResult {
            return valueMap[value] ?: Other
        }
    }
}