package com.pcb.fintech.ui.report

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import com.pcb.fintech.R
import com.pcb.fintech.data.model.body.ReportNoteBody
import com.pcb.fintech.data.model.other.Report
import com.pcb.fintech.data.model.other.ReportStatus
import com.pcb.fintech.data.model.other.ReportStatusDisplay
import com.pcb.fintech.di.ViewModelFactory
import com.pcb.fintech.eventbus.EventBus
import com.pcb.fintech.eventbus.event.UpdatedReportEvent
import com.pcb.fintech.ui.base.BaseMVVMFragment
import com.pcb.fintech.ui.base.OpenActivity
import com.pcb.fintech.ui.views.getTrimText
import com.pcb.fintech.ui.views.setVisibility
import com.pcb.fintech.utils.DateTimeUtil
import com.pcb.fintech.utils.getMoneyInText
import kotlinx.android.synthetic.main.fragment_report_detail.*
import java.io.File

class ReportDetailFragment : BaseMVVMFragment<ReportDetailVM, ViewModelFactory>() {

    private var doingAction: Int = -1

    override fun getLayoutRes(): Int {
        return R.layout.fragment_report_detail
    }

    override fun getViewModelType(): Class<ReportDetailVM> {
        return ReportDetailVM::class.java
    }

    override fun extractData(bundle: Bundle) {
        viewModel.setReport(bundle.getParcelable(EXTRA_REPORT))
    }

    override fun setupViews(view: View) {
        iv_back.setOnClickListener { sendToNav { it.goBack() } }

        iv_pay.setOnClickListener { goToPayment() }

        tv_pay.setOnClickListener { goToPayment() }

        iv_view.setOnClickListener { viewReport() }

        tv_view.setOnClickListener { viewReport() }

        iv_download.setOnClickListener { downloadReport() }

        tv_download.setOnClickListener { downloadReport() }

        delayOnMain({ getReportDetail() })
    }

    private fun getReportDetail() {
        val reportId = viewModel.getReportId() ?: return
        doObserveAll(viewModel.getReportById(reportId),
                onSuccess = { fillReport(it) },
                onError = { showSnackBar(rootView, it) })
    }

    private fun fillReport(report: Report?) {
        report ?: return

        viewModel.setReport(report)

        tv_amount.text = getMoneyInText(report.getTotal())

        tv_required_date.text = report.getCreatedDate(DateTimeUtil.DATE_TEXT_7)

        tv_report_export_date.text = report.getPublishedDate(DateTimeUtil.DATE_TEXT_7)

        val receiveMethods = resources.getStringArray(R.array.receive_methods)
        tv_report_receive.text = receiveMethods.get(report.reportType ?: 0)

        tv_note_customer.setText(report.comment)

        tv_note.setText(report.note)

        val paymentMethodsText = resources.getStringArray(R.array.text_payment_methods)
        tv_payment_method.text = paymentMethodsText.getOrNull(report.paymentMethodId ?: 0)

        val status = ReportStatus.fromValue(report.reportStatus)
        if (status != null) {
            val statusResId = ReportStatusDisplay.getStringRes(status)
            tv_status.text = getString(statusResId)
            val statusColorId = ReportStatusDisplay.getColorRes(status)
            tv_status.setBackgroundColor(ContextCompat.getColor(context!!, statusColorId))
        }

        when (status) {
            ReportStatus.APPROVED -> {
                setVisibility(View.GONE, iv_download, tv_download, iv_view, tv_view)
                setVisibility(View.VISIBLE, iv_pay, tv_pay)
            }
            ReportStatus.SENT -> {
                setVisibility(View.VISIBLE, iv_download, tv_download, iv_view, tv_view)
                setVisibility(View.GONE, iv_pay, tv_pay)
            }
            else -> {
                setVisibility(View.GONE, iv_download, tv_download, iv_view, tv_view, iv_pay, tv_pay)
            }
        }
    }

    private fun viewReport() {
        val report = viewModel.getReport() ?: return

        if (!viewModel.isWriteExternalStorageGranted()) {
            doingAction = ACTION_VIEW_REPORT
            requestWriteExternalStoragePermission()
            return
        }
        sendToNav { it.openFragment(ViewReportFragment.newInstance(report)) }
    }

    private fun downloadReport() {
        val report = viewModel.getReport() ?: return
        val reportId = report.id ?: return
        val reportKey = report.getKey()
        if (!viewModel.isWriteExternalStorageGranted()) {
            doingAction = ACTION_DOWNLOAD_REPORT
            requestWriteExternalStoragePermission()
            return
        }
        doObserveAll(viewModel.downloadReport(reportId, reportKey),
                onSuccess = { handleDownloadReportResult(it) },
                onError = { showSnackBar(rootView, it) })
    }

    private fun handleDownloadReportResult(data: File) {
        val title = getString(R.string.download_report_successful)
        val msg = getString(R.string.download_report_successful_msg, data.path)
        showDialog(title, msg, true)
    }

    private fun requestWriteExternalStoragePermission() {
        val permissions = arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE)
        requestPermissions(permissions, REQUEST_WRITE_EXTERNAL_STORAGE)
    }

    private fun onWriteStoragePermissionGranted() {
        if (doingAction == ACTION_VIEW_REPORT) {
            viewReport()
            return
        }
        if (doingAction == ACTION_DOWNLOAD_REPORT) {
            downloadReport()
            return
        }
    }

    // UI functions

    private fun sendUpdateReport() {
        val report = viewModel.getReport() ?: return
        val note = tv_note.getTrimText()
        if (note == report.note) return
        val updateReportNote = ReportNoteBody(report.id, report.reportStatus, note, note)
        doObserveAll(viewModel.updateReportNote(updateReportNote),
                onSuccess = { handleUpdateNoteResult(it) })
    }

    private fun handleUpdateNoteResult(result: Boolean) {
        if (result) {
            showSnackBar(rootView, R.string.text_update_info_successful)
            EventBus.getInstance().notify(UpdatedReportEvent())
        } else {
            showDialogMessage(R.string.error_common_title, R.string.text_update_info_failed, R.string.close)
        }
    }

    private fun goToPayment() {
        val reportId = viewModel.getReportId() ?: return
        val bundle = PaymentMainFragment.buildBundle(reportId)
        val intent = OpenActivity.getIntent(context, PaymentMainFragment::class.java, bundle)
        startActivity(intent)
    }

    override fun onObserve(): (ReportDetailVM.() -> Unit) = {
        doObserve(requestPaymentLD) { sendToNav { it.goBack() } }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>,
                                            grantResults: IntArray) {
        if (requestCode == REQUEST_WRITE_EXTERNAL_STORAGE) {
            val granted = grantResults[0] == PackageManager.PERMISSION_GRANTED
            if (granted) onWriteStoragePermissionGranted()
        }
    }

    companion object {
        private const val REQUEST_WRITE_EXTERNAL_STORAGE = 101
        private const val ACTION_VIEW_REPORT = 101
        private const val ACTION_DOWNLOAD_REPORT = 102
        private const val EXTRA_REPORT = "EXTRA_REPORT"

        fun getBundle(report: Report): Bundle {
            return Bundle().apply { putParcelable(EXTRA_REPORT, report) }
        }
    }
}