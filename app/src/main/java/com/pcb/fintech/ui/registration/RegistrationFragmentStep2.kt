package com.pcb.fintech.ui.registration

import android.os.CountDownTimer
import android.view.View
import com.pcb.fintech.R
import com.pcb.fintech.data.model.body.ProfileBody
import com.pcb.fintech.data.model.response.OtpCreated
import com.pcb.fintech.di.ViewModelFactory
import com.pcb.fintech.ui.base.BaseMVVMFragment
import com.pcb.fintech.ui.views.EasyTextWatcher
import com.pcb.fintech.ui.views.OnClickAndHideKeyboard
import com.pcb.fintech.ui.views.getTrimText
import com.pcb.fintech.utils.AndroidUtil
import com.pcb.fintech.utils.getHiddenPhone
import kotlinx.android.synthetic.main.fragment_registration_step2.*

class RegistrationFragmentStep2 : BaseMVVMFragment<RegistrationVM, ViewModelFactory>() {

    private lateinit var countTimer: MyCountDownTimer
    private var mMillisSecond: Long = 0

    override fun getLayoutRes(): Int {
        return R.layout.fragment_registration_step2
    }

    override fun getViewModelType(): Class<RegistrationVM> {
        return RegistrationVM::class.java
    }

    override fun viewModelInActivityScope(): Boolean = true

    override fun setupViews(view: View) {
        toolbar.setNavigationOnClickListener { viewModel.goPreviousStep() }

        tv_back.setOnClickListener { viewModel.goPreviousStep() }

        tv_next.setOnClickListener(OnClickAndHideKeyboard { verifyOtp() })

        edt_otp.addTextChangedListener(EasyTextWatcher(afterTextChanged = { validateInputData() }))

        tv_send_otp.setOnClickListener(OnClickAndHideKeyboard { resendOtp() })

        edt_otp.requestFocus()

        validateInputData()
    }

    private fun updateTimer(millisSecond: Long) {
        mMillisSecond = millisSecond
        if (millisSecond < 0) {
            tv_timer_label?.text = getString(R.string.text_otp_expired)
            tv_timer?.visibility = View.INVISIBLE
        } else {
            val second = millisSecond / 1000
            tv_timer_label?.text = getString(R.string.text_enter_otp_expired)
            tv_timer?.text = getString(R.string.text_enter_otp_timer, second.toInt().toString())
            tv_timer?.visibility = View.VISIBLE
        }
    }

    private fun validateInputData() {
        val otp = edt_otp.getTrimText()
        if (otp.length != 6) {
            tv_next.isEnabled = false
            return
        }
        tv_next.isEnabled = true
        AndroidUtil.hideKeyboard(activity)
    }

    private fun fillProfile(profile: ProfileBody) {
        val phoneNumber = profile.phone ?: return
        tv_notify.text = getString(R.string.text_notify_sent_otp, getHiddenPhone(phoneNumber))
        edt_otp.setText("")

        startCountDown()
    }

    private fun verifiedOtpDone() {
        stopTimerOtp()
    }

    override fun onObserve(): (RegistrationVM.() -> Unit) = {
        doObserve(getProfileLD()) { fillProfile(it) }
        doObserve(getStopTimerOtpLD()) { stopTimerOtp() }
        doObserve(getVerifiedOtpLD()) { verifiedOtpDone() }
    }

    private fun resendOtp() {
        val profile = viewModel.getProfileLD().value ?: return
        val phoneNumber = profile.phone ?: return
        doObserveAll(viewModel.createOtp(phoneNumber), onSuccess = { onCreateOtpResult(it) },
                onError = { showSnackBar(rootView, it) })
    }

    private fun verifyOtp() {
        if (mMillisSecond <= 0) {
            showSnackBar(rootView, R.string.text_otp_expired)
            return
        }

        val phoneNumber = viewModel.getProfileLD().value?.phone ?: return
        val otp = edt_otp.getTrimText()

        doObserveAll(viewModel.verifyOtp(phoneNumber, otp), onSuccess = { onVerifyOtpResult(it) })
    }

    private fun onVerifyOtpResult(result: Boolean) {
        if (result) {
            viewModel.notifyOtpVerified()
        } else {
            showDialogMessage(R.string.error_common_title, R.string.text_result_otp_not_correct, R.string.close)
        }
    }

    private fun onCreateOtpResult(result: OtpCreated) {
        val profile = viewModel.getProfileLD().value ?: return
        stopTimerOtp()
        fillProfile(profile)
    }

    private fun startCountDown() {
        stopTimerOtp()
        countTimer = MyCountDownTimer(onCountDownFinished, onCountDownTick, OTP_TIME, OTP_INTERVAL)
        countTimer.start()
    }

    private fun stopTimerOtp() {
        if (::countTimer.isInitialized) {
            countTimer.cancel()
            countTimer.clear()
        }
        updateTimer(-1)
    }

    private val onCountDownFinished: () -> Unit = {
        updateTimer(-1)
    }

    private val onCountDownTick: (millisSecond: Long) -> Unit = {
        updateTimer(it)
    }

    override fun onDestroyView() {
        stopTimerOtp()
        super.onDestroyView()
    }

    companion object {

        private const val OTP_TIME = 60000L
        private const val OTP_INTERVAL = 1000L

        fun newInstance() = RegistrationFragmentStep2()
    }

    class MyCountDownTimer(
            private var onFinish: (() -> Unit)?,
            private var onTick: ((millisSecond: Long) -> Unit)?,
            millisInFuture: Long, countDownInterval: Long
    ) : CountDownTimer(millisInFuture, countDownInterval) {

        override fun onFinish() {
            onFinish?.invoke()
        }

        override fun onTick(millisUntilFinished: Long) {
            onTick?.invoke(millisUntilFinished)
        }

        fun clear() {
            onFinish = null
            onTick = null
        }
    }
}