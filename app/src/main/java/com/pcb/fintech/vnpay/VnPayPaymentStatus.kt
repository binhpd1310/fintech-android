package com.pcb.fintech.vnpay

enum class VnPayPaymentStatus(val code: String, val msg: String) {
    SUCCESSFUL("00", "Thanh toán thành công!"),
    ORDER_NOT_FOUND("01", "Không tìm thấy báo cáo!"),
    ORDER_ALREADY_CONFIRMED("02", "Báo cáo đã được thanh toán!"),
    INVALID_AMOUNT("04", "Số tiền không hợp lệ!"),
    INVALID_SIGNATURE("97", "Thông tin thanh toán không hợp lệ!"),
    SERVER_ERROR("99", "Lỗi hệ thống!");

    companion object {
        private val valuesMap = mutableMapOf<String, VnPayPaymentStatus>().apply {
            for (value in values()) {
                this[value.code] = value
            }
        }

        fun fromCode(code: String?): VnPayPaymentStatus? {
            return valuesMap[code]
        }
    }
}