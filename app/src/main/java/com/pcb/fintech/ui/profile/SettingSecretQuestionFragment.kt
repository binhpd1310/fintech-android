package com.pcb.fintech.ui.profile

import android.content.Intent
import android.os.Parcelable
import android.text.Editable
import android.view.View
import android.widget.Toast
import com.pcb.fintech.R
import com.pcb.fintech.data.model.response.AccountInfo
import com.pcb.fintech.data.model.response.SecretQuestion
import com.pcb.fintech.data.model.response.SecretQuestionBody
import com.pcb.fintech.di.ViewModelFactory
import com.pcb.fintech.ui.base.BaseMVVMFragment
import com.pcb.fintech.ui.base.HasFragmentResult
import com.pcb.fintech.ui.views.SelectPlaceDialog
import com.pcb.fintech.ui.views.SimpleTextWatcher
import com.pcb.fintech.ui.views.getTrimText
import kotlinx.android.synthetic.main.fragment_setting_secret_question.*

class SettingSecretQuestionFragment :
        BaseMVVMFragment<SettingSecretQuestionVM, ViewModelFactory>() {

    private val secretQuestions = mutableListOf<SecretQuestion>()
    private var selectedQuestion: SecretQuestion? = null

    override fun getLayoutRes(): Int {
        return R.layout.fragment_setting_secret_question
    }

    override fun getViewModelType(): Class<SettingSecretQuestionVM> {
        return SettingSecretQuestionVM::class.java
    }

    override fun setupViews(view: View) {
        toolbar.setNavigationOnClickListener { sendToNav { it.goBack() } }

        tv_back.setOnClickListener { sendToNav { it.goBack() } }

        tv_secret_question.setOnClickListener { showSecretQuestions() }

        edt_secret_answer.addTextChangedListener(object : SimpleTextWatcher() {
            override fun afterTextChanged(s: Editable?) {
                validateInputData()
            }
        })

        tv_update.setOnClickListener { clickUpdate() }

        delayOnMain({
            doObserveAll(viewModel.getSecretQuestionsLD(), onSuccess = { onGetSecretQuestionsResult(it) },
                    onError = { showSnackBar(rootView, it) })
        })
    }

    private fun updateSecretQuestion(question: SecretQuestion?, answer: String?) {
        tv_secret_question.text = question?.name
        edt_secret_answer.setText(answer ?: "")
        selectedQuestion = question
    }

    private fun validateInputData(): Boolean {
        var isValid = true
        if (selectedQuestion?.id == null) isValid = false
        if (edt_secret_answer.getTrimText().isEmpty()) isValid = false
        tv_update.isEnabled = isValid
        return isValid
    }

    private fun clickUpdate() {
        if (!validateInputData()) {
            return
        }
        val questionId = selectedQuestion?.id ?: return
        val userId = viewModel.getUserId() ?: return
        val answer = edt_secret_answer.getTrimText()
        val body = SecretQuestionBody(questionId, answer, userId)
        doObserveAll(viewModel.updateSecretQuestionLD(body),
                onSuccess = { onUpdateSecretQuestionResult(it) })
    }

    private fun onGetSecretQuestionsResult(questions: List<SecretQuestion>) {
        secretQuestions.clear()
        secretQuestions.addAll(questions)

        doObserveAll(viewModel.getAccountInfoLD(), onSuccess = { onGetProfileResult(it) })
    }

    private fun onGetProfileResult(accountInfo: AccountInfo) {
        val secretQuestionId = accountInfo.secretQuestionId ?: return
        val answer = accountInfo.secretAnswer
        val question = secretQuestions.findLast { it.id == secretQuestionId }
        updateSecretQuestion(question, answer)
    }

    private fun onUpdateSecretQuestionResult(result: Boolean) {
        if (result) {
            viewModel.postHideLoading()
            showToast(R.string.text_update_info_successful, Toast.LENGTH_LONG)
            setFragmentResult(HasFragmentResult.ACTION_OK)
            sendToNav { it.goBack() }
        } else {
            showDialogMessage(R.string.error_common_title, R.string.text_update_info_failed, R.string.close)
        }
    }

    private fun showSecretQuestions() {
        val fragment = SelectPlaceDialog.newInstance(
                RequestCode.SECRET_QUESTION,
                getString(R.string.text_select_secret_question),
                secretQuestions as ArrayList<out Parcelable>
        )
        fragment.setTargetFragment(this, RequestCode.SECRET_QUESTION)
        fragment.show(parentFragmentManager, null)
    }

    override fun onFragmentResult(requestCode: Int, action: Int, extraData: Intent?) {
        if (action == HasFragmentResult.ACTION_OK) {
            if (requestCode == RequestCode.SECRET_QUESTION) {
                val question: SecretQuestion? =
                        extraData?.extras?.getParcelable(SelectPlaceDialog.EXTRA_DATA)
                updateSecretQuestion(question, "")
                return
            }
        }
    }

    companion object {

        fun newInstance() = SettingSecretQuestionFragment()
    }
}