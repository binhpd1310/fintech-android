package com.pcb.fintech.ui.profile

import android.view.View
import android.widget.Toast
import com.pcb.fintech.R
import com.pcb.fintech.data.model.body.ChangePasswordBody
import com.pcb.fintech.di.ViewModelFactory
import com.pcb.fintech.ui.base.BaseMVVMFragment
import com.pcb.fintech.ui.base.HasFragmentResult
import com.pcb.fintech.ui.views.EasyTextWatcher
import com.pcb.fintech.ui.views.gone
import com.pcb.fintech.ui.views.showOrHidePassword
import com.pcb.fintech.ui.views.visible
import com.pcb.fintech.utils.ValidationUtil.isValidPassword
import kotlinx.android.synthetic.main.fragment_setting_password.*

class SettingPasswordFragment : BaseMVVMFragment<SettingPasswordVM, ViewModelFactory>() {

    override fun getLayoutRes(): Int {
        return R.layout.fragment_setting_password
    }

    override fun getViewModelType(): Class<SettingPasswordVM> {
        return SettingPasswordVM::class.java
    }

    override fun setupViews(view: View) {
        toolbar.setNavigationOnClickListener { sendToNav { it.goBack() } }

        tv_back.setOnClickListener { sendToNav { it.goBack() } }

        edt_old_password.addTextChangedListener(textWatcher)
        edt_new_password.addTextChangedListener(textWatcher)
        edt_confirm_new_password.addTextChangedListener(textWatcher)

        iv_show_old_pass.setOnClickListener { edt_old_password.showOrHidePassword(iv_show_old_pass) }
        iv_show_new_pass.setOnClickListener { edt_new_password.showOrHidePassword(iv_show_new_pass) }
        iv_show_confirm_pass.setOnClickListener { edt_confirm_new_password.showOrHidePassword(iv_show_confirm_pass) }

        tv_update.setOnClickListener { clickUpdate() }
    }

    private val textWatcher = EasyTextWatcher(afterTextChanged = { validateInputData() })

    private fun validateInputData(): Boolean {
        var isValid = true
        if (edt_old_password.text.isEmpty()) isValid = false
        if (edt_new_password.text.isEmpty()) isValid = false
        if (edt_confirm_new_password.text.isEmpty()) isValid = false
        tv_update.isEnabled = isValid
        return isValid
    }

    private fun clickUpdate() {
        if (!validateInputData()) {
            return
        }
        val newPassword = edt_new_password.text.toString()
        if (!isValidPassword(newPassword)) {
            layout_pw_note.visible()
            return
        } else {
            layout_pw_note.gone()
        }
        val oldPassword = edt_old_password.text.toString()
        val confirmNewPassword = edt_confirm_new_password.text.toString()
        if (newPassword != confirmNewPassword) {
            showSnackBar(rootView, R.string.text_enter_confirm_new_password_invalid)
            return
        }
        val userId = viewModel.getUserId() ?: return
        val body = ChangePasswordBody(oldPassword, newPassword, userId)
        doObserveAll(viewModel.updatePasswordLD(body),
                onSuccess = { onUpdatePasswordResult(it) })
    }

    private fun onUpdatePasswordResult(result: Boolean) {
        if (result) {
            viewModel.postHideLoading()
            showToast(R.string.text_update_info_successful, Toast.LENGTH_LONG)
            setFragmentResult(HasFragmentResult.ACTION_OK)
            sendToNav { it.goBack() }
        } else {
            showDialogMessage(R.string.error_common_title, R.string.text_update_info_failed, R.string.close)
        }
    }

    companion object {

        fun newInstance() = SettingPasswordFragment()
    }
}