package com.pcb.fintech.di

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.pcb.fintech.data.source.CommonDataSource
import com.pcb.fintech.data.source.ProfileDataSource
import com.pcb.fintech.data.source.ReportDataSource
import com.pcb.fintech.data.source.local.PreferenceHelper
import com.pcb.fintech.ui.home.HomeVM
import com.pcb.fintech.ui.home.NotiDetailVM
import com.pcb.fintech.ui.home.RateAppVM
import com.pcb.fintech.ui.home.RegisterNotiVM
import com.pcb.fintech.ui.profile.*
import com.pcb.fintech.ui.registration.LoginVM
import com.pcb.fintech.ui.registration.RegistrationVM
import com.pcb.fintech.ui.report.*
import com.pcb.fintech.utils.SchedulerProvider
import javax.inject.Inject
import javax.inject.Singleton

/**
 * ViewModel factory class which keeps all the ViewModel instances
 */
@Singleton
class ViewModelFactory @Inject constructor(
        private val application: Application,
        private val schedulerProvider: SchedulerProvider,
        private val prefHelper: PreferenceHelper,
        private val commonDataSource: CommonDataSource,
        private val profileDataSource: ProfileDataSource,
        private val reportDataSource: ReportDataSource) : ViewModelProvider.Factory {

    private val genVMMap: Map<Class<*>, (() -> Any)> = mapOf(
            RegistrationVM::class.java to { RegistrationVM(application, schedulerProvider, prefHelper, commonDataSource, profileDataSource) },
            LoginVM::class.java to { LoginVM(application, schedulerProvider, prefHelper, commonDataSource) },
            HomeVM::class.java to { HomeVM(application, schedulerProvider, prefHelper, commonDataSource, profileDataSource, reportDataSource) },
            SettingMenuVM::class.java to { SettingMenuVM(application, schedulerProvider, profileDataSource) },
            SettingUserVM::class.java to { SettingUserVM(application, schedulerProvider, prefHelper, commonDataSource, profileDataSource) },
            SettingSecretQuestionVM::class.java to { SettingSecretQuestionVM(application, schedulerProvider, prefHelper, profileDataSource) },
            SettingPasswordVM::class.java to { SettingPasswordVM(application, schedulerProvider, prefHelper, profileDataSource) },
            SettingAddressVM::class.java to { SettingAddressVM(application, schedulerProvider, prefHelper, commonDataSource, profileDataSource) },
            CreateReportVM::class.java to { CreateReportVM(application, schedulerProvider, reportDataSource) },
            PaymentMainVM::class.java to { PaymentMainVM(application, schedulerProvider, prefHelper, commonDataSource, reportDataSource) },
            ReportDetailVM::class.java to { ReportDetailVM(application, schedulerProvider, reportDataSource) },
            NotiDetailVM::class.java to { NotiDetailVM(application, schedulerProvider, commonDataSource) },
            PaymentResultVM::class.java to { PaymentResultVM(application, schedulerProvider, reportDataSource) },
            VerifyOtpVM::class.java to { VerifyOtpVM(application, schedulerProvider, prefHelper, commonDataSource) },
            RegisterNotiVM::class.java to { RegisterNotiVM(application, schedulerProvider, prefHelper, commonDataSource) },
            RateAppVM::class.java to { RateAppVM(application, schedulerProvider) }
    )

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return genVMMap.getValue(modelClass).invoke() as T
    }
}
