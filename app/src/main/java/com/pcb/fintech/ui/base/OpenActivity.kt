package com.pcb.fintech.ui.base

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.pcb.fintech.ui.base.navigation.FragmentAnimation
import com.pcb.fintech.ui.base.navigation.NavController
import com.pcb.fintech.ui.base.navigation.NavControllerImpl
import com.pcb.fintech.ui.base.navigation.NavControllerProvider

class OpenActivity : AppCompatActivity(), NavControllerProvider {

    private val mNavController: NavController by lazy {
        NavControllerImpl(android.R.id.content, supportFragmentManager, this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val bundle = intent.extras
        val fragmentClazzName: String? = bundle?.getString(EXTRA_FRAGMENT, "")
        if (fragmentClazzName.isNullOrEmpty()) {
            finish()
            return
        }
        try {
            val fragmentClazz = Class.forName(fragmentClazzName) as Class<Fragment>
            val fragment = fragmentClazz.newInstance().apply { arguments = bundle }
            mNavController.openFragment(fragment, false, FragmentAnimation.TRANSITION_NONE)
        } catch (e: ClassNotFoundException) {
            e.printStackTrace()
        } catch (e: InstantiationException) {
            e.printStackTrace()
        } catch (e: IllegalAccessException) {
            e.printStackTrace()
        }
    }

    override fun onBackPressed() {
        mNavController.goBack()
    }

    override fun onDestroy() {
        super.onDestroy()
        mNavController.clearController()
    }

    override fun provideNavController(): NavController {
        return mNavController
    }

    companion object {

        private const val EXTRA_FRAGMENT = "EXTRA_FRAGMENT"

        @JvmOverloads
        fun getIntent(context: Context?, fragmentClass: Class<out Fragment>,
                      args: Bundle? = null): Intent? {
            context ?: return null

            val intent = Intent(context, OpenActivity::class.java)
            intent.putExtra(EXTRA_FRAGMENT, fragmentClass.name)
            if (args != null) intent.putExtras(args)
            return intent
        }

    }
}