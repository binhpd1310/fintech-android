package com.pcb.fintech.ui.profile

object RequestCode {
    const val SECRET_QUESTION = 100
    const val PROVINCE = 101
    const val DISTRICT = 102
    const val WARD = 103
    const val TEMP_PROVINCE = 104
    const val TEMP_DISTRICT = 105
    const val TEMP_WARD = 106
    const val REPORT_PROVINCE = 107
    const val REPORT_DISTRICT = 108
    const val REPORT_WARD = 109
}