package com.pcb.fintech.ui.report

import android.app.Application
import com.pcb.fintech.data.source.ReportDataSource
import com.pcb.fintech.ui.base.BaseViewModel
import com.pcb.fintech.utils.SchedulerProvider
import com.pcb.fintech.vnpay.VnPayResponse

class PaymentResultVM
constructor(application: Application, schedulerProvider: SchedulerProvider,
            private val reportDataSource: ReportDataSource
) : BaseViewModel(application, schedulerProvider) {

    fun paymentOnlineStatusLD(vnPayResponse: VnPayResponse) =
            resultLiveData { reportDataSource.paymentOnlineStatus(vnPayResponse) }
}