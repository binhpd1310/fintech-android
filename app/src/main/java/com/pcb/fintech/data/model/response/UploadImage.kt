package com.pcb.fintech.data.model.response

import com.google.gson.annotations.SerializedName

data class UploadImage
constructor(@SerializedName("confidence") val confidence: Float?,
            @SerializedName("faceId") val faceId: String?,
            @SerializedName("faceRectangle") val faceRectangle: Unit?,
            @SerializedName("imagePath") val imagePath: String?)