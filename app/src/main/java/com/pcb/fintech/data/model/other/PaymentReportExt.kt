package com.pcb.fintech.data.model.other

import com.pcb.fintech.data.model.body.PaymentBody
import com.pcb.fintech.data.model.response.PaymentReportResponse
import com.pcb.fintech.utils.formatDoubleToLong

fun PaymentReportResponse.toPaymentOnlineBody(paymentMethodId: Int,
                                              shippingMethodId: Int) = PaymentBody(
        reportId = this.reportId,
        userId = this.userId,
        reportFee = this.getReportFee(),
        paymentFee = this.getPaymentFee(),
        paymentMethodId = paymentMethodId,
        shippingMethod = shippingMethodId,
        shippingFee = this.getShippingFee(),
        shippingAddressId = this.shippingAddressId,
        promotionCode = this.promotionCode,
        taxAmount = this.getTaxAmount(),
        total = this.getTotal()
)

fun PaymentReportResponse.toPaymentOfflineBody(paymentMethodId: Int,
                                               shippingMethodId: Int,
                                               shippingAddressId: Int?,
                                               shippingFee: Double?,
                                               total: Double?) = PaymentBody(
        reportId = this.reportId,
        userId = this.userId,
        reportFee = this.getReportFee(),
        paymentFee = this.getPaymentFee(),
        paymentMethodId = paymentMethodId,
        shippingMethod = shippingMethodId,
        shippingFee = shippingFee.formatDoubleToLong(),
        shippingAddressId = shippingAddressId,
        promotionCode = this.promotionCode,
        taxAmount = this.getTaxAmount(),
        total = total.formatDoubleToLong()
)