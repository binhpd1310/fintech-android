package com.pcb.fintech.ui.views

import androidx.recyclerview.widget.LinearLayoutManager

class SimpleEndlessScrollListener(layoutManager: LinearLayoutManager,
                                  private val onLoadMore: (() -> Unit)? = null)
    : EndlessRecyclerViewScrollListener(layoutManager) {

    override fun onLoadMore(page: Int, totalItemsCount: Int) {
        onLoadMore?.invoke()
    }
}