package com.pcb.fintech.data.model.other

enum class CardType(val value: Int) {
    UNKNOWN(0),
    IDENTITY_CARD(1), // Chứng minh nhân dân
    ID_CARD(2), // Căn cước
    PASSPORT(3); // Hộ chiếu

    companion object {
        private val valueMap by lazy {
            return@lazy mutableMapOf<Int, CardType>().apply {
                for (m in values()) {
                    put(m.value, m)
                }
            }
        }

        fun fromValue(value: Int?): CardType {
            return valueMap[value] ?: UNKNOWN
        }
    }
}