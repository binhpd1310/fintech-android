package com.pcb.fintech.ui.registration

import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.pcb.fintech.R
import com.pcb.fintech.di.ViewModelFactory
import com.pcb.fintech.ui.base.BaseMVVMFragment
import kotlinx.android.synthetic.main.fragment_registration.*

class RegistrationFragment : BaseMVVMFragment<RegistrationVM, ViewModelFactory>() {

    override fun getViewModelType(): Class<RegistrationVM> {
        return RegistrationVM::class.java
    }

    override fun getLayoutRes(): Int {
        return R.layout.fragment_registration
    }

    override fun viewModelInActivityScope(): Boolean = true

    override fun setupViews(view: View) {
        val adapter = RegistrationAdapter(childFragmentManager).apply {
            addFragment(RegistrationFragmentStep1.newInstance())
            addFragment(RegistrationFragmentStep2.newInstance())
            addFragment(RegistrationFragmentStep3.newInstance())
        }
        viewpager.adapter = adapter
        dots_indicator.setViewPager(viewpager)
        dots_indicator.dotsClickable = false
    }

    override fun onObserve(): (RegistrationVM.() -> Unit) = {
        doObserve(getBackActionLD()) { sendToNav { it.goBack() } }
        doObserve(getCreatedOtpLD()) { viewpager.setCurrentItem(1, true) }
        doObserve(getVerifiedOtpLD()) { viewpager.setCurrentItem(2, true) }
    }

    override fun handleBack(): Boolean {
        viewModel.clearTimer()
        val curPos = viewpager.currentItem
        if (curPos > 0) {
            viewpager.setCurrentItem(curPos - 1, true)
            return true
        }
        return false
    }

    class RegistrationAdapter(fm: FragmentManager) :
            FragmentStatePagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

        private val fragments = mutableListOf<Fragment>()

        fun addFragment(fragment: Fragment) {
            if (!fragments.contains(fragment)) fragments.add(fragment)
        }

        override fun getItem(position: Int): Fragment {
            return fragments[position]
        }

        override fun getCount(): Int {
            return fragments.size
        }
    }
}