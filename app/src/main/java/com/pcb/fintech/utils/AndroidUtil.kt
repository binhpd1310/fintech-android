package com.pcb.fintech.utils

import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.content.res.Resources
import android.net.Uri
import android.view.View
import android.view.inputmethod.InputMethodManager
import com.pcb.fintech.R
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader
import kotlin.math.roundToInt


object AndroidUtil {

    fun readAssetFile(context: Context, fileName: String): String? {
        var input: BufferedReader? = null
        try {
            input = BufferedReader(InputStreamReader(context.assets.open(fileName)))
            var line: String?
            val buffer = StringBuilder()
            line = input.readLine()
            while (line != null) {
                buffer.append(line)
                line = input.readLine()
            }

            input.close()
            return buffer.toString()
        } catch (e: IOException) {
            if (input != null)
                try {
                    input.close()
                } catch (e1: IOException) {
                    e1.printStackTrace()
                }

            e.printStackTrace()
        }

        return null
    }

    fun dpToPx(dp: Int): Int {
        val density = Resources.getSystem().displayMetrics.density
        return (dp * density).roundToInt()
    }

    fun hideKeyboard(activity: Activity?) {
        if (activity == null) return
        val imm = activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(activity.window.decorView.windowToken, 0)
    }

    fun hideKeyboard(view: View?) {
        var mFocusView: View? = view ?: return

        val context: Context = view.context
        if (context is Activity) {
            mFocusView = context.currentFocus
        }

        mFocusView?.clearFocus() ?: return

        val manager = mFocusView.context
                .getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        manager.hideSoftInputFromWindow(mFocusView.windowToken, InputMethodManager.HIDE_NOT_ALWAYS)
    }

    fun showKeyboard(activity: Activity?) {
        activity ?: return

        val imm = activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0)
    }

    fun openWebLink(context: Context, url: String?) {
        url ?: return

        val intent = Intent(Intent.ACTION_VIEW)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        intent.data = Uri.parse(url)
        try {
            context.startActivity(intent)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun openGooglePlay(context: Context, packageName: String) {
        val intent = Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=$packageName"))
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        intent.setPackage("com.android.vending")
        try {
            context.startActivity(intent)
        } catch (e: ActivityNotFoundException) {
            val defaultIntent = Intent(Intent.ACTION_VIEW, Uri.parse(context.getString(R.string.text_sharing_url, packageName)))
            defaultIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            context.startActivity(defaultIntent)
        } catch (e: Exception) {
            // Do nothing
        }
    }

    fun shareMessage(context: Context, title: String, content: String, message: String) {
        val intent = Intent(Intent.ACTION_SEND)
        intent.type = "text/plain"
        intent.putExtra(Intent.EXTRA_SUBJECT, title)
        intent.putExtra(Intent.EXTRA_TEXT, content)
        val chooserIntent = Intent.createChooser(intent, message)
        chooserIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        try {
            context.startActivity(chooserIntent)
        } catch (e: Exception) {
            // Do nothing
        }
    }

    fun sendEmail(context: Context, toEmail: String) {
        val intent = Intent(Intent.ACTION_SEND)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        intent.data = Uri.parse("mailto:")
        intent.type = "message/rfc822"
        intent.putExtra(Intent.EXTRA_EMAIL, arrayOf(toEmail))
        try {
            context.startActivity(intent)
        } catch (e: Exception) {
            // Do nothing
        }
    }

    fun callPhoneNumber(context: Context, phoneNumber: String) {
        val intent = Intent(Intent.ACTION_DIAL)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        intent.data = Uri.fromParts("tel", phoneNumber, null)
        try {
            context.startActivity(intent)
        } catch (e: Exception) {
            // Do nothing
        }
    }
}