package com.pcb.fintech.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.core.view.GravityCompat
import androidx.fragment.app.Fragment
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.tabs.TabLayoutMediator
import com.pcb.fintech.R
import com.pcb.fintech.di.ViewModelFactory
import com.pcb.fintech.ui.base.BaseMVVMFragment
import com.pcb.fintech.ui.base.OpenActivity
import com.pcb.fintech.ui.registration.LoginFragment
import com.pcb.fintech.utils.getHomeIntent
import kotlinx.android.synthetic.main.fragment_main.*
import kotlin.system.exitProcess

class MainFragment : BaseMVVMFragment<HomeVM, ViewModelFactory>() {

    private var mIsSessionExpired: Boolean? = false

    private var mQuitApp: Boolean = false

    private var isShowLogin = false

    private lateinit var mOnPageChangeCallback: ViewPager2.OnPageChangeCallback

    override fun getLayoutRes(): Int {
        return R.layout.fragment_main
    }

    override fun getViewModelType(): Class<HomeVM> {
        return HomeVM::class.java
    }

    override fun viewModelInActivityScope(): Boolean = true

    override fun extractData(bundle: Bundle) {
        mIsSessionExpired = bundle.getBoolean(EXTRA_SESSION_EXPIRED, false)
    }

    override fun setupViews(view: View) {
        if (mIsSessionExpired == true) {
            showSnackBar(rootView, R.string.session_expired)
        }

        setupViews(viewModel.isLogin())
    }

    private fun setupViews(isLogin: Boolean) {
        setupLeftMenu(isLogin)
        setupTabViews(isLogin)

        mOnPageChangeCallback = object : ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                if (position == 1 && !viewModel.isLogin() && !isShowLogin) {
                    forceGoToLogin()
                    isShowLogin = true
                } else {
                    isShowLogin = false
                }
            }
        }

    }

    override fun onStart() {
        super.onStart()
        view_pager.registerOnPageChangeCallback(mOnPageChangeCallback)
    }

    override fun onStop() {
        super.onStop()
        view_pager.unregisterOnPageChangeCallback(mOnPageChangeCallback)
    }

    override fun handleBack(): Boolean {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            closeLeftMenu()
            return true
        }

        if (view_pager.currentItem != 0) {
            viewModel.openHomeTab(0)
            return true
        }

        if (!mQuitApp) {
            showDialog(title = null, cancelable = false,
                    message = getString(R.string.quit_app_msg),
                    positiveTextColor = R.color.black_1,
                    negativeTextColor = R.color.colorPrimary,
                    onNegative = { mQuitApp = false },
                    onPositive = {
                        mQuitApp = true
                        sendToNav { it.goBack() }
                    })
            return true
        }

        viewModel.quitApp()

        return false
    }

    private fun openLeftMenu() {
        drawer_layout.openDrawer(GravityCompat.START)
    }

    private fun closeLeftMenu() {
        drawer_layout.closeDrawer(GravityCompat.START)
    }

    private fun logoutDone() {
        setupViews(false)
    }

    override fun onObserve(): (HomeVM.() -> Unit) = {
        doObserve(getUpdateLanguageLD()) {
            onChangeLanguage()
        }
        doObserve(openLeftMenuLD()) {
            if (it) openLeftMenu()
            else closeLeftMenu()
        }
        doObserve(logoutLD()) {
            logoutDone()
        }
        doObserve(quitAppLD()) {
            exitProcess(0)
        }
        doObserve(openHomeTabLD()) {
            view_pager.setCurrentItem(it, false)
        }
    }

    private fun onChangeLanguage() {
        startActivity(getHomeIntent(context!!))
        activity?.finish()
    }

    private fun setupLeftMenu(isLogin: Boolean) {
        val fragment: Fragment = if (isLogin) MenuAfterLoginFragment.newInstance()
        else MenuBeforeLoginFragment.newInstance()
        childFragmentManager.beginTransaction().replace(R.id.menu_frame, fragment).commit()
    }

    private fun setupTabViews(isLogin: Boolean) {
        val adapter = MainAdapter(activity!!, { getTabSize(isLogin) }) { getTabFragment(isLogin, it) }
        view_pager.adapter = adapter
        view_pager.offscreenPageLimit = adapter.itemCount
        view_pager.isUserInputEnabled = false
        TabLayoutMediator(tab_layout, view_pager) { tab, pos ->
            tab.customView = getTabViewAt(isLogin, pos)
        }.attach()
        tab_layout.getTabAt(0)?.select()
    }

    private fun getTabFragment(isLogin: Boolean, position: Int): Fragment {
        return when (position) {
            0 -> HomeFragmentV2.newInstance()
            1 -> ReportMainFragment.newInstance()
            2 -> if (isLogin) NotificationFragment.newInstance() else TermFragment.newInstance()
            3 -> if (isLogin) TermFragment.newInstance() else SupportFragment.newInstance()
            else -> SupportFragment.newInstance()
        }
    }

    private fun getTabSize(isLogin: Boolean): Int {
        return if (isLogin) TAB_SIZE_AFTER_LOGIN else TAB_SIZE_BEFORE_LOGIN
    }

    @StringRes
    private fun getTabNameId(isLogin: Boolean, position: Int): Int {
        return when (position) {
            0 -> R.string.text_home
            1 -> R.string.text_request_title
            2 -> if (isLogin) R.string.text_notification_title else R.string.title_term
            3 -> if (isLogin) R.string.title_term else R.string.title_support
            else -> R.string.title_support
        }
    }

    @DrawableRes
    private fun getTabIconId(isLogin: Boolean, position: Int): Int {
        return when (position) {
            0 -> R.drawable.ic_tab_home
            1 -> R.drawable.ic_tab_report
            2 -> if (isLogin) R.drawable.ic_tab_notification else R.drawable.ic_tab_terms
            3 -> if (isLogin) R.drawable.ic_tab_terms else R.drawable.ic_tab_support
            else -> R.drawable.ic_tab_support
        }
    }

    private fun getTabViewAt(isLogin: Boolean, position: Int): View {
        val tabName = getTabNameId(isLogin, position)
        val tabIconId = getTabIconId(isLogin, position)
        val tabView = LayoutInflater.from(context).inflate(R.layout.item_tab_home, null)
        (tabView.findViewById(R.id.iv_icon) as ImageView).setImageResource(tabIconId)
        (tabView.findViewById(R.id.tv_title) as TextView).text = getString(tabName)
        return tabView
    }

    private fun forceGoToLogin() {
        delayOnMain({ startActivity(OpenActivity.getIntent(context, LoginFragment::class.java)) }, 300)
        delayOnMain({
            view_pager.setCurrentItem(0, true)
            isShowLogin = false
        }, 500)
    }

    companion object {

        private const val TAB_SIZE_BEFORE_LOGIN = 4
        private const val TAB_SIZE_AFTER_LOGIN = 5

        private const val EXTRA_SESSION_EXPIRED = "EXTRA_SESSION_EXPIRED"

        fun getBundle(isSessionExpired: Boolean) = Bundle().apply {
            putBoolean(EXTRA_SESSION_EXPIRED, isSessionExpired)
        }
    }
}