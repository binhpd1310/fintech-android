package com.pcb.fintech.ui.base.navigation

interface HasBackAction {

    fun handleBack(): Boolean
}