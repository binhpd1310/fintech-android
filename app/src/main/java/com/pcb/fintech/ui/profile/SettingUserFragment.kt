package com.pcb.fintech.ui.profile

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.os.Parcelable
import android.provider.MediaStore
import android.text.Editable
import android.view.View
import android.widget.Toast
import com.pcb.fintech.R
import com.pcb.fintech.data.model.Consumer3
import com.pcb.fintech.data.model.body.ProfileBody
import com.pcb.fintech.data.model.response.*
import com.pcb.fintech.di.ViewModelFactory
import com.pcb.fintech.ui.base.BaseMVVMFragment
import com.pcb.fintech.ui.base.HasFragmentResult
import com.pcb.fintech.ui.views.*
import com.pcb.fintech.utils.Constant
import com.pcb.fintech.utils.DateTimeUtil
import com.pcb.fintech.utils.ImageUtil.generateTimeStampPhotoFile
import com.pcb.fintech.utils.ValidationUtil.isValidPhone
import com.pcb.fintech.utils.getImageUri
import com.pcb.fintech.utils.validation.PhoneNumberValidator.Companion.RESULT_PHONE_VALID
import kotlinx.android.synthetic.main.fragment_setting_user.*
import java.io.File
import java.util.*
import kotlin.collections.ArrayList

class SettingUserFragment : BaseMVVMFragment<SettingUserVM, ViewModelFactory>() {

    private var faceImageFile: File? = null
    private var faceImageBase64: String? = null
    private var dateOfBirth: Date? = null

    private var accountInfo: AccountInfo? = null
    private var provinces: ArrayList<Province>? = null
    private var districts: ArrayList<District>? = null
    private var wards: ArrayList<Ward>? = null

    private var selectedProvince: Province? = null
    private var selectedDistrict: District? = null
    private var selectedWard: Ward? = null

    override fun getLayoutRes(): Int {
        return R.layout.fragment_setting_user
    }

    override fun getViewModelType(): Class<SettingUserVM> {
        return SettingUserVM::class.java
    }

    override fun setupViews(view: View) {
        toolbar.setNavigationOnClickListener { sendToNav { it.goBack() } }

        tv_back.setOnClickListener { sendToNav { it.goBack() } }

        iv_face.setOnClickListener { takePhoto() }

        tv_update.setOnClickListener { verifyProfile() }

        layout_dob.setOnClickListener { showPickDateOfBirth() }

        tv_province_of_birth.setOnClickListener { showProvinces() }

        tv_district_of_birth.setOnClickListener { showDistricts() }

        tv_ward_of_birth.setOnClickListener { showWards() }

        delayOnMain({ viewModel.prepareData() })
    }

    private val textWatcher = object : SimpleTextWatcher() {
        override fun afterTextChanged(s: Editable?) {
            validateInputData()
        }
    }

    private fun addTextWatcher() {
        edt_full_name.addTextChangedListener(textWatcher)
        edt_phone.addTextChangedListener(textWatcher)
    }

    private fun removeTextWatcher() {
        edt_full_name.removeTextChangedListener(textWatcher)
        edt_phone.removeTextChangedListener(textWatcher)
    }

    private fun showPickDateOfBirth() {
        val cal = Calendar.getInstance().apply { add(Calendar.YEAR, -14) }
        val datePickerDialog = newInstanceWithLimit(dateOfBirth, cal.time) {
            dateOfBirth = it
            tv_date_of_birth.text = DateTimeUtil.formatDate(it, DateTimeUtil.DATE_TEXT_2)
            validateInputData()
        }
        datePickerDialog.show(activity?.fragmentManager, "")
    }

    private fun showProvinces() {
        provinces ?: return
        val fragment = SelectPlaceDialog.newInstance(
                RequestCode.PROVINCE,
                getString(R.string.text_select_province_title),
                provinces as ArrayList<out Parcelable>?
        )
        fragment.setTargetFragment(this, RequestCode.PROVINCE)
        fragment.show(parentFragmentManager, null)
    }

    private fun showDistricts() {
        districts ?: return
        val province = selectedProvince ?: return
        val districtList = districts?.filter { it.provinceId == province.id } ?: return
        val fragment = SelectPlaceDialog.newInstance(
                RequestCode.DISTRICT,
                getString(R.string.text_select_district_title),
                districtList as ArrayList<out Parcelable>?
        )
        fragment.setTargetFragment(this, RequestCode.DISTRICT)
        fragment.show(parentFragmentManager, null)
    }

    private fun showWards() {
        val district = selectedDistrict ?: return
        val wardList = wards?.filter { it.districtId == district.id } ?: return
        val fragment = SelectPlaceDialog.newInstance(
                RequestCode.WARD,
                getString(R.string.text_select_ward_title),
                wardList as ArrayList<out Parcelable>?
        )
        fragment.setTargetFragment(this, RequestCode.WARD)
        fragment.show(parentFragmentManager, null)
    }

    private fun validateInputData(): Boolean {
        var isValid = true
        if (edt_full_name.isNullOrEmpty()) isValid = false
        if (edt_phone.isNullOrEmpty()) isValid = false
        if (tv_date_of_birth.isNullOrEmpty()) isValid = false
        tv_update.isEnabled = isValid
        return isValid
    }

    private fun verifyProfile() {
        if (!validateInputData()) return
        if (isValidPhone(edt_phone.getTrimText()) != RESULT_PHONE_VALID) {
            showSnackBar(rootView, R.string.error_phone_invalid)
            return
        }
        val accountInfo = viewModel.getAccountInfo() ?: return
        val userName = accountInfo.userName ?: return
        val phone = edt_phone.getTrimTextOrNull() ?: return
        val profileBody = ProfileBody.makeBodyForVerifyInfo(userName, accountInfo.email, phone)

        doObserveAll(viewModel.verifyProfileLD(profileBody),
                { onVerifyProfileResult(it) },
                { showSnackBar(rootView, it) })
    }

    private fun onVerifyProfileResult(result: ProfileVerificationResult) {
        if (result == ProfileVerificationResult.Success) {
            updateProfile()
        } else {
            handleProfileError(result)
        }
    }

    private fun handleProfileError(result: ProfileVerificationResult?) {
        val errorMsg: Int = when (result) {
            ProfileVerificationResult.UserNameExisted -> R.string.error_username_exists
            ProfileVerificationResult.EmailExisted -> R.string.error_email_exists
            ProfileVerificationResult.PhoneExisted -> R.string.error_phone_exists
            else -> R.string.error_profile_invalid
        }
        showSnackBar(rootView, errorMsg)
    }

    private fun updateProfile() {
        val accountInfo = viewModel.getAccountInfo() ?: return
        val fullName = edt_full_name.getTrimTextOrNull()
        val phone = edt_phone.getTrimTextOrNull()
        val birthday = DateTimeUtil.formatDate(dateOfBirth!!, DateTimeUtil.DATE_TEXT_3)
        val placeOfBirth = edt_place_of_birth.getTrimTextOrNull()
        val updateAccountInfo = accountInfo.copy(
                image = faceImageBase64, fullName = fullName, phone = phone, email = accountInfo.email,
                birthday = birthday, placeOfBirth = placeOfBirth, provinceId = selectedProvince?.id,
                districtId = selectedDistrict?.id, wardId = selectedWard?.id
        )
        doObserveAll(viewModel.updateAccountInfoLD(updateAccountInfo),
                { onUpdateAccountResult(it) })
    }

    private fun onUpdateAccountResult(result: Boolean) {
        if (result) {
            viewModel.postHideLoading()
            showToast(R.string.text_update_info_successful, Toast.LENGTH_LONG)
            setFragmentResult(HasFragmentResult.ACTION_OK)
            sendToNav { it.goBack() }
        } else {
            showDialogMessage(R.string.error_common_title, R.string.text_update_info_failed, R.string.close)
        }
    }

    private fun onPrepareDataSuccess(result: Consumer3<List<Province>, List<District>, List<Ward>>) {
        provinces = ArrayList<Province>().apply { addAll(result.data1) }
        districts = ArrayList<District>().apply { addAll(result.data2) }
        wards = ArrayList<Ward>().apply { addAll(result.data3) }

        doObserveAll(viewModel.getAccountInfoLD(),
                { onPreparedData(it) },
                { showDialogMessage(R.string.error_common_title, R.string.error_account_info_invalid, R.string.close) })
    }

    private fun onPreparedData(accountInfo: AccountInfo) {
        val province = provinces?.find { it.id == accountInfo.provinceId }
        val district = districts?.find { it.id == accountInfo.districtId }
        val ward = wards?.find { it.id == accountInfo.wardId }
        updateProvince(province)
        updateDistrict(district)
        updateWard(ward)
        fillAccountInfo(accountInfo)
    }

    private fun fillAccountInfo(accountInfo: AccountInfo?) {
        accountInfo ?: return
        this.accountInfo = accountInfo

        edt_login_name.setText(accountInfo.userName)
        edt_full_name.setText(accountInfo.fullName)
        edt_phone.setText(accountInfo.phone)
        edt_email.setText(accountInfo.email)
        edt_place_of_birth.setText(accountInfo.placeOfBirth)
        tv_province_of_birth.setText(selectedProvince?.name ?: "")
        tv_district_of_birth.setText(selectedDistrict?.name ?: "")
        tv_ward_of_birth.setText(selectedWard?.name ?: "")

        if (accountInfo.getBirthDay() != null) {
            tv_date_of_birth.text = accountInfo.getBirthDayText(DateTimeUtil.DATE_TEXT_2)
            dateOfBirth = accountInfo.getBirthDay()
        }

        if (accountInfo.image != null) {
            faceImageBase64 = accountInfo.image
            // Decode base64 image
            viewModel.decodeBase64(accountInfo.image)
        } else {
            val imageUrl = Constant.DOMAIN + accountInfo.imagePath
            // Encode base64 image
            viewModel.processNewFaceImage(imageUrl)
        }

        validateInputData()
        addTextWatcher()
    }

    private fun updateProvince(province: Province?) {
        tv_province_of_birth.text = province?.name ?: ""
        selectedProvince = province
    }

    private fun updateDistrict(district: District?) {
        tv_district_of_birth.text = district?.name ?: ""
        selectedDistrict = district
    }

    private fun updateWard(ward: Ward?) {
        tv_ward_of_birth.text = ward?.name ?: ""
        selectedWard = ward
    }

    private fun takePhoto() {
        val appContext = activity?.applicationContext ?: return
        if (viewModel.isCameraPermissionGranted()) {
            val imageFile = generateTimeStampPhotoFile(appContext, MEDIA_DIR)
            faceImageFile = imageFile
            val imageUri = getImageUri(appContext, imageFile)
            val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri)
            startActivityForResult(intent, REQUEST_CODE_FACE_IMAGE)
        } else {
            val permissions = arrayOf(Manifest.permission.CAMERA)
            requestPermissions(permissions, REQUEST_CAMERA)
        }
    }

    private fun fillAvatar(bitmap: Bitmap?) {
        bitmap ?: return
        iv_face.setImageBitmap(bitmap)
    }

    override fun onObserve(): (SettingUserVM.() -> Unit) = {
        doObserve(base64EncoderLD) { faceImageBase64 = it }
        doObserve(base64DecoderLD) { fillAvatar(it) }
        doObserve(prepareDataLD) { onPrepareDataSuccess(it) }
    }

    /**
     * onActivityResult
     */
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_CODE_FACE_IMAGE) {
                val tempFile = faceImageFile ?: return
                viewModel.processNewFaceImage(tempFile)
            }
        }
    }

    override fun onRequestPermissionsResult(
            requestCode: Int,
            pers: Array<out String>,
            results: IntArray
    ) {
        if (requestCode == REQUEST_CAMERA) {
            val granted = results[0] == PackageManager.PERMISSION_GRANTED
            if (granted) takePhoto()
        }
    }

    override fun onFragmentResult(requestCode: Int, action: Int, extraData: Intent?) {
        if (action == HasFragmentResult.ACTION_OK) {
            if (requestCode == RequestCode.PROVINCE) {
                val province: Province? =
                        extraData?.extras?.getParcelable(SelectPlaceDialog.EXTRA_DATA)
                updateProvince(province)
                updateDistrict(null)
                updateWard(null)
                validateInputData()
                return
            }
            if (requestCode == RequestCode.DISTRICT) {
                val district: District? =
                        extraData?.extras?.getParcelable(SelectPlaceDialog.EXTRA_DATA)
                updateDistrict(district)
                updateWard(null)
                validateInputData()
                return
            }
            if (requestCode == RequestCode.WARD) {
                val ward: Ward? = extraData?.extras?.getParcelable(SelectPlaceDialog.EXTRA_DATA)
                updateWard(ward)
                validateInputData()
                return
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        removeTextWatcher()
    }

    companion object {
        private const val REQUEST_CAMERA = 100
        private const val REQUEST_CODE_FACE_IMAGE = 103
        private const val MEDIA_DIR = "media"

        fun newInstance() = SettingUserFragment()
    }
}