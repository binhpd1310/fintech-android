package com.pcb.fintech.data.model.other

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import com.pcb.fintech.utils.DateTimeUtil
import com.pcb.fintech.utils.formatDoubleToLong
import kotlinx.android.parcel.IgnoredOnParcel
import kotlinx.android.parcel.Parcelize
import java.util.*

@Parcelize
data class Report(
        @SerializedName("id") val id: Int? = null,
        @SerializedName("paymentMethodId") val paymentMethodId: Int? = null,
        @SerializedName("reportStatus") val reportStatus: Int? = null,
        @SerializedName("timeCreated") val timeCreated: String? = null,
        @SerializedName("publishedDate") val publishedDate: String? = null,
        @SerializedName("reportType") val reportType: Int? = null,
        @SerializedName("total") val total: Double? = null,
        @SerializedName("comment") val comment: String? = null,
        @SerializedName("note") val note: String? = null,
        @SerializedName("reportFile") val reportFile: String? = null,
        @SerializedName("isDeleted") val isDeleted: Boolean? = null
) : Parcelable {

    constructor() : this(null)

    @IgnoredOnParcel
    private var createdDate: Date? = null

    @IgnoredOnParcel
    private var tmpPublishedDate: Date? = null

    fun getCreatedDate(format: String): String {
        if (createdDate == null) {
            createdDate = DateTimeUtil.toDate(timeCreated, DateTimeUtil.DATE_TEXT_3)
        }
        val date = createdDate ?: return ""
        return DateTimeUtil.formatDate(date, format)
    }

    fun getPublishedDate(format: String): String {
        if (tmpPublishedDate == null) {
            tmpPublishedDate = DateTimeUtil.toDate(publishedDate, DateTimeUtil.DATE_TEXT_3)
        }
        val date = tmpPublishedDate ?: return ""
        return DateTimeUtil.formatDate(date, format)
    }

    fun getKey(): String {
        val timeCreated = getCreatedDate(DateTimeUtil.DATE_TEXT_5)
        return String.format("$id-$timeCreated")
    }

    fun getTotal(): Long {
        return total.formatDoubleToLong()
    }
}