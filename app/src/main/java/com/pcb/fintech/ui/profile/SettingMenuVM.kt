package com.pcb.fintech.ui.profile

import android.app.Application
import androidx.lifecycle.LiveData
import com.pcb.fintech.data.model.ResponseResult
import com.pcb.fintech.data.model.response.AccountInfo
import com.pcb.fintech.data.source.ProfileDataSource
import com.pcb.fintech.ui.base.BaseViewModel
import com.pcb.fintech.ui.base.SingleLiveEvent
import com.pcb.fintech.utils.SchedulerProvider

class SettingMenuVM
constructor(application: Application, schedulerProvider: SchedulerProvider,
            private val profileDataSource: ProfileDataSource) :
        BaseViewModel(application, schedulerProvider) {

    private val hasProfileChangedLD = SingleLiveEvent<Boolean>()

    fun hasProfileChangedLD(): LiveData<Boolean> = hasProfileChangedLD

    fun getProfileLD(): LiveData<ResponseResult<AccountInfo>> {
        return resultLiveData(isLoading = false) { profileDataSource.getProfileData() }
    }

    fun needFetchProfile() {
        hasProfileChangedLD.postValue(true)
    }
}