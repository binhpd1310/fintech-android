package com.pcb.fintech.data.model.body

import com.google.gson.annotations.SerializedName

data class VerifyFaceBody constructor(@SerializedName("sourceFaceId1") val sourceFaceId1: String,
                                      @SerializedName("imagePath1") val imagePath1: String,
                                      @SerializedName("sourceFaceId2") val sourceFaceId2: String,
                                      @SerializedName("imagePath2") val imagePath2: String)