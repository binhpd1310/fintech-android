package com.pcb.fintech.data.model.response

import com.google.gson.annotations.SerializedName

data class PaymentOnlineResultResponse
constructor(@SerializedName("rspCode") val rspCode: String? = null,
            @SerializedName("message") val message: String? = null)