package com.pcb.fintech.ui.report

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.pcb.fintech.R
import com.pcb.fintech.data.model.Consumer3
import com.pcb.fintech.data.model.ResponseResult
import com.pcb.fintech.data.model.body.AddressType
import com.pcb.fintech.data.model.body.PaymentBody
import com.pcb.fintech.data.model.other.PaymentMethod
import com.pcb.fintech.data.model.other.ShippingMethod
import com.pcb.fintech.data.model.other.toPaymentOfflineBody
import com.pcb.fintech.data.model.other.toPaymentOnlineBody
import com.pcb.fintech.data.model.response.*
import com.pcb.fintech.data.source.CommonDataSource
import com.pcb.fintech.data.source.ReportDataSource
import com.pcb.fintech.data.source.local.PreferenceHelper
import com.pcb.fintech.data.utils.ResponseResultExt
import com.pcb.fintech.ui.base.AlertData
import com.pcb.fintech.ui.base.BaseViewModel
import com.pcb.fintech.ui.base.getString
import com.pcb.fintech.utils.SchedulerProvider

class PaymentMainVM
constructor(application: Application, schedulerProvider: SchedulerProvider,
            private val preferenceHelper: PreferenceHelper,
            private val commonDataSource: CommonDataSource,
            private val reportDataSource: ReportDataSource)
    : BaseViewModel(application, schedulerProvider) {

    private var mReportId: Int? = null

    private var mShippingAddress: AddressInfo? = null

    private var mPaymentReport: PaymentReportResponse? = null

    private var mShippingFee: ShippingFee? = null

    private val _paymentOnlineLD = MutableLiveData<PaymentOnlineResponse>()

    internal val paymentOnlineLD: LiveData<PaymentOnlineResponse> = _paymentOnlineLD

    private val _paymentOfflineLD = MutableLiveData<Boolean>()

    internal val paymentOfflineLD: LiveData<Boolean> = _paymentOfflineLD

    val prepareDataLD = MutableLiveData<Consumer3<List<Province>, List<District>, List<Ward>>>()

    fun setReportId(reportId: Int?) {
        this.mReportId = reportId
    }

    fun getReportId(): Int? = mReportId

    fun setPaymentReport(paymentReport: PaymentReportResponse) {
        mPaymentReport = paymentReport
    }

    fun getShippingAddress(): AddressInfo? {
        if (mShippingAddress == null) {
            val addressList = preferenceHelper.getAddressInfo()
            mShippingAddress = addressList?.findLast { it.addressType == AddressType.REPORT.value }
        }
        return mShippingAddress
    }

    fun prepareData() {
        callApi(call1 = { commonDataSource.getProvinces() },
                call2 = { commonDataSource.getDistricts() },
                call3 = { commonDataSource.getWards() },
                transform = { data1, data2, data3 -> Consumer3(data1, data2, data3) },
                onSuccess = { prepareDataLD.postValue(it) },
                onFailed = null,
                loading = true)
    }

    fun getShippingFee(): LiveData<ResponseResult<ShippingFee>> {
        val fee = mShippingFee
        return if (fee != null) {
            resultLiveData(false) { ResponseResultExt.getResult(fee) }
        } else {
            resultLiveData { reportDataSource.getShippingFee() }
        }
    }

    fun saveShippingFee(shippingFee: ShippingFee) {
        this.mShippingFee = shippingFee
    }

    fun getPaymentReport(reportId: Int) = resultLiveData { reportDataSource.getPaymentReport(reportId) }

    fun makePayment(paymentMethod: PaymentMethod?, shippingMethod: ShippingMethod?) {
        val paymentBody = getPaymentBody(paymentMethod, shippingMethod) ?: return

        if (paymentMethod == PaymentMethod.VNPAY) {
            paymentOnline(paymentBody)
        } else if (paymentMethod == PaymentMethod.INTERNET_BANKING) {
            paymentOffline(paymentBody)
        }
    }

    private fun paymentOnline(body: PaymentBody) = callApi(
            call = { reportDataSource.paymentOnline(body) },
            onSuccess = { _paymentOnlineLD.postValue(it) },
            onFailed = { showPaymentError() },
            isLoading = true)

    private fun paymentOffline(body: PaymentBody) = callApi(
            call = { reportDataSource.paymentOffline(body) },
            onSuccess = { if (it) _paymentOfflineLD.postValue(it) else showPaymentError() },
            onFailed = { showPaymentError() },
            isLoading = true)

    private fun showPaymentError() {
        errorLV.postValue(AlertData(getString(R.string.error_common_title), getString(R.string.payment_report_failed), getString(R.string.close)))
    }

    private fun getPaymentBody(paymentMethod: PaymentMethod?, shippingMethod: ShippingMethod?): PaymentBody? {
        paymentMethod ?: return null
        shippingMethod ?: return null
        val reportPayment = mPaymentReport ?: return null

        return if (shippingMethod == ShippingMethod.ONLINE) {
            reportPayment.toPaymentOnlineBody(paymentMethod.value, shippingMethod.value)
        } else {
            val total = (reportPayment.total ?: 0.0) + (mShippingFee?.value ?: 0.0)
            reportPayment.toPaymentOfflineBody(paymentMethod.value, shippingMethod.value,
                    getShippingAddress()?.id, mShippingFee?.value, total)
        }
    }
}