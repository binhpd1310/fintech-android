package com.pcb.fintech.ui.home

import android.view.View
import com.pcb.fintech.R
import com.pcb.fintech.di.ViewModelFactory
import com.pcb.fintech.eventbus.EventBus
import com.pcb.fintech.eventbus.event.OpenHomeTabEvent
import com.pcb.fintech.ui.base.BaseMVVMFragment
import kotlinx.android.synthetic.main.fragment_support.*
import kotlinx.android.synthetic.main.item_sp_fbook.*
import kotlinx.android.synthetic.main.item_sp_instruction_for_use.*
import kotlinx.android.synthetic.main.item_sp_questions.*

class SupportFragment : BaseMVVMFragment<HomeVM, ViewModelFactory>() {

    override fun getLayoutRes(): Int {
        return R.layout.fragment_support
    }

    override fun getViewModelType(): Class<HomeVM> {
        return HomeVM::class.java
    }

    override fun viewModelInActivityScope() = true

    override fun setupViews(view: View) {
        toolbar.setNavigationOnClickListener { handleBack() }

        layout_instruction.setOnClickListener { viewModel.openUsageLink() }

        layout_questions.setOnClickListener { viewModel.openFaqLink() }

        layout_email.setOnClickListener { viewModel.sendEmail() }

        layout_phone.setOnClickListener { viewModel.openPhone() }

        layout_fb.setOnClickListener { viewModel.openFbLink() }
    }

    override fun onObserve(): (HomeVM.() -> Unit) = {
        doObserve(getUsageLinkLD()) { tv_item_sug.text = it.title }
        doObserve(getFaqLinkLD()) { tv_item_faq.text = it.title }
        doObserve(getFacebookLinkLD()) { tv_item_fb.text = it.title }
    }

    override fun handleBack(): Boolean {
        EventBus.getInstance().notify(OpenHomeTabEvent(0))
        return true
    }

    companion object {

        fun newInstance() = SupportFragment()
    }
}