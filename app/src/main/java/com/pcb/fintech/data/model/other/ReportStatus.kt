package com.pcb.fintech.data.model.other

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
enum class ReportStatus constructor(val value: Int) : Parcelable {
    WAITING(1), // waiting for approval
    REJECTED(2),
    APPROVED(3),
    CONFIRM_PAYMENT(4),
    PAID(5),
    GOT_PDF(6),
    PUBLISHED(7),
    SENT(8), // sent/completed report
    CANCEL(9);

    companion object {
        private val valueMap by lazy {
            return@lazy HashMap<Int, ReportStatus>().apply {
                for (v in values()) {
                    put(v.value, v)
                }
            }
        }

        fun fromValue(type: Int?): ReportStatus? {
            return valueMap[type]
        }
    }
}