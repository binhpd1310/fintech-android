package com.pcb.fintech.ui.views

import android.content.Context
import android.graphics.Canvas
import android.graphics.Path
import android.graphics.RectF
import android.util.AttributeSet
import android.view.View
import com.pcb.fintech.R
import com.pcb.fintech.utils.AndroidUtil

class RoundView : View {

    private val mPath: Path = Path()

    private var mCornerRadius = 4

    constructor(context: Context?) : super(context)

    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs) {
        attrs?.let { parseAttrs(it) }
    }

    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        attrs?.let { parseAttrs(it) }
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        val changedRect = RectF(0f, 0f, w.toFloat(), h.toFloat())
        resetPath(changedRect)
    }

    override fun draw(canvas: Canvas) {
        val save: Int = canvas.save()
        canvas.clipPath(mPath)
        super.draw(canvas)
        canvas.restoreToCount(save)
    }

    override fun dispatchDraw(canvas: Canvas) {
        val save = canvas.save()
        canvas.clipPath(mPath)
        super.dispatchDraw(canvas)
        canvas.restoreToCount(save)
    }

    private fun resetPath(rectF: RectF) {
        mPath.reset()
        mPath.addRoundRect(rectF, getCornerRadiusInPx(), getCornerRadiusInPx(), Path.Direction.CW)
        mPath.close()
    }

    private fun parseAttrs(attrs: AttributeSet) {
        val typeArray = context.theme.obtainStyledAttributes(attrs,
                R.styleable.RoundStyle, 0, 0)
        try {
            mCornerRadius = typeArray.getInt(R.styleable.RoundStyle_roundCornerRadius, 4)
        } finally {
            typeArray.recycle()
        }
    }

    private fun getCornerRadiusInPx(): Float {
        return AndroidUtil.dpToPx(mCornerRadius).toFloat()
    }
}