package com.pcb.fintech.data.model.body

import com.google.gson.annotations.SerializedName

class CreateNewsletter(@SerializedName("appUserId") val appUserId: Int?,
                       @SerializedName("email") val email: String?,
                       @SerializedName("status") val status: Int = 1)