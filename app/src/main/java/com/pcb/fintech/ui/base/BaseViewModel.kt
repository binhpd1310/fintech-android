package com.pcb.fintech.ui.base

import android.app.Application
import androidx.annotation.CallSuper
import androidx.lifecycle.*
import com.pcb.fintech.FintechApp
import com.pcb.fintech.R
import com.pcb.fintech.data.model.ResponseResult
import com.pcb.fintech.data.source.local.PreferenceHelper
import com.pcb.fintech.data.source.remote.CustomHttpException
import com.pcb.fintech.eventbus.EventBus
import com.pcb.fintech.eventbus.observer.EventObserver
import com.pcb.fintech.language.Language
import com.pcb.fintech.utils.SchedulerProvider
import com.yariksoffice.lingver.Lingver
import io.reactivex.disposables.CompositeDisposable
import kotlinx.coroutines.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File
import java.net.HttpURLConnection

abstract class BaseViewModel
constructor(val app: Application, val schedulerProvider: SchedulerProvider) : AndroidViewModel(app) {

    private val disposables = CompositeDisposable()

    private val viewModelJob = SupervisorJob()

    private val observers = mutableSetOf<ViewModelObserver>()

    private var mPrefHelper: PreferenceHelper = PreferenceHelper(app)

    private val loadingLV = MutableLiveData<LoadingStatus>()

    @JvmField
    val errorLV = MutableLiveData<AlertData>()

    fun getContext() = getApplication<FintechApp>()

    fun getLoadingLV(): LiveData<LoadingStatus> = loadingLV

    protected open fun saveLanguage(language: Language) {
        mPrefHelper.saveLanguage(language)
        Lingver.getInstance().setLocale(getContext(), language.code, language.country)
    }

    protected fun launchDataLoad(work: ((coroutineScope: CoroutineScope) -> Unit)?) {
        viewModelScope.launch {
            withContext(Dispatchers.Default) {
                work?.invoke(this)
            }
        }
    }

    protected fun createTextBody(text: String): RequestBody {
        return RequestBody.create(MediaType.parse("text/plain"), text)
    }

    protected fun createFileBody(name: String, file: File): MultipartBody.Part {
        val requestFile = RequestBody.create(MediaType.parse("multipart/form-mock"), file)
        return MultipartBody.Part.createFormData(name, file.name, requestFile)
    }

    protected fun registerEvent(eventType: String, eventObserver: EventObserver) {
        observers.add(EventBusEvent(eventType, eventObserver))
        EventBus.getInstance().register(eventType, eventObserver)
    }

    private class EventBusEvent(private val eventType: String,
                                private val eventObserver: EventObserver) : ViewModelObserver {

        override fun onCleared() {
            EventBus.getInstance().unregister(eventType, eventObserver)
        }
    }

    @CallSuper
    override fun onCleared() {
        super.onCleared()
        disposables.clear()
        viewModelJob.cancel()
        observers.forEach { it.onCleared() }
        observers.clear()
    }

    fun postShowLoading() {
        loadingLV.postValue(LoadingStatus.SHOW_LOADING)
    }

    fun postHideLoading() {
        loadingLV.postValue(LoadingStatus.HIDE_LOADING)
    }

    private suspend fun <T> handleResult(result: ResponseResult<T>,
                                         onSuccess: ((data: T) -> Unit)? = null,
                                         onFailed: ((message: String) -> Unit)? = null,
                                         isLoading: Boolean = true) {
        if (result is ResponseResult.Loading) {
            if (isLoading) postShowLoading()
            return
        }
        if (result is ResponseResult.Error) {
            if (isLoading) postHideLoading()
            if (onFailed != null) {
                withContext(Dispatchers.Main) { onFailed.invoke(result.getMessageAvoidNull()) }
            } else {
                if (result.throwable is CustomHttpException
                        && (result.throwable.code() == HttpURLConnection.HTTP_UNAUTHORIZED ||
                                result.throwable.code() == HttpURLConnection.HTTP_NO_CONTENT)) {
                    // Do nothing
                } else {
                    errorLV.postValue(AlertData(getString(R.string.error_common_title),
                            getString(R.string.error_common_message), getString((R.string.close))))
                }
            }
            return
        }
        if (result is ResponseResult.Success) {
            if (isLoading) postHideLoading()
            withContext(Dispatchers.Main) { onSuccess?.invoke(result.data) }
        }
    }

    fun <T> callApi(call: suspend () -> ResponseResult<T>,
                    onSuccess: ((data: T) -> Unit)? = null,
                    onFailed: ((message: String) -> Unit)? = null,
                    isLoading: Boolean = true,
                    onTerminate: (() -> Unit)? = null) {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                handleResult(ResponseResult.Loading, null, null, isLoading)

                val response = call.invoke()

                handleResult(response, onSuccess, onFailed, isLoading)

                onTerminate?.invoke()
            }
        }
    }

    fun <T1, T2, T3, R> callApi(call1: suspend () -> ResponseResult<T1>,
                                call2: suspend () -> ResponseResult<T2>,
                                call3: suspend () -> ResponseResult<T3>,
                                transform: ((result1: T1, result2: T2, result3: T3) -> R),
                                onSuccess: ((data: R) -> Unit)? = null,
                                onFailed: ((message: String?) -> Unit)? = null,
                                loading: Boolean = true) {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                handleResult(ResponseResult.Loading, null, null, loading)

                val response1 = async { call1.invoke() }
                val response2 = async { call2.invoke() }
                val response3 = async { call3.invoke() }
                val result1 = response1.await()
                val result2 = response2.await()
                val result3 = response3.await()
                if (result1 !is ResponseResult.Success) {
                    handleResult(result1, null, onFailed, loading)
                    return@withContext
                }
                if (result2 !is ResponseResult.Success) {
                    handleResult(result2, null, onFailed, loading)
                    return@withContext
                }
                if (result3 !is ResponseResult.Success) {
                    handleResult(result3, null, onFailed, loading)
                    return@withContext
                }
                val result = transform(result1.data, result2.data, result3.data)
                handleResult(ResponseResult.Success(result), onSuccess, onFailed, loading)
            }
        }
    }

    companion object {

        fun <T> resultLiveData(isLoading: Boolean = true,
                               call: suspend () -> ResponseResult<T>): LiveData<ResponseResult<T>> {
            return liveData(Dispatchers.IO) {
                if (isLoading) emit(ResponseResult.Loading)
                val response = call.invoke()
                emit(response)
            }
        }
    }
}