package com.pcb.fintech.di.component

import com.pcb.fintech.FintechApp
import com.pcb.fintech.di.module.AppModule
import com.pcb.fintech.di.module.FragmentBuilderModule
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Component(modules = [
    AndroidSupportInjectionModule::class,
    AppModule::class,
    FragmentBuilderModule::class
])
@Singleton
interface AppComponent : AndroidInjector<FintechApp> {

    @Component.Builder
    abstract class Builder : AndroidInjector.Builder<FintechApp>()

    override fun inject(app: FintechApp)
}
