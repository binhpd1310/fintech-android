package com.pcb.fintech.ui.home.viewholder

import android.view.View
import com.pcb.fintech.R
import com.pcb.fintech.data.model.response.Notification
import com.pcb.fintech.ui.adapter.MultiTypeVH
import com.pcb.fintech.utils.DateTimeUtil
import kotlinx.android.synthetic.main.item_notification.*

class NotificationVH(val view: View,
                     onClickItem: ((pos: Int) -> Unit)?) : MultiTypeVH<Notification>(view) {

    init {
        view.setOnClickListener { onClickItem?.invoke(adapterPosition) }
    }

    override fun bindView(item: Notification) {
        tv_subject.text = item.subject
        tv_content.text = item.notifyContent
        tv_date_label.text = item.getCreatedDateText(DateTimeUtil.DATE_TEXT_2)
        if (item.isRead == true) {
            iv_email.setImageResource(R.drawable.icon_thu_da_doc)
        } else {
            iv_email.setImageResource(R.drawable.icon_thu_chua_doc)
        }
    }

}