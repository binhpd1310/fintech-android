package com.pcb.fintech.utils

import android.util.Log

object LogUtil {

    private const val defaultTag = "Fintech - Android"

    @JvmStatic
    fun debug(message: String?) {
        Log.d(defaultTag, message)
    }

    @JvmStatic
    fun debug(tag: String?, message: String?) {
        Log.d(tag, message)
    }

    @JvmStatic
    fun error(message: String?) {
        Log.e(defaultTag, message)
    }

    @JvmStatic
    fun error(tag: String?, message: String?) {
        Log.e(tag, message)
    }

    @JvmStatic
    fun error(throwable: Throwable) {
        Log.e(defaultTag, throwable.toString())
    }
}