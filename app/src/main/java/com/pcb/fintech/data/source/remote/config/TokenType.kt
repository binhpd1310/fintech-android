package com.pcb.fintech.data.source.remote.config

enum class TokenType(val value: String) {
    BEARER("Bearer"),
    BASIC("Basic");

    companion object {

        fun fromValue(value: String?): TokenType? {
            value ?: return null
            for (type in values()) {
                if (type.value == value) return type
            }
            return null
        }
    }
}