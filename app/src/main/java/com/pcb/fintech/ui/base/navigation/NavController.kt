package com.pcb.fintech.ui.base.navigation

import android.content.Intent
import androidx.fragment.app.Fragment
import com.pcb.fintech.R

interface NavController {

    val activeFragment: Fragment?

    fun openFragment(fragment: Fragment, addToBackStack: Boolean = true,
                     @FragmentAnimation animationType: Int = FragmentAnimation.TRANSITION_SLIDE_LEFT_RIGHT,
                     forResult: Pair<Fragment, Int>? = null)

    fun openActivity(intent: Intent?, enterAnim: Int = R.anim.slide_in_from_right,
                     exitAnim: Int = R.anim.slide_out_to_left)

    fun setResult(action: Int, intent: Intent?)

    fun goBack()

    fun clearController()

    companion object {
        const val KEY_ANIM_ENTER_RES_ID = "KEY_ANIM_ENTER_RES_ID"
        const val KEY_ANIM_EXIT_RES_ID = "KEY_ANIM_EXIT_RES_ID"
    }
}