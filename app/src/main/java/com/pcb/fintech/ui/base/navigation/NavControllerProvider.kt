package com.pcb.fintech.ui.base.navigation

interface NavControllerProvider {

    fun provideNavController(): NavController
}