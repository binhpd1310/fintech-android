package com.pcb.fintech.ui.adapter

import android.content.Context
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.extensions.LayoutContainer

abstract class MultiTypeVH<O>(itemView: View) : RecyclerView.ViewHolder(itemView), LayoutContainer {

    abstract fun bindView(item: O)

    override val containerView: View?
        get() = itemView

    protected val context: Context = itemView.context.applicationContext
}