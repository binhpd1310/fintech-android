package com.pcb.fintech.ui.profile

import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.pcb.fintech.data.model.Consumer3
import com.pcb.fintech.data.model.body.AddressesBody
import com.pcb.fintech.data.model.response.District
import com.pcb.fintech.data.model.response.Province
import com.pcb.fintech.data.model.response.Ward
import com.pcb.fintech.data.source.CommonDataSource
import com.pcb.fintech.data.source.ProfileDataSource
import com.pcb.fintech.data.source.local.PreferenceHelper
import com.pcb.fintech.ui.base.BaseViewModel
import com.pcb.fintech.utils.SchedulerProvider

class SettingAddressVM
constructor(application: Application, schedulerProvider: SchedulerProvider,
            private val preferenceHelper: PreferenceHelper,
            private val commonDataSource: CommonDataSource,
            private val profileDataSource: ProfileDataSource) : BaseViewModel(application, schedulerProvider) {

    val prepareDataLD = MutableLiveData<Consumer3<List<Province>, List<District>, List<Ward>>>()

    fun getUserId() = preferenceHelper.getUserId()

    fun prepareData() {
        callApi(call1 = { commonDataSource.getProvinces() },
                call2 = { commonDataSource.getDistricts() },
                call3 = { commonDataSource.getWards() },
                transform = { data1, data2, data3 -> Consumer3(data1, data2, data3) },
                onSuccess = { prepareDataLD.postValue(it) },
                onFailed = null,
                loading = true)
    }

    fun getAddressesLD() = resultLiveData(false) { profileDataSource.getProfileAddress() }

    fun updateAddressesLD(addressesBody: AddressesBody) = resultLiveData { profileDataSource.updateAddresses(addressesBody) }
}
