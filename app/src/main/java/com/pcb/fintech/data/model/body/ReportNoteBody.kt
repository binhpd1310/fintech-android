package com.pcb.fintech.data.model.body

import com.google.gson.annotations.SerializedName

data class ReportNoteBody
constructor(@SerializedName("ReportId") val reportId: Int? = null,
            @SerializedName("Status") val status: Int? = null,
            @SerializedName("Note") val note: String? = null,
            @SerializedName("Comment") val Comment: String? = null)