package com.pcb.fintech.data.model.body

import com.google.gson.annotations.SerializedName

class ExtractCardBody(@SerializedName("identityType") val identityType: Int,
                      @SerializedName("frontCard") val frontCard: String,
                      @SerializedName("backsideCard") val backsideCard: String)