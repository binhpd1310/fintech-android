package com.pcb.fintech.data.model

data class Consumer2<T1, T2>(val data1: T1?, val data2: T2?)