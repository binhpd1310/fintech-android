package com.pcb.fintech.data.model.response

import com.google.gson.annotations.SerializedName

class AddressInfo constructor(
        @SerializedName("addressType") val addressType: Int? = null,
        @SerializedName("appUserId") val appUserId: Int? = null,
        @SerializedName("city") val city: String? = null,
        @SerializedName("contactName") val contactName: String? = null,
        @SerializedName("districtId") val districtId: Int? = null,
        @SerializedName("fullAddress") val fullAddress: String? = null,
        @SerializedName("id") val id: Int? = null,
        @SerializedName("phone") val phone: String? = null,
        @SerializedName("provinceId") val provinceId: Int? = null,
        @SerializedName("wardId") val wardId: Int? = null)