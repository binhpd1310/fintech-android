package com.pcb.fintech.data.model.body

import com.google.gson.annotations.SerializedName

data class PaymentBody constructor(
    @SerializedName("reportId") val reportId: Int? = null,
    @SerializedName("userId") val userId: Int? = null,
    @SerializedName("reportFee") val reportFee: Long? = null,
    @SerializedName("paymentFee") val paymentFee: Long? = null,
    @SerializedName("paymentMethodId") val paymentMethodId: Int? = null,
    @SerializedName("shippingMethod") val shippingMethod: Int? = null,
    @SerializedName("shippingFee") val shippingFee: Long? = null,
    @SerializedName("shippingAddressId") val shippingAddressId: Int? = null,
    @SerializedName("promotionCode") val promotionCode: String? = null,
    @SerializedName("taxAmount") val taxAmount: Long? = null,
    @SerializedName("total") val total: Long? = null
)