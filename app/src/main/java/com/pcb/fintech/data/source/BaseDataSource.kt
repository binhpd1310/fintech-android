package com.pcb.fintech.data.source

import com.pcb.fintech.data.model.ResponseResult
import com.pcb.fintech.data.source.remote.CustomHttpException
import com.pcb.fintech.utils.LogUtil
import retrofit2.Response

/**
 * Abstract Base Data source class getInstance error handling
 */
abstract class BaseDataSource {

    protected suspend fun <T : Any, R : Any> getResult(
            call: suspend () -> Response<T>,
            onErrorReturn: (() -> T)? = null,
            transform: (callResult: T) -> R,
            errorMsgExtractor: ((response: Response<T>) -> String)? = null
    ): ResponseResult<R> {
        try {
            val response = call.invoke()
            val body = response.body()
            if (response.isSuccessful && body != null) {
                return ResponseResult.Success(body).map(transform)
            }
            if (onErrorReturn != null) {
                return ResponseResult.Success(onErrorReturn.invoke()).map(transform)
            }
            val errorMsg = errorMsgExtractor?.invoke(response)
            return ResponseResult.Error(CustomHttpException(response, errorMsg))
        } catch (e: Exception) {
            LogUtil.error(e)
            return ResponseResult.Error(e)
        }
    }
}