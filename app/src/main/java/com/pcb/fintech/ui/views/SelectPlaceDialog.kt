package com.pcb.fintech.ui.views

import android.content.Intent
import android.os.Bundle
import android.os.Parcelable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatDialogFragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.pcb.fintech.R
import com.pcb.fintech.data.model.response.District
import com.pcb.fintech.data.model.response.Province
import com.pcb.fintech.data.model.response.SecretQuestion
import com.pcb.fintech.data.model.response.Ward
import com.pcb.fintech.ui.adapter.MultiTypeAdapter
import com.pcb.fintech.ui.adapter.ViewTypeData
import com.pcb.fintech.ui.base.HasFragmentResult
import com.pcb.fintech.ui.profile.RequestCode
import com.pcb.fintech.ui.profile.viewholder.DistrictVH
import com.pcb.fintech.ui.profile.viewholder.ProvinceVH
import com.pcb.fintech.ui.profile.viewholder.SecretQuestionVH
import com.pcb.fintech.ui.profile.viewholder.WardVH
import kotlinx.android.synthetic.main.fragment_select_place.*

class SelectPlaceDialog : AppCompatDialogFragment() {

    private var data: List<out Parcelable>? = null
    private var requestType: Int? = null
    private var requestTitle: String? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_select_place, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setCancelable(true)
        setupViews()
    }

    override fun onResume() {
        super.onResume()
        dialog?.window?.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
    }

    private fun setupViews() {
        requestType = arguments?.getInt(EXTRA_REQUEST_CODE)
        requestTitle = arguments?.getString(EXTRA_REQUEST_TITLE)

        tv_title.text = requestTitle

        setupData()
    }

    private fun setupData() {
        data = arguments?.getParcelableArrayList(EXTRA_DATA)
        rv_data.layoutManager = LinearLayoutManager(context)

        val adapter = getAdapter() ?: return
        rv_data.adapter = adapter
        adapter.submitList(data as List<Nothing>)
    }

    private fun getAdapter(): MultiTypeAdapter<*>? {
        return when (requestType) {
            RequestCode.PROVINCE, RequestCode.TEMP_PROVINCE, RequestCode.REPORT_PROVINCE -> MultiTypeAdapter.Builder()
                    .addViewType(ViewTypeData({ 0 }, { R.layout.item_place }, { Province::class.java },
                            { _, itemView -> ProvinceVH(itemView, onClickItem) }))
                    .build()
            RequestCode.DISTRICT, RequestCode.TEMP_DISTRICT, RequestCode.REPORT_DISTRICT -> MultiTypeAdapter.Builder()
                    .addViewType(ViewTypeData({ 0 }, { R.layout.item_place }, { District::class.java },
                            { _, itemView -> DistrictVH(itemView, onClickItem) }))
                    .build()
            RequestCode.WARD, RequestCode.TEMP_WARD, RequestCode.REPORT_WARD -> MultiTypeAdapter.Builder()
                    .addViewType(ViewTypeData({ 0 }, { R.layout.item_place }, { Ward::class.java },
                            { _, itemView -> WardVH(itemView, onClickItem) }))
                    .build()
            RequestCode.SECRET_QUESTION -> MultiTypeAdapter.Builder()
                    .addViewType(ViewTypeData({ 0 }, { R.layout.item_place }, { SecretQuestion::class.java },
                            { _, itemView -> SecretQuestionVH(itemView, onClickItem) }))
                    .build()
            else -> null
        }
    }

    private val onClickItem: (item: Parcelable) -> Unit = {
        if (targetFragment is HasFragmentResult) {
            val intent = Intent().apply {
                putExtras(Bundle().apply { putParcelable(EXTRA_DATA, it) })
            }
            (targetFragment as HasFragmentResult).onFragmentResult(targetRequestCode, HasFragmentResult.ACTION_OK, intent)
        }
        dismiss()
    }

    companion object {
        const val EXTRA_REQUEST_CODE = "EXTRA_REQUEST_CODE"
        const val EXTRA_REQUEST_TITLE = "EXTRA_REQUEST_TITLE"
        const val EXTRA_DATA = "EXTRA_DATA"

        fun newInstance(requestType: Int, requestTitle: String,
                        data: java.util.ArrayList<out Parcelable>?) = SelectPlaceDialog().apply {
            arguments = Bundle().apply {
                putInt(EXTRA_REQUEST_CODE, requestType)
                putString(EXTRA_REQUEST_TITLE, requestTitle)
                putParcelableArrayList(EXTRA_DATA, data)
            }
        }
    }
}