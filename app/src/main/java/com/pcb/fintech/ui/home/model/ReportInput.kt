package com.pcb.fintech.ui.home.model

import com.pcb.fintech.data.model.other.ReportStatus
import java.util.*

class ReportInput(val filterStatus: ReportStatus?,
                  val filterStartDate: Date?,
                  val filterEndDate: Date?,
                  val keywords: String? = null)