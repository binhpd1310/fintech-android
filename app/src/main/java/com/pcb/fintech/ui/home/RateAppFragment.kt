package com.pcb.fintech.ui.home

import android.view.View
import com.pcb.fintech.R
import com.pcb.fintech.di.ViewModelFactory
import com.pcb.fintech.ui.base.BaseMVVMFragment
import com.pcb.fintech.ui.views.OnClickAndHideKeyboard
import kotlinx.android.synthetic.main.fragment_rate_app.*

class RateAppFragment : BaseMVVMFragment<RateAppVM, ViewModelFactory>() {

    override fun getLayoutRes(): Int {
        return R.layout.fragment_rate_app
    }

    override fun getViewModelType(): Class<RateAppVM> {
        return RateAppVM::class.java
    }

    override fun setupViews(view: View) {
        toolbar.setNavigationOnClickListener { sendToNav { it.goBack() } }

        layout_rate_app.setOnClickListener(OnClickAndHideKeyboard { (viewModel.rateApp()) })

        layout_share_app.setOnClickListener(OnClickAndHideKeyboard { (viewModel.shareApp()) })
    }
}