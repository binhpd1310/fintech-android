package com.pcb.fintech.data.model.response

import com.google.gson.annotations.SerializedName
import com.pcb.fintech.data.model.other.Report

data class ReportsResponse(@SerializedName("pageNumber") val pageNumber: Int? = null,
                           @SerializedName("pageSize") val pageSize: Int? = null,
                           @SerializedName("totalNumberOfPages") val totalNumberOfPages: Int? = null,
                           @SerializedName("totalNumberOfRecords") val totalNumberOfRecords: Int? = null,
                           @SerializedName("results") val results: List<Report>? = null)