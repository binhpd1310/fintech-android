package com.pcb.fintech.language

import android.annotation.TargetApi
import android.content.Context
import android.content.res.Configuration
import android.os.Build
import com.pcb.fintech.data.source.local.PreferenceHelper
import java.util.*

object LocaleHelper {

    @JvmStatic
    fun onAttach(context: Context): Context {
        val prefHelper = PreferenceHelper(context)
        val language = prefHelper.getLanguage()
        val locale = Locale(language.code, language.country)
        Locale.setDefault(locale)
        return setLocale(context, locale)
    }

    @JvmStatic
    fun onAttach(context: Context, locale: Locale): Context {
        return setLocale(context, locale)
    }

    @JvmStatic
    fun updateConfiguration(context: Context, configuration: Configuration): Configuration {
        val prefHelper = PreferenceHelper(context)
        val language = prefHelper.getLanguage()
        val locale = Locale(language.code, language.country)
        Locale.setDefault(locale)
        val newContext = createNewContext(context, configuration)
        return newContext.resources.configuration
    }

    @JvmStatic
    fun getDefaultLocale(context: Context): Locale {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            context.resources.configuration.locales[0]
        } else {
            context.resources.configuration.locale
        }
    }

    @JvmStatic
    fun setLocale(context: Context, locale: Locale): Context {
        val configuration = context.resources.configuration
        configuration.setLocale(locale)
        configuration.setLayoutDirection(locale)
        return createNewContext(context, configuration)
    }

    @JvmStatic
    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    private fun createNewContext(context: Context, configuration: Configuration): Context {
        return context.createConfigurationContext(configuration)
    }

    @JvmStatic
    private fun updateCurrentContext(context: Context, configuration: Configuration): Context {
        val resources = context.resources
        resources.updateConfiguration(configuration, resources.displayMetrics)
        return context
    }
}