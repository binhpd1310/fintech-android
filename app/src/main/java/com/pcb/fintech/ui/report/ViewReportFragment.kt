package com.pcb.fintech.ui.report

import android.os.Bundle
import android.os.Handler
import android.view.View
import com.pcb.fintech.R
import com.pcb.fintech.data.model.other.Report
import com.pcb.fintech.di.ViewModelFactory
import com.pcb.fintech.ui.base.BaseMVVMFragment
import kotlinx.android.synthetic.main.fragment_pdf_viewer.*
import kotlinx.android.synthetic.main.fragment_report_create.rootView
import java.io.File

class ViewReportFragment : BaseMVVMFragment<ReportDetailVM, ViewModelFactory>() {

    private var reportFile: File? = null

    override fun getLayoutRes(): Int {
        return R.layout.fragment_pdf_viewer
    }

    override fun getViewModelType(): Class<ReportDetailVM> {
        return ReportDetailVM::class.java
    }

    override fun extractData(bundle: Bundle) {
        viewModel.setReport(bundle.getParcelable<Report>(EXTRA_REPORT_ID))
    }

    override fun setupViews(view: View) {
        toolbar.setNavigationOnClickListener { sendToNav { it.goBack() } }

        val report = viewModel.getReport()
        if (report == null) {
            showSnackBar(rootView, R.string.no_file_to_view)
            Handler().postDelayed({ sendToNav { it.goBack() } }, 2000)
            return
        }
        val reportId = report.id ?: return
        val reportKey = report.getKey()
        doObserveAll(viewModel.downloadReportCache(reportId, reportKey),
                onSuccess = { handleDownloadReport(it) },
                onError = { showSnackBar(rootView, R.string.error_common_title) })
    }

    private fun handleDownloadReport(response: File) {
        pdf_view.fromFile(response).load()
        reportFile = response
    }

    companion object {

        private const val EXTRA_REPORT_ID = "EXTRA_REPORT_ID"

        fun newInstance(report: Report?) = ViewReportFragment().apply {
            arguments = Bundle().apply { putParcelable(EXTRA_REPORT_ID, report) }
        }
    }
}