package com.pcb.fintech.data.source

import android.content.Context
import com.pcb.fintech.R
import com.pcb.fintech.data.model.ResponseResult
import com.pcb.fintech.data.model.body.*
import com.pcb.fintech.data.model.response.*
import com.pcb.fintech.data.source.local.PreferenceHelper
import com.pcb.fintech.data.source.remote.ApiService
import com.pcb.fintech.data.source.remote.config.TokenType
import com.pcb.fintech.utils.Constant.DEFAULT_TERM_LINK_EN
import com.pcb.fintech.utils.Constant.DEFAULT_TERM_LINK_VI
import com.pcb.fintech.utils.LinkType
import com.pcb.fintech.utils.getLocalLink
import okhttp3.MultipartBody
import okhttp3.RequestBody
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class CommonDataSource @Inject constructor(
        private val context: Context,
        private val apiService: ApiService,
        private val prefHelper: PreferenceHelper) : BaseDataSource() {

    /**
     * Account
     */

    suspend fun verifyRegisterProfile(profile: ProfileBody) =
            getResult(call = { apiService.verifyRegisterProfile((profile)) },
                    transform = { ProfileVerificationResult.fromValue(it) })

    suspend fun createOtp(phoneNumber: String) =
            getResult(call = { apiService.createOtp(phoneNumber) },
                    transform = { it })

    suspend fun verifyOtp(verifyOtpBody: VerifyOtpBody) =
            getResult(call = { apiService.verifyOtp(verifyOtpBody) },
                    transform = { it })

    suspend fun uploadImage(phone: RequestBody, imageType: RequestBody,
                            imageFile: MultipartBody.Part) =
            getResult(call = { apiService.uploadImage(phone, imageType, imageFile) },
                    transform = { it })

    suspend fun verifyFace(sourceFaceId1: String, imagePath1: String, sourceFaceId2: String, imagePath2: String) =
            getResult(call = { apiService.verifyFace(VerifyFaceBody(sourceFaceId1, imagePath1, sourceFaceId2, imagePath2)) },
                    transform = { it })

    suspend fun extractCardInfo(body: ExtractCardBody) =
            getResult(call = { apiService.extractCardInfo(body) },
                    transform = { it })

    suspend fun registerCardInfo(account: RegisterAccountBody) =
            getResult(call = { apiService.registerAccount(account) },
                    transform = { it })

    suspend fun login(userName: String, password: String, rememberMe: Boolean) =
            getResult(call = { apiService.login(LoginBody(userName, password, rememberMe)) },
                    transform = {
                        val expiredTime = System.currentTimeMillis() + (it.expiresIn ?: 0) * 1000
                        prefHelper.saveAccessToken(it.token)
                        prefHelper.saveTokenType(TokenType.BEARER.value)
                        prefHelper.saveExpiredTime(expiredTime)
                        it
                    })

    suspend fun forgotPassword(emailBody: EmailBody) =
            getResult(call = { apiService.forgotPassword(emailBody) },
                    transform = { it },
                    errorMsgExtractor = {
                        if (it.code() == 400) context.getString(R.string.error_forgot_pw_not_found)
                        else it.message()
                    })

    suspend fun getProvinces(): ResponseResult<List<Province>> {
        val provinces = prefHelper.getProvinces()
        return if (provinces.isNullOrEmpty()) {
            getResult(call = { apiService.getProvinces() },
                    transform = {
                        prefHelper.saveProvinces(it)
                        it
                    })
        } else {
            ResponseResult.Success(provinces)
        }
    }

    suspend fun getDistricts(): ResponseResult<List<District>> {
        val districts = prefHelper.getDistricts()
        return if (districts.isNullOrEmpty()) {
            getResult(call = { apiService.getDistricts() },
                    transform = {
                        prefHelper.saveDistricts(it)
                        it
                    })
        } else {
            ResponseResult.Success(districts)
        }
    }

    suspend fun getWards(): ResponseResult<List<Ward>> {
        val wards = prefHelper.getWards()
        return if (wards.isNullOrEmpty()) {
            getResult(call = { apiService.getWards() },
                    transform = {
                        prefHelper.saveWards(it)
                        it
                    })
        } else {
            ResponseResult.Success(wards)
        }
    }

    suspend fun logout() =
            getResult(call = { apiService.logout() }, transform = { it })

    /**
     * Notification
     */
    suspend fun getAllNotifications() =
            getResult(call = { apiService.getAllNotifications() },
                    transform = {
                        val newList = it.sortedByDescending { item -> item.getCreatedDate() }
                        newList
                    })

    suspend fun getNotifications(searchNotiBody: SearchNotiBody) =
            getResult(call = { apiService.getNotifications(searchNotiBody) },
                    transform = { it })

    suspend fun getNotiById(notiId: Int) =
            getResult(call = { apiService.getNotiById(notiId) },
                    transform = { it })

    suspend fun deleteOneNoti(notiId: Int) =
            getResult(call = { apiService.deleteOneNoti(notiId) },
                    transform = { it })

    suspend fun deleteAllNoti() =
            getResult(call = { apiService.deleteAllNoti() },
                    transform = { it })

    /**
     * Others
     */

    suspend fun getSlides() =
            getResult(call = { apiService.getSlides() },
                    transform = { it.filter { item -> item.isActive == true } })

    suspend fun getTermLink() =
            getResult(call = { apiService.getLinks() },
                    onErrorReturn = { listOf() },
                    transform = {
                        val foundLink = it.find { item -> item.linkCode == LinkType.TERM.value }
                        val defaultLink = WebLink(linkEn = DEFAULT_TERM_LINK_EN, linkVi = DEFAULT_TERM_LINK_VI,
                                titleEn = context.getString(R.string.title_term), titleVi = context.getString(R.string.title_term))
                        getLocalLink(prefHelper, foundLink, defaultLink)
                    })

    suspend fun getServiceLinks(): ResponseResult<List<WebLink>> =
            getResult(call = { apiService.getLinks() }, transform = { it })

    suspend fun createMobileOtp(phone: String) =
            getResult(call = { apiService.createMobileOtp(phone) },
                    transform = { it })

    suspend fun verifyMobileOtp(verifyOtpBody: VerifyOtpBody) =
            getResult(call = { apiService.verifyMobileOtp(verifyOtpBody) },
                    transform = { it })

    /**
     * Newsletter
     */

    suspend fun getNewsletter(customerId: Int) =
            getResult(call = { apiService.getNewsletter(customerId) },
                    transform = { it })

    suspend fun createNewsletter(body: CreateNewsletter) =
            getResult(call = { apiService.createNewsletter(body) },
                    transform = { it })

    suspend fun updateNewsletter(body: UpdateNewsletter) =
            getResult(call = { apiService.updateNewsletter(body) },
                    transform = { it })
}