package com.pcb.fintech.data.model

data class Consumer3<T1, T2, T3>(val data1: T1, val data2: T2, val data3: T3)