package com.pcb.fintech.data.source.remote.config

interface ApiParams {
    fun getAuthorization(): String
    fun getAccessToken(): String
    fun getContentType(): String
    fun getCacheControl(): String
    fun getAPIKey(): String
}