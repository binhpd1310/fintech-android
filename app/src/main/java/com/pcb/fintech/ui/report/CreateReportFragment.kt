package com.pcb.fintech.ui.report

import android.view.View
import com.pcb.fintech.R
import com.pcb.fintech.data.model.body.ReportBody
import com.pcb.fintech.di.ViewModelFactory
import com.pcb.fintech.eventbus.EventBus
import com.pcb.fintech.eventbus.event.CreatedReportEvent
import com.pcb.fintech.ui.base.BaseMVVMFragment
import com.pcb.fintech.ui.base.OpenActivity
import com.pcb.fintech.ui.profile.SettingAddressFragment
import com.pcb.fintech.ui.views.getTrimText
import com.pcb.fintech.utils.AndroidUtil
import kotlinx.android.synthetic.main.fragment_report_create.*

class CreateReportFragment : BaseMVVMFragment<CreateReportVM, ViewModelFactory>() {

    override fun getLayoutRes(): Int {
        return R.layout.fragment_report_create
    }

    override fun getViewModelType(): Class<CreateReportVM> {
        return CreateReportVM::class.java
    }

    override fun setupViews(view: View) {
        toolbar.setNavigationOnClickListener { sendToNav { it.goBack() } }

        btn_create.setOnClickListener {
            AndroidUtil.hideKeyboard(it)
            createReport()
        }
    }

    private fun createReport() {
        val reportBody = ReportBody(edt_note.getTrimText())
        doObserveAll(viewModel.createReport(reportBody),
                onSuccess = { handleCreateReportSuccessful(it) })
    }

    private fun handleCreateReportSuccessful(responseCode: Int) {
        showSnackBar(rootView, R.string.create_request_successful)
        EventBus.getInstance().notify(CreatedReportEvent())
        if (responseCode > 0) {
            val bundle = PaymentMainFragment.buildBundle(responseCode)
            val intent = OpenActivity.getIntent(context, PaymentMainFragment::class.java, bundle)
            startActivity(intent)
        }
        sendToNav { it.goBack() }
    }
}