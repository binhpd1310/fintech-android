package com.pcb.fintech.di.module

import com.pcb.fintech.ui.home.*
import com.pcb.fintech.ui.profile.*
import com.pcb.fintech.ui.registration.*
import com.pcb.fintech.ui.report.*
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentBuilderModule {

    @ContributesAndroidInjector
    abstract fun contributeRegistrationFragment(): RegistrationFragment

    @ContributesAndroidInjector
    abstract fun contributeRegistrationFragmentStep1(): RegistrationFragmentStep1

    @ContributesAndroidInjector
    abstract fun contributeRegistrationFragmentStep2(): RegistrationFragmentStep2

    @ContributesAndroidInjector
    abstract fun contributeRegistrationFragmentStep3(): RegistrationFragmentStep3

    @ContributesAndroidInjector
    abstract fun contributeLoginFragment(): LoginFragment

    @ContributesAndroidInjector
    abstract fun contributeRequestFragment(): ReportMainFragment

    @ContributesAndroidInjector
    abstract fun contributeNotificationFragment(): NotificationFragment

    @ContributesAndroidInjector
    abstract fun contributeSettingInfoFragment(): SettingMenuFragment

    @ContributesAndroidInjector
    abstract fun contributeSettingUserFragment(): SettingUserFragment

    @ContributesAndroidInjector
    abstract fun contributeSettingScretQuestionFragment(): SettingSecretQuestionFragment

    @ContributesAndroidInjector
    abstract fun contributeSettingPasswordFragment(): SettingPasswordFragment

    @ContributesAndroidInjector
    abstract fun contributeSettingAddressFragment(): SettingAddressFragment

    @ContributesAndroidInjector
    abstract fun contributeSettingProfileFragment(): SettingProfileFragment

    @ContributesAndroidInjector
    abstract fun contributePaymentFragment(): PaymentMainFragment

    @ContributesAndroidInjector
    abstract fun contributeCreateReportFragment(): CreateReportFragment

    @ContributesAndroidInjector
    abstract fun contributeReportDetailFragment(): ReportDetailFragment

    @ContributesAndroidInjector
    abstract fun contributeForgotPasswordFragment(): ForgotPasswordFragment

    @ContributesAndroidInjector
    abstract fun contributePdfViewerFragment(): ViewReportFragment

    @ContributesAndroidInjector
    abstract fun contributeNotiDetailFragment(): NotiDetailFragment

    @ContributesAndroidInjector
    abstract fun contributePaymentResultFragment(): PaymentResultFragment

    @ContributesAndroidInjector
    abstract fun contributePaymentViewFragment(): PaymentViewFragment

    @ContributesAndroidInjector
    abstract fun contributeHomeFragmentRequestLogin(): HomeFragmentV2

    @ContributesAndroidInjector
    abstract fun contributeLeftMenuLoginedFragment(): MenuAfterLoginFragment

    @ContributesAndroidInjector
    abstract fun contributeLeftMenuNotLoginFragment(): MenuBeforeLoginFragment

    @ContributesAndroidInjector
    abstract fun contributeMainFragment(): MainFragment

    @ContributesAndroidInjector
    abstract fun contributeVerifyOtpFragment(): VerifyOtpFragment

    @ContributesAndroidInjector
    abstract fun contributeTermFragment(): TermFragment

    @ContributesAndroidInjector
    abstract fun contributeSupportFragment(): SupportFragment

    @ContributesAndroidInjector
    abstract fun contributeRegisterNotiFragment(): RegisterNotiFragment

    @ContributesAndroidInjector
    abstract fun contributeRateAppFragment(): RateAppFragment
}