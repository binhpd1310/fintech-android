package com.pcb.fintech.data.model.response

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import com.pcb.fintech.data.model.response.District
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Province constructor(
        @SerializedName("id") val id: Int? = null,
        @SerializedName("name") val name: String? = null,
        @SerializedName("displayOrder") val displayOrder: Int? = null,
        @SerializedName("type") val type: String? = null,
        @SerializedName("countryId") val countryId: Int? = null,
        @SerializedName("country") val country: String? = null,
        @SerializedName("districts") val districts: List<District>? = null) : Parcelable {

    var selected: Boolean = false
}