package com.pcb.fintech.ui.home.menuitem

import android.content.Context
import android.content.Intent
import com.pcb.fintech.eventbus.EventBus
import com.pcb.fintech.eventbus.event.CloseLeftMenuEvent
import com.pcb.fintech.eventbus.event.OpenHomeTabEvent
import com.pcb.fintech.ui.base.OpenActivity
import com.pcb.fintech.ui.home.RateAppFragment
import com.pcb.fintech.ui.home.RegisterNotiFragment
import com.pcb.fintech.ui.registration.LoginFragment
import com.pcb.fintech.ui.registration.RegistrationFragment

class MenuClickBeforeLoginHandler(private val context: Context,
                                  private val sendToAct: (intent: Intent?) -> Unit,
                                  private val changeLanguage: () -> Unit) : HomeMenuClickHandler {

    override fun clickFeature(featureId: FeatureId) {
        when (featureId) {
            FeatureId.FT_REPORT_ID -> goToLogin()
            FeatureId.FT_LANGUAGE_ID -> changeLanguage()
            FeatureId.FT_SUPPORT_ID -> goToSupport()
            FeatureId.FT_REGISTER_NOTIFICATION_ID -> goToRegisterNoti()
            FeatureId.FT_RATE_ID -> goToRateApp()
            FeatureId.FT_LOGIN -> goToLogin()
            FeatureId.FT_REGISTER -> goToRegistration()
            else -> {
            }
        }
        EventBus.getInstance().notify(CloseLeftMenuEvent())
    }

    private fun goToLogin() {
        sendToAct(OpenActivity.getIntent(context, LoginFragment::class.java))
    }

    private fun goToRegistration() {
        sendToAct(OpenActivity.getIntent(context, RegistrationFragment::class.java))
    }

    private fun goToSupport() {
        EventBus.getInstance().notify(OpenHomeTabEvent(3))
    }

    private fun goToRegisterNoti() {
        sendToAct(OpenActivity.getIntent(context, RegisterNotiFragment::class.java))
    }

    private fun goToRateApp() {
        sendToAct(OpenActivity.getIntent(context, RateAppFragment::class.java))
    }
}