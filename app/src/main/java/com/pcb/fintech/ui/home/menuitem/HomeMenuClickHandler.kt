package com.pcb.fintech.ui.home.menuitem

interface HomeMenuClickHandler {

    fun clickFeature(featureId: FeatureId)

}