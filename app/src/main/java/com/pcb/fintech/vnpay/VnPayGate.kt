package com.pcb.fintech.vnpay

import android.content.Intent
import com.pcb.fintech.data.model.response.PaymentOnlineResponse
import com.pcb.fintech.utils.LogUtil

object VnPayGate {
    private const val KEY_URL = "url"
    private const val KEY_TMN_CODE = "tmn_code"
    private const val KEY_SCHEME = "scheme"
    private const val VALUE_TMN_CODE = "TDUNGVN1"
    private const val VALUE_SCHEME = "TDUNGVN1"

    private const val KEY_VNPAY_URL = VnPayParams.vnPayUrl

    private val attributeMap = mutableMapOf(
            VnPayParams.vnpAmount to VnPayParams.vnp_Amount,
            VnPayParams.vnpCommand to VnPayParams.vnp_Command,
            VnPayParams.vnpCreateDate to VnPayParams.vnp_CreateDate,
            VnPayParams.vnpCurrCode to VnPayParams.vnp_CurrCode,
            VnPayParams.vnpIpAddr to VnPayParams.vnp_IpAddr,
            VnPayParams.vnpLocale to VnPayParams.vnp_Locale,
            VnPayParams.vnpOrderInfo to VnPayParams.vnp_OrderInfo,
            VnPayParams.vnpOrderType to VnPayParams.vnp_OrderType,
            VnPayParams.returnUrl to VnPayParams.vnp_ReturnUrl,
            VnPayParams.vnpTmnCode to VnPayParams.vnp_TmnCode,
            VnPayParams.vnpTxnRef to VnPayParams.vnp_TxnRef,
            VnPayParams.vnpVersion to VnPayParams.vnp_Version,
            VnPayParams.vnpSecureHash to VnPayParams.vnp_SecureHash
    )

    fun buildUrl(response: PaymentOnlineResponse): String {
        val urlBuilder = StringBuilder()

        val domain = response.vnPayUrl
        urlBuilder.append(domain)
        urlBuilder.append("?")

        appendParam(urlBuilder, VnPayParams.vnp_Amount, response.vnpAmount.toLong())
        appendParam(urlBuilder, VnPayParams.vnp_Command, response.vnpCommand)
        appendParam(urlBuilder, VnPayParams.vnp_CreateDate, response.vnpCreateDate)
        appendParam(urlBuilder, VnPayParams.vnp_CurrCode, response.vnpCurrCode)
        appendParam(urlBuilder, VnPayParams.vnp_IpAddr, response.vnpIpAddr)
        appendParam(urlBuilder, VnPayParams.vnp_Locale, response.vnpLocale)
        appendParam(urlBuilder, VnPayParams.vnp_OrderInfo, response.vnpOrderInfo)
        appendParam(urlBuilder, VnPayParams.vnp_OrderType, response.vnpOrderType)
        appendParam(urlBuilder, VnPayParams.vnp_ReturnUrl, response.returnUrl)
        appendParam(urlBuilder, VnPayParams.vnp_TmnCode, response.vnpTmnCode)
        appendParam(urlBuilder, VnPayParams.vnp_TxnRef, response.vnpTxnRef)
        appendParam(urlBuilder, VnPayParams.vnp_Version, response.vnpVersion)
        appendParam(urlBuilder, VnPayParams.vnp_SecureHash, response.vnpSecureHash, true)

        return urlBuilder.toString()
    }

    private fun appendParam(builder: StringBuilder, paramName: String, paramValue: String, end: Boolean = false) {
        builder.append(paramName)
        builder.append("=")
        builder.append(paramValue)
        if (!end) builder.append("&")
    }

    private fun appendParam(builder: StringBuilder, paramName: String, paramValue: Long, end: Boolean = false) {
        builder.append(paramName)
        builder.append("=")
        builder.append(paramValue)
        if (!end) builder.append("&")
    }

    fun buildVnResponse(intent: Intent?): VnPayResponse? {
        intent ?: return null
        val response = VnPayResponse()
        try {
            val amount = intent.getStringExtra(VnPayParams.vnp_Amount)
            response.vnp_Amount = amount?.toDouble() ?: 0.0
        } catch (e: NumberFormatException) {
            LogUtil.error(e)
        }
        try {
            val payDate = intent.getStringExtra(VnPayParams.vnp_PayDate)
            response.vnp_PayDate = payDate?.toLong() ?: 0
        } catch (e: NumberFormatException) {
            LogUtil.error(e)
        }
        try {
            val txnRef = intent.getStringExtra(VnPayParams.vnp_TxnRef)
            response.vnp_TxnRef = txnRef?.toLong() ?: 0
        } catch (e: NumberFormatException) {
            LogUtil.error(e)
        }
        response.vnp_BankCode = intent.getStringExtra(VnPayParams.vnp_BankCode) ?: ""
        response.vnp_BankTranNo = intent.getStringExtra(VnPayParams.vnp_BankTranNo) ?: ""
        response.vnp_CardType = intent.getStringExtra(VnPayParams.vnp_CardType) ?: ""
        response.vnp_OrderInfo = intent.getStringExtra(VnPayParams.vnp_OrderInfo) ?: ""
        response.vnp_ResponseCode = intent.getStringExtra(VnPayParams.vnp_ResponseCode) ?: ""
        response.vnp_TmnCode = intent.getStringExtra(VnPayParams.vnp_TmnCode) ?: ""
        response.vnp_TransactionNo = intent.getStringExtra(VnPayParams.vnp_TransactionNo) ?: ""
        response.vnp_SecureHashType =
                intent.getStringExtra(VnPayParams.vnp_SecureHashType) ?: ""
        response.vnp_SecureHash = intent.getStringExtra(VnPayParams.vnp_SecureHash) ?: ""
        return response
    }
}