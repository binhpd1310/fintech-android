package com.pcb.fintech.ui.home

import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.pcb.fintech.data.model.body.CreateNewsletter
import com.pcb.fintech.data.model.body.UpdateNewsletter
import com.pcb.fintech.data.model.response.RegisterNewsletter
import com.pcb.fintech.data.source.CommonDataSource
import com.pcb.fintech.data.source.local.PreferenceHelper
import com.pcb.fintech.ui.base.BaseViewModel
import com.pcb.fintech.utils.SchedulerProvider

class RegisterNotiVM(application: Application, schedulerProvider: SchedulerProvider,
                     private val prefHelper: PreferenceHelper,
                     private val commonDataSource: CommonDataSource
) : BaseViewModel(application, schedulerProvider) {

    internal val newsletterLD = MutableLiveData<RegisterNewsletter>()

    init {
        getNewsletter()
    }

    fun getUserId() = prefHelper.getUserId() ?: -1

    private fun getNewsletter() {
        callApi({ commonDataSource.getNewsletter(getUserId()) }, onSuccess = {
            newsletterLD.postValue(it)
        })
    }

    fun createNewsletterLD(body: CreateNewsletter) = resultLiveData {
        commonDataSource.createNewsletter(body)
    }

    fun updateNewsletter(body: UpdateNewsletter) = resultLiveData {
        commonDataSource.updateNewsletter(body)
    }
}