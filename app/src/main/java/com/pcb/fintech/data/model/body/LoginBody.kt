package com.pcb.fintech.data.model.body

import com.google.gson.annotations.SerializedName

class LoginBody constructor(@SerializedName("username") val userName: String,
                            @SerializedName("password") val password: String,
                            @SerializedName("rememberMe") val rememberMe: Boolean)