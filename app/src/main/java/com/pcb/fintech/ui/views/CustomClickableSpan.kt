package com.pcb.fintech.ui.views

import android.graphics.Typeface
import android.text.TextPaint
import android.text.style.ClickableSpan
import android.view.View

/**
 * Custom a clickable span
 * @property onClickSpan A function which is called when click this text span
 * @property typeface Set new typeface to this text span
 * @property color Set new color to this text span
 * @property isUnderLine Set underline to this text span
 */
class CustomClickableSpan(private val onClickSpan: (() -> Unit)? = null,
                          private val typeface: Typeface? = null,
                          private val color: Int? = null,
                          private val isUnderLine: Boolean? = null) : ClickableSpan() {

    override fun onClick(widget: View) {
        onClickSpan?.invoke()
    }

    override fun updateDrawState(tp: TextPaint) {
        super.updateDrawState(tp)
        tp.typeface = typeface ?: tp.typeface
        tp.color = color ?: tp.color
        tp.isUnderlineText = isUnderLine ?: tp.isUnderlineText
    }
}