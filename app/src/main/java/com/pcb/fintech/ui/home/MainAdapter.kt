package com.pcb.fintech.ui.home

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter

class MainAdapter(activity: FragmentActivity,
                  private val getSize: () -> Int,
                  private val buildFragment: (pos: Int) -> Fragment) : FragmentStateAdapter(activity) {

    override fun getItemCount(): Int {
        return getSize()
    }

    override fun createFragment(position: Int): Fragment {
        return buildFragment(position)
    }
}