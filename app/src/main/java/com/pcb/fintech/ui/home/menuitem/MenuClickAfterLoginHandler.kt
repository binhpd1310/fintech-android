package com.pcb.fintech.ui.home.menuitem

import android.content.Context
import android.content.Intent
import com.pcb.fintech.eventbus.EventBus
import com.pcb.fintech.eventbus.event.CloseLeftMenuEvent
import com.pcb.fintech.eventbus.event.OpenHomeTabEvent
import com.pcb.fintech.ui.base.OpenActivity
import com.pcb.fintech.ui.home.RateAppFragment
import com.pcb.fintech.ui.home.RegisterNotiFragment
import com.pcb.fintech.ui.profile.SettingMenuFragment

class MenuClickAfterLoginHandler(private val context: Context,
                                 private val logout: () -> Unit,
                                 private val sendToAct: (intent: Intent?) -> Unit,
                                 private val changeLanguage: () -> Unit) : HomeMenuClickHandler {

    override fun clickFeature(featureId: FeatureId) {
        when (featureId) {
            FeatureId.FT_PROFILE -> goToProfile()
            FeatureId.FT_REPORT_ID -> goToReports()
            FeatureId.FT_SETUP_ID -> goToProfile()
            FeatureId.FT_LANGUAGE_ID -> changeLanguage()
            FeatureId.FT_NOTIFICATION_ID -> goToNotifications()
            FeatureId.FT_SUPPORT_ID -> goToSupport()
            FeatureId.FT_REGISTER_NOTIFICATION_ID -> goToRegisterNoti()
            FeatureId.FT_RATE_ID -> goToRateApp()
            FeatureId.FT_LOGOUT_ID -> logout()
            else -> {
            }
        }
        EventBus.getInstance().notify(CloseLeftMenuEvent())
    }

    private fun goToProfile() {
        sendToAct(OpenActivity.getIntent(context, SettingMenuFragment::class.java))
    }

    private fun goToReports() {
        EventBus.getInstance().notify(OpenHomeTabEvent(1))
    }

    private fun goToNotifications() {
        EventBus.getInstance().notify(OpenHomeTabEvent(2))
    }

    private fun goToSupport() {
        EventBus.getInstance().notify(OpenHomeTabEvent(4))
    }

    private fun goToRegisterNoti() {
        sendToAct(OpenActivity.getIntent(context, RegisterNotiFragment::class.java))
    }

    private fun goToRateApp() {
        sendToAct(OpenActivity.getIntent(context, RateAppFragment::class.java))
    }

}