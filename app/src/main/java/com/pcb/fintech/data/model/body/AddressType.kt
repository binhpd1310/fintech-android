package com.pcb.fintech.data.model.body

enum class AddressType(val value: Int) {
    PERMANENT(1),
    TEMPORARY(2),
    REPORT(3);

    companion object {
        private val valueMap by lazy {
            return@lazy mutableMapOf<Int, AddressType>().apply {
                for (item in values()) {
                    put(item.value, item)
                }
            }
        }

        fun fromValue(value: Int?): AddressType? {
            return valueMap[value]
        }
    }
}