package com.pcb.fintech.data.model.response

import com.google.gson.annotations.SerializedName
import com.pcb.fintech.utils.formatDoubleToLong

data class PaymentReportResponse constructor(
        @SerializedName("reportId") val reportId: Int? = null,
        @SerializedName("userId") val userId: Int? = null,
        @SerializedName("reportFee") val reportFee: Double? = null,
        @SerializedName("paymentFee") val paymentFee: Double? = null,
        @SerializedName("paymentMethodId") val paymentMethodId: Int? = null,
        @SerializedName("shippingMethod") val shippingMethod: Int? = null,
        @SerializedName("shippingFee") val shippingFee: Double? = null,
        @SerializedName("shippingAddressId") val shippingAddressId: Int? = null,
        @SerializedName("promotionCode") val promotionCode: String? = null,
        @SerializedName("taxAmount") val taxAmount: Double? = null,
        @SerializedName("total") val total: Double? = null) {

    fun getReportFee() = reportFee.formatDoubleToLong()
    fun getPaymentFee() = paymentFee.formatDoubleToLong()
    fun getShippingFee() = shippingFee.formatDoubleToLong()
    fun getTaxAmount() = taxAmount.formatDoubleToLong()
    fun getTotal() = total.formatDoubleToLong()
}