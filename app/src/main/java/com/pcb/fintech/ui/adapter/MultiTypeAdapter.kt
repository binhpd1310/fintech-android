package com.pcb.fintech.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncDifferConfig
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView

class MultiTypeAdapter<O : Any> private constructor(
    updateCallback: UpdateCallback,
    diffCallback: DiffUtil.ItemCallback<O>,
    private val viewTypeMap: Map<Int, ViewTypeData<O, *, out MultiTypeVH<Any>>>
) : RecyclerView.Adapter<MultiTypeVH<Any>>() {

    private val mHelper: AsyncListDiffer<O> =
        AsyncListDiffer<O>(
            SimpleListUpdateCallback(this, updateCallback),
            AsyncDifferConfig.Builder(diffCallback).build()
        )

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MultiTypeVH<Any> {
        val viewTypeData = viewTypeMap[viewType]
            ?: throw IllegalArgumentException("View type mock is not found!")
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(viewTypeData.layoutRes.invoke(), parent, false)
            ?: throw IllegalArgumentException("View type mock is not found!")
        return viewTypeData.createViewHolder(parent, view)
    }

    override fun onBindViewHolder(holder: MultiTypeVH<Any>, position: Int) {
        val item = getItem(position)
        holder.bindView(item)
    }

    override fun getItemViewType(position: Int): Int {
        val item = getItem(position)

        for ((key, viewTypeData) in viewTypeMap) {
            val itemClazz = viewTypeData.itemClazz.invoke()
            val isViewType = itemClazz.isInstance(item) && key == viewTypeData.viewType.invoke()
            if (isViewType) return key
        }

        throw IllegalArgumentException("View type is not compatible!")
    }

    override fun getItemCount(): Int {
        return mHelper.currentList.size
    }

    fun getItem(index: Int): O {
        return mHelper.currentList[index]
    }

    fun submitList(list: List<O>?) {
        mHelper.submitList(list)
    }

    class Builder {

        private var updateCallback: UpdateCallback = UpdateCallback()

        private var diffCallback: DiffUtil.ItemCallback<Any> = SimpleDiffCallback()

        private val viewTypeMap = hashMapOf<Int, ViewTypeData<*, *, *>>()

        fun withCallback(updateCallback: UpdateCallback): Builder {
            this.updateCallback = updateCallback
            return this
        }

        fun withCallback(diffCallback: DiffUtil.ItemCallback<Any>): Builder {
            this.diffCallback = diffCallback
            return this
        }

        fun addViewType(viewTypeData: ViewTypeData<*, *, *>): Builder {
            this.viewTypeMap[viewTypeData.viewType.invoke()] = viewTypeData
            return this
        }

        fun build(): MultiTypeAdapter<Any> {
            return MultiTypeAdapter(
                updateCallback, diffCallback,
                viewTypeMap as Map<Int, ViewTypeData<Any, *, out MultiTypeVH<Any>>>
            )
        }
    }
}