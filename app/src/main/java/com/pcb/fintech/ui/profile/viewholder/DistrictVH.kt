package com.pcb.fintech.ui.profile.viewholder

import android.view.View
import com.pcb.fintech.data.model.response.District
import kotlinx.android.synthetic.main.item_place.*
import com.pcb.fintech.ui.adapter.MultiTypeVH

class DistrictVH(private val itemV: View,
                 onClickListener: (item: District) -> Unit) : MultiTypeVH<District>(itemV) {

    init {
        itemV.setOnClickListener { onClickListener.invoke(itemV.tag as District) }
    }

    override val containerView: View?
        get() = itemV

    override fun bindView(item: District) {
        itemV.tag = item
        tv_place.text = item.name
        iv_select.visibility = if (item.selected) View.VISIBLE else View.GONE
    }

}