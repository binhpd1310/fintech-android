package com.pcb.fintech.ui.home

import android.content.Intent
import android.os.Handler
import android.view.View
import android.view.animation.AnimationUtils
import androidx.core.widget.doAfterTextChanged
import androidx.recyclerview.widget.LinearLayoutManager
import com.pcb.fintech.R
import com.pcb.fintech.data.model.other.Report
import com.pcb.fintech.data.model.other.ReportStatus
import com.pcb.fintech.di.ViewModelFactory
import com.pcb.fintech.ui.adapter.MultiTypeAdapter
import com.pcb.fintech.ui.adapter.ViewTypeData
import com.pcb.fintech.ui.base.BaseMVVMFragment
import com.pcb.fintech.ui.base.HasFragmentResult
import com.pcb.fintech.ui.base.OpenActivity
import com.pcb.fintech.ui.home.SearchReportDialog.Companion.EXTRA_END_DATE
import com.pcb.fintech.ui.home.SearchReportDialog.Companion.EXTRA_SEARCH_TYPE
import com.pcb.fintech.ui.home.SearchReportDialog.Companion.EXTRA_START_DATE
import com.pcb.fintech.ui.home.SearchReportDialog.Companion.EXTRA_TYPE_STATUS
import com.pcb.fintech.ui.home.SearchReportDialog.Companion.TYPE_TIME_ALL
import com.pcb.fintech.ui.home.SearchReportDialog.Companion.TYPE_TIME_RANGE
import com.pcb.fintech.ui.home.model.ReportInput
import com.pcb.fintech.ui.home.viewholder.ReportVH
import com.pcb.fintech.ui.profile.SettingAddressFragment
import com.pcb.fintech.ui.report.CreateReportFragment
import com.pcb.fintech.ui.report.ReportDetailFragment
import com.pcb.fintech.ui.report.VerifyOtpFragment
import com.pcb.fintech.ui.views.*
import com.pcb.fintech.utils.AndroidUtil
import kotlinx.android.synthetic.main.fragment_report_main.*
import java.util.*

class ReportMainFragment : BaseMVVMFragment<HomeVM, ViewModelFactory>() {

    private lateinit var mScrollListener: EndlessRecyclerViewScrollListener
    private lateinit var mAdapter: MultiTypeAdapter<Any>

    private var mFilterType = TYPE_TIME_ALL
    private var mFilterStatus: ReportStatus? = null
    private var mFilterStartDate: Date? = null
    private var mFilterEndDate: Date? = null

    override fun getLayoutRes(): Int {
        return R.layout.fragment_report_main
    }

    override fun getViewModelType(): Class<HomeVM> {
        return HomeVM::class.java
    }

    override fun viewModelInActivityScope(): Boolean = true

    override fun setupViews(view: View) {
        iv_back.setOnClickListener { handleBack() }

        layout_bg.setOnClickListener {
            hideActions()
        }

        refresh_layout.setOnRefreshListener {
            refreshSearchReport()
        }

        btn_filter.setOnClickListener {
            hideActions()
            Handler().postDelayed({ showSearchDialog() }, 100)
        }

        btn_create.setOnClickListener {
            hideActions()
            Handler().postDelayed({ viewModel.createReport() }, 100)
        }

        btn_add_report.setOnClickListener {
            showOrHideActions()
        }

        iv_filter.setOnClickListener {
            showSearchDialog()
        }

        edt_search.doAfterTextChanged {
            sendSearchRequest()
        }

        if (viewModel.isLogin()) {
            btn_add_report.visible()
        } else {
            btn_add_report.gone()
        }

        val layoutManager = LinearLayoutManager(context)
        val decoration = SpacesItemDecoration(AndroidUtil.dpToPx(12), LinearLayoutManager.VERTICAL)
        mScrollListener = SimpleEndlessScrollListener(layoutManager) { viewModel.getNextReports() }
        mAdapter = MultiTypeAdapter.Builder()
                .addViewType(ViewTypeData({ 0 }, { R.layout.item_report },
                        { Report::class.java }, { _, itemView -> ReportVH(context!!, itemView, onClickItem) }))
                .build()
        rv_data.adapter = mAdapter
        rv_data.layoutManager = layoutManager
        rv_data.addItemDecoration(decoration)
        rv_data.addOnScrollListener(mScrollListener)

        showTotalReport(0)

        delayOnMain({ refreshSearchReport() })
    }

    private val onClickItem: (pos: Int) -> Unit = func@{
        val item = viewModel.getReports()?.getOrNull(it) ?: return@func
        val data = ReportDetailFragment.getBundle(item)
        val intent = OpenActivity.getIntent(context, ReportDetailFragment::class.java, data)
        startActivity(intent)
    }

    private fun showOrHideActions() {
        if (layout_quick_action.visibility == View.GONE) {
            showActions()
        } else {
            hideActions()
        }
    }

    private fun showActions() {
        if (layout_quick_action.visibility == View.VISIBLE) return
        layout_quick_action.visibility = View.VISIBLE
        layout_bg.visibility = View.VISIBLE
        layout_quick_action.startAnimation(AnimationUtils.loadAnimation(context, R.anim.anim_actions_show))
        btn_add_report.rotation = 45f
    }

    private fun hideActions() {
        if (layout_quick_action.visibility == View.GONE) return
        layout_quick_action.visibility = View.GONE
        layout_bg.visibility = View.GONE
        layout_quick_action.startAnimation(AnimationUtils.loadAnimation(context, R.anim.anim_actions_hide))
        btn_add_report.rotation = 0f
    }

    private fun showSearchDialog() {
        val dialog =
                SearchReportDialog.newInstance(mFilterType, mFilterStatus, mFilterStartDate, mFilterEndDate)
        dialog.setTargetFragment(this, CODE_SEARCH)
        dialog.show(parentFragmentManager, null)
    }

    private fun beforeCreateReport(needToVerifyOtp: Boolean) {
        if (needToVerifyOtp) {
            val intent = OpenActivity.getIntent(context, VerifyOtpFragment::class.java)
            startActivity(intent)
        } else {
            val intent = OpenActivity.getIntent(context, CreateReportFragment::class.java)
            startActivity(intent)
        }
    }

    private fun logoutDone() {
        mFilterType = TYPE_TIME_ALL
        mFilterStatus = null
        mFilterStartDate = null
        mFilterEndDate = null
    }

    override fun onObserve(): (HomeVM.() -> Unit) = {
        doObserve(getUpdateReportListLD()) { handleGetReports(it) }
        doObserve(getTotalReportsLD()) { showTotalReport(it) }
        doObserve(needToVerifyOtp()) { beforeCreateReport(it) }
        doObserve(logoutLD()) { logoutDone() }
        doObserve(needFillUserInfo()) { showDialogFillData() }
        observeSearch()
    }

    private fun showDialogFillData() {
        showDialog(getString(R.string.create_report_label), getString(R.string.please_update_address_info),
                cancelable = false, onPositive = { goToAddress() })
    }

    private fun goToAddress() {
        val intent = OpenActivity.getIntent(context, SettingAddressFragment::class.java)
        startActivity(intent)
        activity?.finish()
    }

    private fun showTotalReport(total: Int) {
        val fullText = getString(R.string.report_list_home, total)
        val webLink = getString(R.string.report_list_home_list, total)
        tv_total.setSpannable(fullText, arrayOf(webLink, total.toString()),
                arrayOf(R.color.text_color_major, R.color.text_color_minor), arrayOf(false, false))
    }

    private fun handleGetReports(reports: List<Report>) {
        mScrollListener.reset()
        mAdapter.submitList(reports)
        refresh_layout.isRefreshing = false
    }

    private fun getSearchKeyword(): String? {
        return edt_search?.getTrimText()
    }

    override fun onFragmentResult(requestCode: Int, action: Int, extraData: Intent?) {
        if (requestCode == CODE_SEARCH && action == HasFragmentResult.ACTION_OK) {
            val bundle = extraData?.extras ?: return

            val searchType = bundle.getInt(EXTRA_SEARCH_TYPE)
            if (searchType == TYPE_TIME_ALL) {
                mFilterStartDate = null
                mFilterEndDate = null
            } else if (searchType == TYPE_TIME_RANGE) {
                mFilterStartDate = bundle.getSerializable(EXTRA_START_DATE) as Date?
                mFilterEndDate = bundle.getSerializable(EXTRA_END_DATE) as Date?
            }
            mFilterStatus = bundle.getParcelable(EXTRA_TYPE_STATUS)
            mFilterType = searchType

            refreshSearchReport()
        }
    }

    private fun refreshSearchReport() {
        val reportInput = ReportInput(mFilterStatus, mFilterStartDate, mFilterEndDate, getSearchKeyword())
        viewModel.getReportsWithRefresh(reportInput)
    }

    private fun sendSearchRequest() {
        val reportInput = ReportInput(mFilterStatus, mFilterStartDate, mFilterEndDate, getSearchKeyword())
        viewModel.sendSearchRequest(reportInput)
    }

    override fun showLoading(cancelable: Boolean) {
        refresh_layout.isRefreshing = true
    }

    override fun hideLoading() {
        refresh_layout.isRefreshing = false
    }

    override fun handleBack(): Boolean {
        viewModel.openHomeTab(0)
        return true
    }

    override fun onDestroyView() {
        super.onDestroyView()
        rv_data.removeOnScrollListener(mScrollListener)
    }

    companion object {

        private const val CODE_SEARCH = 101

        fun newInstance() = ReportMainFragment()
    }
}