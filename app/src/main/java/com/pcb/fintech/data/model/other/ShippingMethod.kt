package com.pcb.fintech.data.model.other

enum class ShippingMethod(val value: Int) {
    UNKNOWN(0),
    ONLINE(1),
    OFFLINE(2);

    companion object {
        private val valueMap by lazy {
            return@lazy HashMap<Int, ShippingMethod>().apply {
                for (m in values()) {
                    put(m.value, m)
                }
            }
        }

        fun fromValue(value: Int?): ShippingMethod {
            return valueMap[value] ?: UNKNOWN
        }
    }
}