package com.pcb.fintech.data.model.body

import com.google.gson.annotations.SerializedName

data class AddressesBody
constructor(@SerializedName("addresses") val addresses: List<Address>)