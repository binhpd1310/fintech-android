package com.pcb.fintech.ui.home

import android.view.View
import android.widget.Toast
import com.pcb.fintech.R
import com.pcb.fintech.data.model.body.CreateNewsletter
import com.pcb.fintech.data.model.body.UpdateNewsletter
import com.pcb.fintech.di.ViewModelFactory
import com.pcb.fintech.ui.base.BaseMVVMFragment
import com.pcb.fintech.ui.views.EasyTextWatcher
import com.pcb.fintech.ui.views.OnClickAndHideKeyboard
import com.pcb.fintech.ui.views.getTrimText
import com.pcb.fintech.utils.ValidationUtil
import kotlinx.android.synthetic.main.fragment_register_noti.*

class RegisterNotiFragment : BaseMVVMFragment<RegisterNotiVM, ViewModelFactory>() {

    override fun getLayoutRes(): Int {
        return R.layout.fragment_register_noti
    }

    override fun getViewModelType(): Class<RegisterNotiVM> {
        return RegisterNotiVM::class.java
    }

    override fun setupViews(view: View) {
        toolbar.setNavigationOnClickListener { sendToNav { it.goBack() } }

        tv_cancel.setOnClickListener { sendToNav { it.goBack() } }

        edt_email.addTextChangedListener(EasyTextWatcher(afterTextChanged = { validateInputData() }))

        tv_register.setOnClickListener(OnClickAndHideKeyboard { registerNoti() })
    }

    private fun validateInputData() {
        var isValid = true
        if (!ValidationUtil.isValidEmail(edt_email.getTrimText())) isValid = false
        tv_register.isEnabled = isValid
    }

    private fun registerNoti() {
        val curNewsletter = viewModel.newsletterLD.value
        if (curNewsletter == null) {
            val body = CreateNewsletter(viewModel.getUserId(), edt_email.getTrimText())
            doObserveAll(viewModel.createNewsletterLD(body), onSuccess = {
                showToast(R.string.register_success, Toast.LENGTH_LONG)
            })
        } else {
            val newEmail = edt_email.getTrimText()
            if (newEmail == curNewsletter.email) return

            val body = UpdateNewsletter(curNewsletter.id, newEmail)
            doObserveAll(viewModel.updateNewsletter(body), onSuccess = {
                showToast(R.string.update_register_success, Toast.LENGTH_LONG)
            })
        }
    }

    override fun onObserve(): (RegisterNotiVM.() -> Unit) = {
        doObserve(newsletterLD) { edt_email.setText(it?.email) }
    }
}