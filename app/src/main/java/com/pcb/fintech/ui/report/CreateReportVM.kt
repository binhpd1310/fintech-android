package com.pcb.fintech.ui.report

import android.app.Application
import com.pcb.fintech.data.source.ReportDataSource
import com.pcb.fintech.data.model.body.ReportBody
import com.pcb.fintech.ui.base.BaseViewModel
import com.pcb.fintech.utils.SchedulerProvider

class CreateReportVM
constructor(application: Application, schedulerProvider: SchedulerProvider,
            private val reportDataSource: ReportDataSource
) : BaseViewModel(application, schedulerProvider) {

    fun createReport(reportBody: ReportBody) =
            resultLiveData { reportDataSource.createReport(reportBody) }
}