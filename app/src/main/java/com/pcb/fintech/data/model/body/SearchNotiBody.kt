package com.pcb.fintech.data.model.body

import com.google.gson.annotations.SerializedName

data class SearchNotiBody constructor(
        @SerializedName("displayPages") val displayPages: Int? = null,
        @SerializedName("currentPage") val currentPage: Int? = null
)