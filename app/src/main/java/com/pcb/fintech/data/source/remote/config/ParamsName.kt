package com.pcb.fintech.data.source.remote.config

object ParamsName {
    const val AUTHORIZATION = "Authorization"
    const val CONTENT_TYPE = "Content-Type"
    const val CACHE_CONTROL = "cache-control"
    const val REFRESH_TOKEN = "refresh_token"
    const val API_KEY = "X-Api-Key"
}