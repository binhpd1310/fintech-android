package com.pcb.fintech.data.model.body

import com.google.gson.annotations.SerializedName

data class Address constructor(
        @SerializedName("appUserId") var appUserId: Int? = null,
        @SerializedName("addressType") var addressType: Int? = null,
        @SerializedName("contactName") var contactName: String? = null,
        @SerializedName("phone") var phone: String? = null,
        @SerializedName("fullAddress") var fullAddress: String? = null,
        @SerializedName("provinceId") var provinceId: Int? = null,
        @SerializedName("districtId") var districtId: Int? = null,
        @SerializedName("wardId") var wardId: Int? = null) {

    constructor(addressType: Int, appUserId: Int?) : this(appUserId, addressType, null,
            null, null, null, null, null)

}