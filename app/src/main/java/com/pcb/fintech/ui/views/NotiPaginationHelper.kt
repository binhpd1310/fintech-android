package com.pcb.fintech.ui.views

import com.pcb.fintech.data.model.ResponseResult
import com.pcb.fintech.data.model.body.SearchNotiBody
import com.pcb.fintech.data.model.response.Notification
import com.pcb.fintech.data.source.CommonDataSource
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.util.concurrent.atomic.AtomicBoolean
import javax.inject.Inject

class NotiPaginationHelper @Inject constructor(
        private val coroutineScope: CoroutineScope,
        private val commonDataSource: CommonDataSource) : PaginationHelper<List<Notification>> {

    private var pageLoading = PAGE_DEFAULT

    private var isLoadMore = true

    private var isApiCalling = AtomicBoolean(false)

    private val mNotifications = mutableListOf<Notification>()

    override fun canCallNext(): Boolean = isLoadMore

    override fun callWithRefresh(callback: (result: List<Notification>) -> Any) {
        if (isApiCalling.get()) return

        isApiCalling.set(true)

        isLoadMore = true
        pageLoading = PAGE_DEFAULT

        mNotifications.clear()

        request(callback)
    }

    override fun callNext(callback: (result: List<Notification>) -> Any) {
        if (isApiCalling.get()) return

        if (!canCallNext()) return

        isApiCalling.set(true)

        pageLoading += 1

        request(callback)
    }

    private fun getRequestBody() = SearchNotiBody(
            displayPages = PAGE_SIZE,
            currentPage = pageLoading)

    private fun request(callback: (result: List<Notification>) -> Any) {
        coroutineScope.launch {
            withContext(Dispatchers.IO) {
                val responseResult = commonDataSource.getNotifications(getRequestBody())

                if (responseResult is ResponseResult.Success) {
                    val curPageNumber = responseResult.data.pageNumber ?: 0
                    val totalPageNumber = responseResult.data.totalNumberOfPages ?: 0
                    isLoadMore = curPageNumber < totalPageNumber

                    val reports = responseResult.data.results ?: mutableListOf()
                    mNotifications.addAll(reports)

                    val data = mutableListOf<Notification>()
                            .apply { addAll(mNotifications) }
                    handleResult(data, callback)

                    return@withContext
                } else if (responseResult is ResponseResult.Error) {
                    handleResult(null, callback)

                    return@withContext
                }
                handleResult(null, callback)
            }
        }
    }

    private fun handleResult(result: List<Notification>? = null,
                             callback: ((result: List<Notification>) -> Any)? = null) {
        isApiCalling.set(false)

        result ?: return
        callback?.invoke(result)
    }

    fun updateNotification(notiId: Int, found: (index: Int, notiList: MutableList<Notification>) -> Unit,
                           finish: (notis: List<Notification>) -> Unit) {
        for ((index, item) in mNotifications.withIndex()) {
            if (item.id == notiId) {
                found.invoke(index, mNotifications)
            }
        }
        finish.invoke(mNotifications)
    }

    companion object {
        private const val PAGE_DEFAULT = 1
        private const val PAGE_SIZE = 10
    }
}