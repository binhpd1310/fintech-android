package com.pcb.fintech.data.source.remote

import com.pcb.fintech.data.model.body.*
import com.pcb.fintech.data.model.other.Report
import com.pcb.fintech.data.model.response.*
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Response
import retrofit2.http.*

interface ApiService {

    /**
     * Account
     */

    @POST("api/customer/Register/VerifyProfile")
    suspend fun verifyRegisterProfile(@Body profile: ProfileBody): Response<Int>

    @POST("api/customer/Service/CreateOtp/{phoneNumber}")
    suspend fun createOtp(@Path("phoneNumber") phoneNumber: String): Response<OtpCreated>

    @POST("api/customer/Service/VerifyOtp")
    suspend fun verifyOtp(@Body verifyOtpBody: VerifyOtpBody): Response<Boolean>

    @Multipart
    @POST("api/customer/Service/UploadImage")
    suspend fun uploadImage(
            @Part("PHONE_NUMBER") phone: RequestBody,
            @Part("IMAGE_TYPE") imageType: RequestBody,
            @Part imageFile: MultipartBody.Part
    ): Response<UploadImage>

    @POST("api/customer/Service/FaceVerify")
    suspend fun verifyFace(@Body verifyFaceBody: VerifyFaceBody): Response<Float>

    @POST("api/customer/Service/TextRecognition")
    suspend fun extractCardInfo(@Body body: ExtractCardBody): Response<CardInfo>

    @POST("api/customer/Register/Create")
    suspend fun registerAccount(@Body account: RegisterAccountBody): Response<Boolean>

    @POST("api/Auth")
    suspend fun oldLogin(@Body loginBody: LoginBody): Response<LoginResponse>

    @POST("api/auth/appgettoken")
    suspend fun login(@Body loginBody: LoginBody): Response<LoginResponse>

    @GET("api/common/Province/GetAll")
    suspend fun getProvinces(): Response<List<Province>>

    @GET("api/common/District/GetAll")
    suspend fun getDistricts(): Response<List<District>>

    @GET("api/common/Ward/GetAll")
    suspend fun getWards(): Response<List<Ward>>

    @GET("api/Customer/SecretQuestion/GetAll")
    suspend fun getSecretQuestions(): Response<List<SecretQuestion>>

    @PUT("api/Customer/Profile/UpdateSecretQuestion")
    suspend fun updateSecretQuestion(@Body secretQuestionBody: SecretQuestionBody): Response<Boolean>

    @POST("api/Account/AppForgotPassword")
    suspend fun forgotPassword(@Body emailBody: EmailBody): Response<Int>

    @GET("api/common/Logout/Logout")
    suspend fun logout(): Response<Any>

    /**
     *  Profile
     */

    @GET("api/customer/Profile/GetInfo")
    suspend fun getAccountInfo(): Response<AccountInfo>

    @PUT("api/customer/Profile/UpdateInfo")
    suspend fun updateAccountInfo(@Body accountInfo: AccountInfo): Response<Boolean>

    @GET("api/Customer/Profile/GetAddress")
    suspend fun getProfileAddress(): Response<AddressesBody>

    @PUT("api/Customer/Profile/ChangePassword")
    suspend fun updatePassword(@Body changePasswordBody: ChangePasswordBody): Response<Boolean>

    @PUT("api/Customer/Profile/UpdateAddress")
    suspend fun updateAddress(@Body addressesBody: AddressesBody): Response<Boolean>

    @POST("api/customer/Profile/UpdateProfile")
    suspend fun updateProfile(@Body profile: UpdateProfileBody): Response<Boolean>

    @POST("api/customer/Profile/VerifyProfile")
    suspend fun verifyUpdateProfile(@Body profile: ProfileBody): Response<Int>

    /**
     * Reports
     */

    @GET("api/customer/Report/TotalReport")
    suspend fun getTotalReport(): Response<Int>

    @POST("api/customer/Report/Create")
    suspend fun createReport(@Body reportBody: ReportBody): Response<Int>

    @POST("api/customer/Report/Search")
    suspend fun getAllReports(@Body searchReportBody: SearchReportBody): Response<ReportsResponse>

    @POST("api/customer/Report/UpdateNote")
    suspend fun updateReportNote(@Body reportNoteBody: ReportNoteBody): Response<Boolean>

    @GET("api/customer/Report/GetById/{reportId}")
    suspend fun getReportById(@Path("reportId") reportId: Int): Response<Report>

    @DELETE("api/customer/Report/UpdateDeleteStatus/{reportId}")
    suspend fun deleteReportById(@Path("reportId") reportId: Int): Response<Boolean>

    @GET("api/customer/Report/IsReady")
    suspend fun canCreateReport(): Response<Boolean>

    /**
     * Payment
     */

    @POST("api/customer/Report/GetPayment")
    suspend fun getPaymentReport(@Query("reportId") reportId: Int): Response<PaymentReportResponse>

    @GET("api/admin/Setting/GetById/5")
    suspend fun getShippingFee(): Response<ShippingFee>

    @POST("api/customer/Report/PaymentOffline")
    suspend fun paymentOffline(@Body paymentBody: PaymentBody): Response<Boolean>

    @POST("api/customer/Report/PaymentOnline")
    suspend fun paymentOnline(@Body paymentBody: PaymentBody): Response<PaymentOnlineResponse>

    @GET("api/PaymentOnline/PaymentResultDisplay")
    suspend fun paymentOnlineStatus(@Query("vnp_Amount") vnp_Amount: Double,
                                    @Query("vnp_BankCode") vnp_BankCode: String,
                                    @Query("vnp_BankTranNo") vnp_BankTranNo: String,
                                    @Query("vnp_CardType") vnp_CardType: String,
                                    @Query("vnp_OrderInfo") vnp_OrderInfo: String,
                                    @Query("vnp_PayDate") vnp_PayDate: Long,
                                    @Query("vnp_ResponseCode") vnp_ResponseCode: String,
                                    @Query("vnp_TmnCode") vnp_TmnCode: String,
                                    @Query("vnp_TransactionNo") vnp_TransactionNo: String,
                                    @Query("vnp_TxnRef") vnp_TxnRef: Long,
                                    @Query("vnp_SecureHashType") vnp_SecureHashType: String,
                                    @Query("vnp_SecureHash") vnp_SecureHash: String): Response<PaymentOnlineResultResponse>

    @GET("api/common/Report/Download/{report_id}")
    suspend fun downloadReport(@Path("report_id") reportId: Int): Response<ResponseBody>

    /**
     * Notification
     */

    @GET("api/customer/Notify/GetAll")
    suspend fun getAllNotifications(): Response<List<Notification>>

    @POST("api/customer/Notify/Search")
    suspend fun getNotifications(@Body searchNotiBody: SearchNotiBody): Response<NotisResponse>

    @GET("api/customer/Notify/GetById/{id}")
    suspend fun getNotiById(@Path("id") notiId: Int): Response<Notification>

    @DELETE("api/customer/Notify/Delete/{id}")
    suspend fun deleteOneNoti(@Path("id") id: Int): Response<Any>

    @DELETE("api/customer/Notify/DeleteAll")
    suspend fun deleteAllNoti(): Response<Any>

    /**
     * Others
     */

    @GET("api/customer/MobileApp/GetSlides")
    suspend fun getSlides(): Response<List<Slide>>

    @GET("api/customer/MobileApp/GetLinks")
    suspend fun getLinks(): Response<List<WebLink>>

    @POST("api/customer/MobileApp/CreateOtp/{phoneNumber}")
    suspend fun createMobileOtp(@Path("phoneNumber") phoneNumber: String): Response<OtpCreated>

    @POST("api/customer/MobileApp/VerifyOtp")
    suspend fun verifyMobileOtp(@Body verifyOtpBody: VerifyOtpBody): Response<Boolean>

    @GET("api/customer/MobileApp/GetNewsletter/{customer_id}")
    suspend fun getNewsletter(@Path("customer_id") customerId: Int): Response<RegisterNewsletter>

    @POST("api/customer/MobileApp/CreateNewsletter")
    suspend fun createNewsletter(@Body body: CreateNewsletter): Response<Boolean>

    @PUT("api/customer/MobileApp/UpdateNewsletter")
    suspend fun updateNewsletter(@Body body: UpdateNewsletter): Response<Boolean>
}
