package com.pcb.fintech.data.model.body

import com.google.gson.annotations.SerializedName

data class ReportBody(@SerializedName("note") val note: String)