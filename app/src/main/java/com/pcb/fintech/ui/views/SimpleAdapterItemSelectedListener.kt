package com.pcb.fintech.ui.views

import android.view.View
import android.widget.AdapterView

open class SimpleAdapterItemSelectedListener : AdapterView.OnItemSelectedListener {
    override fun onNothingSelected(parent: AdapterView<*>?) {
        /*Do nothing*/
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        /*Do nothing*/
    }
}