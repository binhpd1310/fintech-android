package com.pcb.fintech.eventbus.event

import com.pcb.fintech.eventbus.base.BaseEvent
import com.pcb.fintech.eventbus.observer.EventObserver

class CancelledReportEvent : BaseEvent<String>(TYPE) {

    override fun fire(observers: List<EventObserver>) {
        observers.forEach { it.onEvent(this) }
    }

    override fun getType(): String = TYPE

    companion object {
        var TYPE = this::class.java.name
    }
}