package com.pcb.fintech.data.utils

import com.pcb.fintech.data.model.body.Address

fun Address?.isFullFilled(): Boolean {
    return this != null
            && appUserId != null
            && addressType != null
            && !contactName.isNullOrEmpty()
            && !phone.isNullOrEmpty()
            && !fullAddress.isNullOrEmpty()
            && provinceId != null
            && districtId != null
            && wardId != null
}

fun Address?.isCommonFilled(): Boolean {
    return this != null
            && appUserId != null
            && addressType != null
            && !fullAddress.isNullOrEmpty()
            && provinceId != null
            && districtId != null
            && wardId != null
}