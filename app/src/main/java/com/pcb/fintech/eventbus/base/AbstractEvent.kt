package com.pcb.fintech.eventbus.base

import com.pcb.fintech.eventbus.observer.EventObserver

interface AbstractEvent {

    fun fire(observers: List<EventObserver>)

    fun getType(): String
}