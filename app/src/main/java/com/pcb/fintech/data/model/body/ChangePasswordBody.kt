package com.pcb.fintech.data.model.body

import com.google.gson.annotations.SerializedName

data class ChangePasswordBody constructor(
        @SerializedName("oldPassword") val oldPassword: String,
        @SerializedName("newPassword") val newPassword: String,
        @SerializedName("appUserId") var appUserId: Int)