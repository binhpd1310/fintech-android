package com.pcb.fintech.ui.profile

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Handler
import android.provider.MediaStore
import android.view.View
import android.widget.AdapterView
import android.widget.Toast
import com.pcb.fintech.R
import com.pcb.fintech.data.model.body.ExtractCardBody
import com.pcb.fintech.data.model.body.ProfileBody
import com.pcb.fintech.data.model.body.UpdateProfileBody
import com.pcb.fintech.data.model.other.CardType
import com.pcb.fintech.data.model.response.AccountInfo
import com.pcb.fintech.data.model.response.CardInfo
import com.pcb.fintech.data.model.response.ProfileVerificationResult
import com.pcb.fintech.data.model.response.UploadImage
import com.pcb.fintech.di.ViewModelFactory
import com.pcb.fintech.ui.base.BaseMVVMFragment
import com.pcb.fintech.ui.base.HasFragmentResult
import com.pcb.fintech.ui.registration.RegistrationVM
import com.pcb.fintech.ui.views.*
import com.pcb.fintech.utils.*
import com.pcb.fintech.utils.ImageUtil.fixOrientationBugOfProcessedBitmap
import com.pcb.fintech.utils.ImageUtil.generateTimeStampPhotoFile
import com.pcb.fintech.utils.ImageUtil.processBeforeUpload
import com.pcb.fintech.utils.validation.CardValidationResult
import kotlinx.android.synthetic.main.fragment_setting_profile.*
import kotlinx.android.synthetic.main.layout_profile_select_images.*
import kotlinx.android.synthetic.main.layout_profile_verify_info.*
import java.io.File
import java.util.*

class SettingProfileFragment : BaseMVVMFragment<RegistrationVM, ViewModelFactory>() {

    private var frontImageFile: File? = null
    private var backImageFile: File? = null
    private var faceImageFile: File? = null
    private var frontImagePath: String? = null
    private var backImagePath: String? = null
    private var faceImagePath: String? = null
    private var cardType: CardType? = null
    private var idCardNumber: String? = null
    private var issueOfDate: Date? = null
    private var expiredOfDate: Date? = null
    private var issueOfPlace: String? = null

    private var currentTakeImageCode: Int = -1

    override fun getLayoutRes(): Int {
        return R.layout.fragment_setting_profile
    }

    override fun getViewModelType(): Class<RegistrationVM> {
        return RegistrationVM::class.java
    }

    override fun viewModelInActivityScope(): Boolean = true

    override fun onDestroyView() {
        removeTextWatcher()
        super.onDestroyView()
    }

    override fun setupViews(view: View) {
        tv_previous.setOnClickListener { sendToNav { it.goBack() } }

        toolbar.setNavigationOnClickListener { sendToNav { it.goBack() } }

        registerEvent()
    }

    private fun registerEvent() {
        iv_front.setOnClickListener { takePhoto(REQUEST_CODE_FRONT_IMAGE) }
        iv_back.setOnClickListener { takePhoto(REQUEST_CODE_BACK_IMAGE) }
        iv_face.setOnClickListener { takePhoto(REQUEST_CODE_FACE_IMAGE) }
        tv_issue_of_date.setOnClickListener { showPickIssueOfDate() }
        tv_expired_of_date.setOnClickListener { showPickExpiredOfDate() }

        tv_save.setOnClickListener { verifyProfile() }

        sp_id_type.onItemSelectedListener = spIdTypeListener

        getInfo()

        addTextWatcher()
    }

    private fun getInfo() {
        delayOnMain({
            doObserveAll(viewModel.getAccountInfoLD(), { fillAccountInfo(it) },
                    { showDialogMessage(R.string.error_common_title, R.string.error_account_info_invalid, R.string.close) })
        })
    }

    private val textWatcher = EasyTextWatcher(afterTextChanged = { validateInputData() })

    private fun addTextWatcher() {
        edt_number_of_card.addTextChangedListener(textWatcher)
        tv_issue_of_date.addTextChangedListener(textWatcher)
        tv_expired_of_date.addTextChangedListener(textWatcher)
        edt_issue_of_place.addTextChangedListener(textWatcher)
    }

    private fun removeTextWatcher() {
        edt_number_of_card.removeTextChangedListener(textWatcher)
        tv_issue_of_date.removeTextChangedListener(textWatcher)
        tv_expired_of_date.removeTextChangedListener(textWatcher)
        edt_issue_of_place.removeTextChangedListener(textWatcher)
    }

    private fun fillAccountInfo(accountInfo: AccountInfo?) {
        accountInfo ?: return

        removeTextWatcher()

        val idType = accountInfo.idType ?: 0
        sp_id_type.setSelection(idType)
        edt_number_of_card.setText(accountInfo.idNumber)
        tv_issue_of_date.text = accountInfo.getIssuedOfDateText(DateTimeUtil.DATE_TEXT_2)
        edt_issue_of_place.setText(accountInfo.issuedPlace)
        val frontUrl = Constant.DOMAIN + accountInfo.frontCard
        val backUrl = Constant.DOMAIN + accountInfo.backsideCard
        val faceUrl = Constant.DOMAIN + accountInfo.imagePath
        iv_front.setImageURI(frontUrl.formatPath())
        iv_back.setImageURI(backUrl.formatPath())
        iv_face.setImageURI(faceUrl.formatPath())

        cardType = CardType.fromValue(idType)
        idCardNumber = accountInfo.idNumber
        issueOfDate = accountInfo.getIssuedOfDate()
        issueOfPlace = accountInfo.issuedPlace
        frontImagePath = accountInfo.frontCard
        backImagePath = accountInfo.backsideCard
        faceImagePath = accountInfo.imagePath
        setRuleForCardInput(cardType)

        validateInputData()

        addTextWatcher()
    }

    private val spIdTypeListener = object : SimpleAdapterItemSelectedListener() {
        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            cardType = CardType.fromValue(position)
            setRuleForCardInput(cardType)
            validateInputData()
        }
    }

    private fun showPickIssueOfDate() {
        val datePickerDialog = newInstanceWithLimit(issueOfDate) {
            issueOfDate = it
            tv_issue_of_date.text = DateTimeUtil.formatDate(it, DateTimeUtil.DATE_TEXT_2)
        }
        datePickerDialog.show(activity?.fragmentManager, "")
    }

    private fun showPickExpiredOfDate() {
        val datePickerDialog = newInstance(expiredOfDate) {
            expiredOfDate = it
            tv_expired_of_date.text = DateTimeUtil.formatDate(it, DateTimeUtil.DATE_TEXT_2)
        }
        datePickerDialog.show(activity?.fragmentManager, "")
    }

    private fun takePhoto(requestCode: Int) {
        val appContext = activity?.applicationContext ?: return
        if (requestCode == -1) return

        if (viewModel.isCameraPermissionGranted()) {
            val imageFile = generateTimeStampPhotoFile(appContext, MEDIA_DIR)
            if (requestCode == REQUEST_CODE_FRONT_IMAGE) {
                frontImageFile = imageFile
            }
            if (requestCode == REQUEST_CODE_BACK_IMAGE) {
                backImageFile = imageFile
            }
            if (requestCode == REQUEST_CODE_FACE_IMAGE) {
                faceImageFile = imageFile
            }
            val imageUri = getImageUri(appContext, imageFile)
            val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri)
            startActivityForResult(intent, requestCode)
        } else {
            currentTakeImageCode = requestCode
            val permissions = arrayOf(Manifest.permission.CAMERA)
            requestPermissions(permissions, REQUEST_CAMERA)
        }
    }

    private fun setRuleForCardInput(cardType: CardType?) {
        when (cardType) {
            CardType.IDENTITY_CARD -> edt_number_of_card.setMaxLength(9)
            CardType.ID_CARD -> edt_number_of_card.setMaxLength(12)
            CardType.PASSPORT -> edt_number_of_card.setMaxLength(8)
            else -> edt_number_of_card.setMaxLength(12)
        }
    }

    private fun validateInputData(): Boolean {
        idCardNumber = edt_number_of_card.getTrimText()

        var isValid = true
        if (cardType == null || cardType == CardType.UNKNOWN) isValid = false
        if (frontImagePath == null) isValid = false
        if (backImagePath == null) isValid = false
        if (idCardNumber.isNullOrEmpty()) isValid = false
        tv_save.isEnabled = isValid
        return isValid
    }

    private fun validateCardNumber(): Boolean {
        val cardNumberText = edt_number_of_card.getTrimText()

        val result = ValidationUtil.isValidCardNumber(cardType, cardNumberText)
        if (result is CardValidationResult.CardInvalid) {
            showSnackBar(rootView, result.msgRes)
            return false
        }
        return true
    }

    private fun takePhotoCallback(requestCode: Int) {
        if (requestCode == REQUEST_CODE_FRONT_IMAGE) {
            uploadFrontImage()
            return
        }
        if (requestCode == REQUEST_CODE_BACK_IMAGE) {
            uploadBackImage()
            return
        }
        if (requestCode == REQUEST_CODE_FACE_IMAGE) {
            uploadFaceImage()
            return
        }
    }

    /**
     * Request to upload front card image
     */
    private fun uploadFrontImage() {
        val phoneNumber = viewModel.getAccountInfo()?.phone ?: return
        val tempFile = frontImageFile ?: return
        val bitmap = fixOrientationBugOfProcessedBitmap(tempFile, context!!) ?: return
        val imageFile = processBeforeUpload(activity, tempFile, bitmap, MEDIA_DIR) ?: return
        frontImageFile = imageFile

        doObserveAll(viewModel.uploadFrontImage(phoneNumber, imageFile),
                { handleUploadFrontImageResult(it) },
                { handleUploadFrontImageFailed() })
    }

    private fun handleUploadFrontImageResult(result: UploadImage) {
        val subPath = result.imagePath
        val imagePath = Constant.DOMAIN + subPath
        iv_front.setImageURI(imagePath.formatPath())
        showSnackBar(rootView, R.string.text_upload_image_successful)
        frontImagePath = subPath
    }

    private fun handleUploadFrontImageFailed() {
        frontImageFile = null
        showDefaultDialog()
    }

    /**
     * Request to upload back card image
     */
    private fun uploadBackImage() {
        val phoneNumber = viewModel.getAccountInfo()?.phone ?: return
        val tempFile = backImageFile ?: return
        val bitmap = fixOrientationBugOfProcessedBitmap(tempFile, context!!) ?: return
        val imageFile = processBeforeUpload(activity, tempFile, bitmap, MEDIA_DIR) ?: return
        backImageFile = imageFile

        doObserveAll(viewModel.uploadBackImage(phoneNumber, backImageFile!!),
                { handleUploadBackImageResult(it) },
                { handleUploadBackImageFailed() })
    }

    private fun handleUploadBackImageResult(result: UploadImage) {
        val subPath = result.imagePath
        val imagePath = Constant.DOMAIN + subPath
        iv_back.setImageURI(imagePath.formatPath())
        showSnackBar(rootView, R.string.text_upload_image_successful)
        backImagePath = subPath
    }

    private fun handleUploadBackImageFailed() {
        backImageFile = null
        showDefaultDialog()
    }

    /**
     * Request to upload face image
     */
    private fun uploadFaceImage() {
        val phoneNumber = viewModel.getAccountInfo()?.phone ?: return
        val tempFile = faceImageFile ?: return
        val bitmap = fixOrientationBugOfProcessedBitmap(tempFile, context!!) ?: return
        val imageFile = processBeforeUpload(activity, tempFile, bitmap, MEDIA_DIR) ?: return
        faceImageFile = imageFile

        doObserveAll(viewModel.uploadFaceImage(phoneNumber, faceImageFile!!),
                { handleUploadFaceImageResult(it) },
                { handleUploadFaceImageFailed() })
    }

    private fun handleUploadFaceImageResult(result: UploadImage) {
        val subPath = result.imagePath
        val imagePath = Constant.DOMAIN + subPath
        iv_face.setImageURI(imagePath.formatPath())
        showSnackBar(rootView, R.string.text_upload_image_successful)
        faceImagePath = subPath
    }

    private fun handleUploadFaceImageFailed() {
        faceImageFile = null
        showDefaultDialog()
    }

    /**
     * Request to verify face
     */
    private fun verifyFace() {
//        if (frontImagePath != null && backImagePath != null) {
//            doObserveAll(viewModel.verifyFace(frontImagePath!!, backImagePath!!),
//                    { /* Do nothing */ },
//                    { showSnackBar(rootView, it) })
//        }
    }

    /**
     * Request to extract mock from card
     */
    private fun extractCardInfo() {
        val frontPath = frontImagePath ?: return
        val backPath = backImagePath ?: return
        val type = cardType?.value ?: return
        doObserveAll(viewModel.extractCardInfo(ExtractCardBody(type, frontPath, backPath)),
                { handleExtractCardInfo(it) })
    }

    private fun handleExtractCardInfo(result: CardInfo) {
        edt_number_of_card.setText(result.idNumber)
        edt_issue_of_place.setText(result.issuedPlace)
        validateInputData()
    }

    /**
     * Verify profile
     */
    private fun verifyProfile() {
        if (!validateInputData()) return
        if (!validateCardNumber()) return

        val accountInfo = viewModel.getAccountInfo() ?: return
        val userName = accountInfo.userName ?: return
        val idNumber = idCardNumber ?: return
        val profileBody = ProfileBody.makeBodyForVerifyIdNumber(userName, idNumber)

        doObserveAll(viewModel.verifyProfileLD(profileBody),
                onSuccess = { onVerifyProfileResult(it) })
    }

    private fun onVerifyProfileResult(result: ProfileVerificationResult) {
        if (result == ProfileVerificationResult.Success) {
            updateAccount()
        } else {
            handleProfileError(result)
        }
    }

    private fun handleProfileError(result: ProfileVerificationResult?) {
        val errorMsg: Int = when (result) {
            ProfileVerificationResult.IdNumberExisted -> R.string.error_id_number_exists
            else -> R.string.error_profile_invalid
        }
        showDialogMessage(R.string.error_common_title, errorMsg, R.string.close)
    }

    /**
     * Update profile info
     */
    private fun updateAccount() {
        val userId = viewModel.getUserId()
        val issueOfDateStr = issueOfDate.toDateString(DateTimeUtil.DATE_TEXT_3)
        val expiredOfDateStr = expiredOfDate.toDateString(DateTimeUtil.DATE_TEXT_3)
        val accountBody = UpdateProfileBody(
                idNumber = idCardNumber, confidence = 0, idType = cardType?.value,
                frontCard = frontImagePath, backsideCard = backImagePath, imagePath = faceImagePath,
                issuedDate = issueOfDateStr, expiredDate = expiredOfDateStr,
                issuedPlace = issueOfPlace, appUserId = userId
        )
        doObserveAll(viewModel.updateProfile(accountBody),
                onSuccess = { handleUpdateAccountResult(it) })
    }

    private fun handleUpdateAccountResult(result: Boolean) {
        if (result) {
            viewModel.postHideLoading()
            setFragmentResult(HasFragmentResult.ACTION_OK)
            showToast(R.string.text_update_info_successful, Toast.LENGTH_LONG)
            Handler().postDelayed({ sendToNav { it.goBack() } }, 1000)
        } else {
            showDialogMessage(R.string.error_common_title, R.string.text_update_info_failed, R.string.close)
        }
    }

    /**
     * onActivityResult
     */
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            takePhotoCallback(requestCode)
            validateInputData()
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>,
                                            grantResults: IntArray) {
        if (requestCode == REQUEST_CAMERA) {
            val granted = grantResults[0] == PackageManager.PERMISSION_GRANTED
            if (granted) takePhoto(currentTakeImageCode)
        }
    }

    companion object {
        private const val REQUEST_CAMERA = 100
        private const val REQUEST_CODE_FRONT_IMAGE = 101
        private const val REQUEST_CODE_BACK_IMAGE = 102
        private const val REQUEST_CODE_FACE_IMAGE = 103
        private const val MEDIA_DIR = "media"

        fun newInstance() = SettingProfileFragment()
    }
}